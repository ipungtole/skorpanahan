<?php defined('BASEPATH') OR exit('No direct script access allowed');

Class M_catatan_perlombaan extends CI_Model {
    var $table = 't_peserta';
    var $column_order = array('t_peserta.id_peserta','nik','t_peserta.nama_peserta','nama_event','m_event.lap','count(t_scores.skor)','sum(t_scores.skor)'); //set column field database for datatable orderable
    var $column_search = array('nama_peserta','nik'); //set column field database for datatable searchable just firstname , lastname , address are searchable
    var $order = array('sum(t_scores.skor)' => 'desc'); // default order 

    var $table2 = 't_scores';
    var $column_order2 = array('skor'); //set column field database for datatable orderable
    var $column_search2 = array('skor'); //set column field database for datatable searchable just firstname , lastname , address are searchable
    var $order2 = array('id_transaction' => 'desc'); // default order 
 
 
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }
 
    private function _get_datatables_query(){
         
        $this->db->select('t_peserta.id_peserta,t_peserta.nama_peserta,t_peserta.nik,m_event.nama_event,m_event.lap,count(t_scores.skor) jumlah,sum(t_scores.skor) total');
        $this->db->from($this->table);
        $this->db->join('t_scores','t_scores.id_peserta = t_peserta.id_peserta','left');
        $this->db->join('m_event','m_event.id_event = t_peserta.id_event');
        $this->db->where(['t_peserta.deleted' => null,'t_peserta.status_peserta' => 1]);
        if($this->input->post('event')){
            $this->db->where(['t_peserta.id_event' => $this->input->post('event')]);
        }
        $this->db->group_by('t_peserta.nama_peserta,t_peserta.nik,t_peserta.id_peserta');
        
		
        $i = 0;
     
        foreach ($this->column_search as $item) // loop column 
        {
            if($this->input->post('search')) // if datatable send POST for search
            {
                 
                if($i===0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
 
                if(count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }
         
        if(isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
 
    function get_datatables(){
        $this ->_get_datatables_query();
        if($this->input->post('length') != -1)
        $this->db->limit($this->input->post('length'),$this->input->post('start'));
        $query = $this->db->get();
        return $query->result();
    }
 
    function count_filtered( ){
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
 
    public function count_all(){
        $this->_get_datatables_query();
        return $this->db->count_all_results();
    }

    function get_transaction($id_peserta){
        $this->db->select('count(id_peserta) as done');
        $this->db->from('t_scores');
        $this->db->where('id_peserta',$id_peserta);
        $this->db->group_by('id_peserta');
        $query = $this->db->get();
        return $query->row();
    }

    function get_lap($id_peserta){
        $this->db->select('m_event.lap');
        $this->db->from('t_peserta');
        $this->db->join('m_event','t_peserta.id_event = m_event.id_event');
        $this->db->where('id_peserta',$id_peserta);
        $query = $this->db->get();
        return $query->row();
    }

    private function _get_datatables_query2(){
         
        $this->db->from($this->table2);
        $this->db->where(['id_peserta' => $this->input->get('peserta')]);
		
        $i = 0;
     
        foreach ($this->column_search2 as $item) // loop column 
        {
            if($this->input->post('search')) // if datatable send POST for search
            {
                 
                if($i===0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
 
                if(count($this->column_search2) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }
         
        if(isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order2[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order2))
        {
            $order = $this->order2;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
 
    function get_datatables2(){
        $this ->_get_datatables_query2();
        if($this->input->post('length') != -1)
        $this->db->limit($this->input->post('length'),$this->input->post('start'));
        $query = $this->db->get();
        return $query->result();
    }
 
    function count_filtered2( ){
        $this->_get_datatables_query2();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all2(){
        $this->_get_datatables_query2();
        return $this->db->count_all_results();
    }
    
}
