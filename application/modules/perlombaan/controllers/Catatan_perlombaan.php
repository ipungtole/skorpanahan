<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Catatan_perlombaan extends MY_Controller {
    function __construct(){
		parent::__construct();
		$this->load->library('template');
		$this->load->model('dashboard/m_global','global');
		
        $akses = modules::run('dashboard/login/role_acess');
        
    	if(!$akses){
    		$forbidden = modules::run('dashboard/login/forbidden');
    		echo $forbidden;
    		exit;
    	}

    }

    public function index(){
        $act            = $this->input->get('act');
        $data['rules']  = modules::run('dashboard/login/get_role_acess');
        $data['lang']   = $this->global->get_data('m_plugins',['id_plugins' => '1'],null)->row()->status;
      
    	switch ($act) {
            case 'list':
                $this->access('list_data');
    			$this->list($data['rules']->view_button,$data['rules']->edit_button,$data['rules']->delete_button,$data['rules']->link_menu,$data['rules']->add_button);
                break;
            case 'list_detail':
    			$this->list_detail($data['rules']->view_button,$data['rules']->edit_button,$data['rules']->delete_button,$data['rules']->link_menu,$data['rules']->add_button);
                break;
            case 'add':
                $this->access('add_button');
                $this->add($data['rules']->link_menu);
                break;
            case 'edit':
                $this->access('edit_button');
                $this->update();
                break;
            case 'order':
                $type   = $this->input->get('type');
                $urutan = $this->input->get('urutan');
                $this->order($urutan,$type);
                break;
            case 'delete':
                $this->access('delete_button');
                $id  = $this->input->get('id');
                $this->delete($id);
                break;
            case 'detail':
                $id  = $this->input->get('id');
                $this->detail($id);
                break;
            case 'detail_scores':
                $id  = $this->input->get('id');
                $this->detail_scores($id);
                break;
            case 'pages':
                $this->get_pages();
                break;
            case 'bahasa':
                $this->get_language();
                break;
            default:
                $this->access('list_data');
				$this->template->dashboards($data['rules']->link_menu,$data);
    			break;
    	}
    }

  
    private function access($act){
        $data = modules::run('dashboard/login/get_role_acess');
        if(!$data->$act){
            $forbidden = modules::run('dashboard/login/forbidden');
            echo $forbidden;
            exit;
        }
    }

    private function order($urutan,$type){
        $urutan_awal = (int)$urutan;
        if(!is_numeric($urutan_awal)){
            echo 'acess denied';
            exit;    
        }
        if($type == 'up'){
            if($urutan_awal == 1){
                echo json_encode(['status' => false,'pesan' => 'Menu Awal gak bisa di naikkan lagi']);
                exit;
            }
            $di_ganti = $this->global->get_data('m_menufront',['order' => $urutan_awal])->row();
            $me_ganti = $this->global->get_data('m_menufront',['order' => (int)$urutan_awal-1])->row();
            $this->global->update_tabel('m_menufront',['order' => (int)$urutan_awal-1],['id_menu' => $di_ganti->id_menu]);
            $this->global->update_tabel('m_menufront',['order' => $urutan_awal],['id_menu' => $me_ganti->id_menu]);
            echo json_encode(['status' => true,'pesan' => 'Data berhasil di update']);
        }else{
            $this->db->select_max('order');
            $result = $this->db->get('m_menufront')->row();
            if($urutan_awal == $result->order){
                echo json_encode(['status' => false,'pesan' => 'Menu Terakhir gak bisa di turunkan lagi']);
                exit;
            }

            $di_ganti = $this->global->get_data('m_menufront',['order' => $urutan_awal])->row();
            $me_ganti = $this->global->get_data('m_menufront',['order' => (int)$urutan_awal+1])->row();
            $this->global->update_tabel('m_menufront',['order' => (int)$urutan_awal+1],['id_menu' => $di_ganti->id_menu]);
            $this->global->update_tabel('m_menufront',['order' => $urutan_awal],['id_menu' => $me_ganti->id_menu]);
            echo json_encode(['status' => true,'pesan' => 'Data berhasil di update']);
         
        }
    }

    private function list($view,$edit,$delete,$url,$add){
        $method		= $this->input->method();
		if($method != 'post'){
			$forbidden = modules::run('dashboard/login/forbidden');
    		echo $forbidden;
			exit;
		}	
        $url_exp    = explode('/',$url);
        $alias      = $url_exp[1];
        $this->load->model($url_exp[0].'/m_'.$url_exp[1],$alias);
        $bahasa     = $this->input->post('bahasa'); 

        $where      = ['m_menufront.language' => $bahasa];
        
        $list 		= $this->$alias->get_datatables($where);
		$data 		= array();
        $no 		= $this->input->post('start');
        $this->db->select_max('order');
        $result = $this->db->get('m_menufront')->row();
		
		foreach ($list as $pages) {

			$b_add = ($add)?'<a class="btn bg-cyan btn-xs waves-effect" href="javascript:void(0)" title="Add" onclick="add('."'".$pages->id_peserta."'".')"><i class="material-icons">library_add</i> Add</a> ':'';
			
            $b_view = ($view)?'<a class="btn bg-teal btn-xs waves-effect" href="javascript:void(0)" title="View" onclick="view('."'".$pages->id_peserta."'".')"><i class="material-icons">chat</i> View</a> ':'';

			$b_edi 	= ($edit)?'<a class="btn bg-orange btn-xs waves-effect" href="javascript:void(0)" title="Edit" onclick="edit('."'".$pages->id_peserta."'".')"><i class="material-icons">border_color</i> Edit</a> ':'';
			
			$b_del	= ($delete)?'<a class="btn bg-red btn-xs waves-effect" href="javascript:void(0)" title="Hapus" onclick="hapus('."'".$pages->id_peserta."'".')"><i class="material-icons ">delete</i> Delete</a> ':'';

			$no++;
			$row = array();
			$row[] = $no;
            $row[] = $pages->nik;
			$row[] = $pages->nama_peserta;
			$row[] = $pages->nama_event;
			$row[] = $pages->lap;
			$row[] = $pages->jumlah;
			$row[] = $pages->total;
			
			
			$row[] = $b_add.$b_view.$b_del;
			
			$data[] = $row;
		}
	
		$csrf_name = $this->security->get_csrf_token_name();
		$csrf_hash = $this->security->get_csrf_hash(); 

		$output = array(
						"draw" 				=> $this->input->post('draw'),
						"recordsTotal" 		=> $this->$alias->count_all($where),
						"recordsFiltered" 	=> $this->$alias->count_filtered($where),
						"data" 				=> $data,
				);
		$json_data[$csrf_name] = $csrf_hash; 
		echo json_encode($output);
    }

    private function list_detail($view,$edit,$delete,$url,$add){
        $method		= $this->input->method();
		if($method != 'post'){
			$forbidden = modules::run('dashboard/login/forbidden');
    		echo $forbidden;
			exit;
		}	
        $url_exp    = explode('/',$url);
        $alias      = $url_exp[1];
        $this->load->model($url_exp[0].'/m_'.$url_exp[1],$alias);
                
        $list 		= $this->$alias->get_datatables2();
		$data 		= array();
        $no 		= $this->input->post('start');
        $this->db->select_max('order');
        $result = $this->db->get('m_menufront')->row();
		
		foreach ($list as $pages) {

			$b_add = ($add)?'<a class="btn bg-cyan btn-xs waves-effect" href="javascript:void(0)" title="Add" onclick="add('."'".$pages->id_transaction."'".')"><i class="material-icons">library_add</i> Add</a> ':'';
			
            $b_view = ($view)?'<a class="btn bg-teal btn-xs waves-effect" href="javascript:void(0)" title="View" onclick="view('."'".$pages->id_transaction."'".')"><i class="material-icons">chat</i> View</a> ':'';

			$b_edi 	= '<a class="btn bg-orange btn-xs waves-effect" href="javascript:void(0)" title="Edit" onclick="edit_scores('."'".$pages->id_transaction."'".')"><i class="material-icons">border_color</i> Edit</a>';
			
			$b_del	= ($delete)?'<a class="btn bg-red btn-xs waves-effect" href="javascript:void(0)" title="Hapus" onclick="hapus('."'".$pages->id_transaction."'".')"><i class="material-icons ">delete</i> Delete</a> ':'';

			$no++;
			$row = array();
			$row[] = $no;
            $row[] = $pages->date_created;
			$row[] = $pages->skor;
			
			$row[] = $b_edi.$b_del;
			
			$data[] = $row;
		}
	
		$csrf_name = $this->security->get_csrf_token_name();
		$csrf_hash = $this->security->get_csrf_hash(); 

		$output = array(
						"draw" 				=> $this->input->post('draw'),
						"recordsTotal" 		=> $this->$alias->count_all2(),
						"recordsFiltered" 	=> $this->$alias->count_filtered2(),
						"data" 				=> $data,
				);
		$json_data[$csrf_name] = $csrf_hash; 
		echo json_encode($output);
    }

    private function add($url){
        $url_exp    = explode('/',$url);
        $alias      = $url_exp[1];
        $this->load->model($url_exp[0].'/m_'.$url_exp[1],$alias);

        $method		= $this->input->method();
		if($method != 'post'){
			$forbidden = modules::run('dashboard/login/forbidden');
    		echo $forbidden;
			exit;
        }
        $cek_lomba 		= $this->$alias->get_transaction($this->input->post('id'));
        $cek_lap 		= $this->$alias->get_lap($this->input->post('id'));

        if($cek_lomba){
            $done = $cek_lomba->done;

            if((int)$cek_lap->lap <= (int)$cek_lomba->done){
                echo json_encode(['status' => false,'pesan' => 'Batas Rembahan anda sudah melebihi event']);
                exit;
            }
        }

        $this->validasi();
        
        $data = [
			'skor'          => $this->input->post('skor'),
			'id_peserta'    => $this->input->post('id')
        ];
        $this->global->insert('t_scores',$data);
        echo json_encode(['status' => true,'pesan' => 'Data berhasil di simpan']);
    }

    private function update(){
        $method     = $this->input->method();
        if($method != 'post'){
            $forbidden = modules::run('dashboard/login/forbidden');
            echo $forbidden;
            exit;
        }
        
        $id         = $this->input->post('id');
        $this->validasi();
        $data = [
			'skor'          => $this->input->post('skor')
        ];
        $this->global->update_tabel('t_scores',$data,['id_transaction' => $id]);
        echo json_encode(['status' => true,'pesan' => 'Data berhasil di update']);
    }

    private function get_language(){
        $data = $this->global->get_data('m_event',['status_event' => 1])->result();
        if($data){
            $result = array_merge(['status' => true,'result' => (array)$data]);
            echo json_encode($result);
        }else{
            echo json_encode(['status' => false,'pesan' => 'data tidak ditemukan']);
        }
    }

    private function delete($id){
        $this->global->update_tabel('m_config',['delete' => 1],['ID_CONFIG' => $id]);
        echo json_encode(['status' => true,'pesan' => 'Data berhasil di hapus']);
    }

    private function detail($id){
        $data = $this->global->get_data('t_scores',['id_peserta' => $id])->row();
        if($data){
            $result = array_merge(['status' => true],(array)$data);
            echo json_encode($result);
        }else{
            echo json_encode(['status' => false,'pesan' => 'data tidak ditemukan']);
        }
    }

    private function detail_scores($id){
        $data = $this->global->get_data('t_scores',['id_transaction' => $id])->row();
        if($data){
            $result = array_merge(['status' => true],(array)$data);
            echo json_encode($result);
        }else{
            echo json_encode(['status' => false,'pesan' => 'data tidak ditemukan']);
        }
    }

    private function validasi(){
        $data                   = array();
        $data['error_string']   = array();
        $data['inputerror']     = array();
        $data['status']         = TRUE;

        if($this->input->post('skor') == ''){
            $data['inputerror'][] = 'skor';
            $data['error_string'][] = 'Skor harus di isi';
            $data['status'] = FALSE;
        }

        if($this->input->post('id') == ''){
            $data['inputerror'][] = 'id';
            $data['error_string'][] = 'Kode harus di isi';
            $data['status'] = FALSE;
        }
   
        if($data['status'] === FALSE){
            echo json_encode($data);
            exit();
        }
    }

}
