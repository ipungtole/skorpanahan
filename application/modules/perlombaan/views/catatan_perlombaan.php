<?php
echo link_tag('assets/admin/' . $a_theme . '/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css');
echo link_tag('assets/admin/' . $a_theme . '/plugins/bootstrap-datepicker/css/bootstrap-datepicker.css');

?>
<section class="content">
	<div class="container-fluid">
		<div class="block-header">
			<h2>
				<ol class="breadcrumb breadcrumb-bg-pink">
					<li><a href="javascript:void(0);"><i class="material-icons">home</i> Home</a></li>
					<?php
					$aks = $akses[0]['link_menu'];
					if ($aks) {
						$exp = explode('/', $aks);
						$last = $this->uri->total_segments();
						$end = $this->uri->segment($last);
						foreach ($exp as $key) {
							if ($key == $end) {
								$d 	= 'class="active"';
								$a 	= '';
								$a1 = '';
								$en = $akses[0]['nama_menu'];
							} else {
								$a 	= '<a href="javascript:void(0);">';
								$d 	= '';
								$a1 = '</a>';
								$en = $key;
							}
							echo '<li ' . $d . '> ' . $a . ucwords(str_replace('_', ' ', $en)) . $a1 . '</li>';
						}
					}
					?>
				</ol>
			</h2>
		</div>
		<!-- Basic Examples -->
		<div class="row clearfix">
			<?php
				if($lang){
			?>
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<div class="card">
						<div class="header">
							<h2>
								Pilih Event
							</h2>
						</div>
						<div class="body">
							<form action="#" id="form-filter" class="form-horizontal">
								<?= $csrf ?>
								<input type="hidden" value="" name="id_detail"/> 
								<div class="form-body">
									<div class="form-group">
										<label class="control-label col-md-3"> Event </label>
										<div class="col-md-4">
											<select name="language" id="bahasa" class="form-control">
											</select>
											<span class="help-block"></span>
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-md-3"></label>
										<div class="col-md-7">
											<button type="button" id="btn-filter" class="btn btn-primary"><i class="material-icons">search</i> Pilih</button>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			<?php
				}
			?>
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="card">
					<div class="header">
						<h2>
							<?= $akses[0]['nama_menu'] ?>
						</h2>

					</div>
					<div class="body">

						<div class="table-responsive">
							<table id="list_data" style="width:100%" class="table table-bordered table-striped table-hover dataTable table-responsive">
								<thead>
									<tr>
										<th>No</th>
										<th>NIK</th>
										<th>Nama</th>
										<th>Event</th>
										<th>Putaran</th>
										<th>Lap</th>
										<th>Total Skor</th>
										<th>Action</th>
									</tr>
								</thead>
							</table>
						</div>

					</div>
				</div>
			</div>
		</div>
		<!-- #END# Basic Examples -->

	</div>
</section>


<div class="modal fade" id="modals_detail" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title" id="defaultModalLabel">Modal title</h4>
			</div>
			<div class="modal-body">
				<table id="list_data_detail" style="width:100%" class="table table-bordered table-striped table-hover dataTable table-responsive">
					<thead>
						<tr>
							<th>No</th>
							<th>Tanggal</th>
							<th>Skor</th>
							<th>Action</th>
						</tr>
					</thead>
				</table>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="modals" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title" id="defaultModalLabel">Modal title</h4>
			</div>
			<div class="modal-body">
				<form id="form" class="form-horizontal">
					<?= $csrf ?>
					<div class="form-group">
						<label class="control-label col-md-3"> Skor</label>
						<div class="col-md-9">
							<input type="text" name="skor" id="skor" class="form-control" placeholder="Skor">
							<span class="help-block"></span>
							<input type="hidden" name="id" id="id">
							<input type="hidden" name="language" id="language">
						</div>
					</div>
					
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" onclick="simpan()" class="btn btn-link waves-effect">SIMPAN</button>
				<button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
			</div>
		</div>
	</div>
</div>

<script src="<?php echo base_url('assets/admin/' . $a_theme . '/plugins/jquery-datatable/jquery.dataTables.js') ?>"></script>
<script src="<?php echo base_url('assets/admin/' . $a_theme . '/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js') ?>"></script>
<script src="<?php echo base_url('assets/admin/' . $a_theme . '/plugins/momentjs/moment.js') ?>"></script>
<script src="<?php echo base_url('assets/admin/' . $a_theme . '/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js') ?>"></script>
<script src="<?php echo base_url('assets/admin/' . $a_theme . '/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js') ?>"></script>
<script type="text/javascript">
	var save_method; //for save method string
	var table;
	var base_url = '<?php echo base_url(); ?>';
	// var hash2       ;
	var hash = '<?= $this->security->get_csrf_hash(); ?>';
	
	bahasa();
	$('#modul-list').hide();
	$('#pages-list').hide();
	$('#links').hide();
	$(function() {
		// $("input").change(function() {
		// 	$(this).parent().parent().removeClass('has-error');
		// 	$(this).next().empty();
		// });

		

		table = $('#list_data').DataTable({
			"responsive": true,
			"processing": true, //Feature control the processing indicator.
			"serverSide": true, //Feature control DataTables' server-side processing mode.
			"order": [], //Initial no order.

			// Load data for the table's content from an Ajax source
			"ajax": {
				"url": "<?php echo site_url($url . '?act=list') ?>",
				"type": "POST",
				"data": function ( data ) {
					data.rania_altha	= hash;
					data.event			= $('#bahasa').val();
				},
			},

			//Set column definition initialisation properties.
			"columnDefs": [{
				"targets": [-1], //last column
				"orderable": false, //set not orderable
			}, ],

		});
		$('#btn-filter').click(function(){ //button filter event click
			table.ajax.reload(null,false);  //just reload table
		});

		$('#btn-reset').click(function(){ //button reset event click
			$('#form-filter')[0].reset();
			$('[name="nomor_stt"]').val(0).trigger('chosen:updated');
			table.ajax.reload(null,false);  //just reload table
		});

	});

	function bahasa(){
		fcb = $("[name='language']");
		$.ajax({
				url : "<?php echo site_url($url.'?act=bahasa')?>",
				type : 'GET', 
				dataType : 'json',
				}).done(function(response){
					fcb.html("<option value='0'>--- Pilih Event ---</option>");
					if(response.result){
						for(i=0;i<response.result.length;i++){
							var optmrtg = "<option value='"+response.result[i]['id_event']+"' ";
							optmrtg += " >"+response.result[i]['nama_event']+"</option>";
							fcb.append(optmrtg).selectpicker('refresh');

						}
					}                        
				});
	}


	function add(param) {
		$("[name='id']").val(param);
		$('.form-group').removeClass('has-error'); // clear error class
		$('.help-block').empty(); // clear error string
		$('#form').trigger("reset");
		bahasa = $('#bahasa').val();
		$('#language').val(bahasa);
		$('#modals').modal('show');
		$('.modal-title').text('Tambah <?= $akses[0]['nama_menu'] ?> Baru');
		save_method = 'add';
	}

	function edit(param) {
		detail(param);
		save_method = 'edit';
		$('#modals').modal('show');
		$('.modal-title').text('Edit <?= $akses[0]['nama_menu'] ?> ');
	}

	function edit_scores(param) {
		detail_scores(param);
		save_method = 'edit';
		$('#modals').modal('show');
		$('.modal-title').text('Edit <?= $akses[0]['nama_menu'] ?> ');
	}

	function detail(id) {
		var select;
		$.ajax({
			url: "<?php echo site_url($url . '?act=detail&id=') ?>" + id,
			type: 'GET',
			dataType: 'json',
			success: function(data) {
				$("[name='id']").val(data.id_peserta);
				
			}
		});
	}

	function detail_scores(id) {
		var select;
		$.ajax({
			url: "<?php echo site_url($url . '?act=detail_scores&id=') ?>" + id,
			type: 'GET',
			dataType: 'json',
			success: function(data) {
				$("[name='id']").val(data.id_transaction);
				$("[name='skor']").val(data.skor);
				
			}
		});
	}


	function simpan() {
		$('#btnSave').text('saving...'); //change button text
		$('#btnSave').attr('disabled', true); //set button disable 
		var urls;

		var formData = new FormData($('#form')[0]);
		if (save_method == 'add') {
			urls = '<?php echo site_url($url . '?act=add') ?>';
		} else {
			urls = '<?php echo site_url($url . '?act=edit') ?>';
		}
		// ajax adding data to database
		$.ajax({
			url: urls,
			type: "POST",
			data: formData,
			contentType: false,
			processData: false,
			dataType: "JSON",
			success: function(data) {

				if (data.status) { //if success close modal and reload ajax table
					if (save_method == 'add') {

						$.confirm({
							icon: 'fa fa-smile-o',
							title: 'Yeaah....',
							content: data.pesan,
							theme: 'modern',
							closeIcon: true,
							type: 'green',
							animation: 'rotate',
							buttons: {
								IsiLagi: function() {
									$('#form').trigger("reset");
									table.ajax.reload(null, false);
								},
								Tidak: function() {
									$('#modals').modal('hide');
									table.ajax.reload(null, false);
								}
							}
						});

					} else {
						$.confirm({
							icon: 'fa fa-smile-o',
							title: 'Yeaah....',
							content: data.pesan,
							theme: 'modern',
							closeIcon: true,
							type: 'orange',
							animation: 'rotate',
							buttons: {
								Ok: function() {
									$('#modals').modal('hide');
									table.ajax.reload(null, false);
								}
							}
						});
					}

				} else if(data.status == false){
					$.confirm({
							icon: 'fa fa-frown-o',
							title: 'Oh Noo....',
							content: data.pesan,
							theme: 'modern',
							closeIcon: true,
							type: 'red',
							animation: 'rotate',
							buttons: {
								Ok: function() {
									$('#modals').modal('hide');
									table.ajax.reload(null, false);
								}
							}
						});
				}else {
					for (var i = 0; i < data.inputerror.length; i++) {
						if (data.inputerror[i] == 'cabang' || data.inputerror[i] == 'nama_kapal') {
							$('[name="' + data.inputerror[i] + '"]').parent().addClass('error'); //select parent twice to select div form-group class and add has-error class
							$('[name="' + data.inputerror[i] + '"]').next().next().text(data.error_string[i]); //select span help-block class set text error string
						} else {
							$('[name="' + data.inputerror[i] + '"]').parent().parent().addClass('has-error'); //select parent twice to select div form-group class and add has-error class
							$('[name="' + data.inputerror[i] + '"]').next().text(data.error_string[i]);
						}
					}




					$('#btnSave').html('<i class="material-icons">save</i>  Simpan'); //change button text
					$('#btnSave').attr('disabled', false); //set button enable 
				}
			},
			error: function(jqXHR, textStatus, errorThrown) {

			}
		});
	}

	function order(id,param){
		$.ajax({
			url: "<?php echo site_url($url . '?act=order&urutan=') ?>" + id +'&type='+ param,
			type: 'GET',
			dataType: 'json',
			success: function(data) {
				table.ajax.reload(null, false);
			}
		});
	}

	function view(id){
		$('.modal-title').text('Detail <?= $akses[0]['nama_menu'] ?>');
		$('#modals_detail').modal('show');
		table = $('#list_data_detail').DataTable({
			"responsive": true,
			"bDestroy":true,
			"paging": false,
			"searching": false,
			"processing": true, //Feature control the processing indicator.
			"serverSide": true, //Feature control DataTables' server-side processing mode.
			"order": [], //Initial no order.

			// Load data for the table's content from an Ajax source
			"ajax": {
				"url": "<?php echo site_url($url . '?act=list_detail&peserta=') ?>"+id,
				"type": "POST",
				"data": function ( data ) {
					data.rania_altha	= hash;
				},
			},

			//Set column definition initialisation properties.
			"columnDefs": [{
				"targets": [-1], //last column
				"orderable": false, //set not orderable
			}, ],

		});
	}

	function hapus(id) {
		$.confirm({
			icon: 'fa fa-trash',
			title: 'Hapus',
			content: 'Apakah Anda Akan Menghapus Data ini ?',
			theme: 'modern',
			closeIcon: true,
			type: 'red',
			animation: 'rotate',
			buttons: {
				Ya: function() {
					$.ajax({
						url: '<?php echo site_url($url . '?act=delete&id=') ?>' + id,
						type: "GET",
						dataType: "JSON",
						success: function(data) {
							if (data.status) {
								var icon = 'fa fa-check';
								var judul = 'Hapus Data Berhasil';
								var warna = 'green';
							} else {
								var icon = 'fa fa-times';
								var judul = 'Hapus Data Gagal';
								var warna = 'red';

							}
							$.alert({
								icon: icon,
								title: judul,
								content: '' + data.pesan + '',
								theme: 'modern',
								closeIcon: true,
								type: warna,
								animation: 'rotate'
							});
							table.ajax.reload(null, false);
						},
						error: function(jqXHR, textStatus, errorThrown) {
							alert('Error get data from ajax');
						}
					});
				},

				Tidak: function() {

				}
			}
		});
	}
</script>
