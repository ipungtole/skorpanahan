﻿<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>403 | <?= $title ?></title>
    <!-- Favicon-->
     <?php 
        echo link_tag('assets/admin/'.$a_theme.'/iconfont/material-icons.css');
         echo link_tag('assets/admin/'.$a_theme.'/plugins/bootstrap/css/bootstrap.css');
        echo link_tag('assets/admin/'.$a_theme.'/css/style.css');
        echo link_tag('assets/admin/'.$a_theme.'/plugins/node-waves/waves.css');
    ?>
</head>

<body class="four-zero-four">
    <div class="four-zero-four-container">
        <div class="error-code">403</div>
        <div class="error-message">This page acess is not allowed</div>
        <div class="button-place">
            <a href="<?= base_url() ?>" class="btn btn-default btn-lg waves-effect">GO TO HOMEPAGE</a>
        </div>
    </div>

    <!-- Jquery Core Js -->
    <script src="../../plugins/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core Js -->
    <script src="../../plugins/bootstrap/js/bootstrap.js"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="../../plugins/node-waves/waves.js"></script>
</body>

</html>