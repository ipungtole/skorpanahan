<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title><?= $title ?></title>
    <!-- Favicon-->
    <link rel="icon" href="favicon.ico" type="image/x-icon">

    <!-- Google Fonts -->
    <!-- <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css"> -->
    <!-- <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css"> -->

    <?php 
        echo link_tag('assets/admin/'.$a_theme.'/iconfont/material-icons.css');
        echo link_tag('assets/admin/'.$a_theme.'/plugins/bootstrap/css/bootstrap.css');
        echo link_tag('assets/admin/'.$a_theme.'/plugins/node-waves/waves.css');
        echo link_tag('assets/admin/'.$a_theme.'/plugins/animate-css/animate.css');
        echo link_tag('assets/admin/'.$a_theme.'/plugins/morrisjs/morris.css');
        echo link_tag('assets/admin/'.$a_theme.'/css/style.css');
        echo link_tag('assets/admin/'.$a_theme.'/css/themes/all-themes.css');
        echo link_tag('assets/admin/'.$a_theme.'/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css');
        echo link_tag('assets/admin/'.$a_theme.'/plugins/bootstrap-select/css/bootstrap-select.css');
        echo link_tag('assets/admin/'.$a_theme.'/plugins/jconfirm/jquery-confirm.css');
    ?>
    <!-- Jquery Core Js -->
    <script src="<?php echo base_url('assets/admin/'.$a_theme.'/plugins/jquery/jquery.min.js')?>"></script>
    
    <!-- Bootstrap Core Js -->
    <script src="<?php echo base_url('assets/admin/'.$a_theme.'/plugins/bootstrap/js/bootstrap.js')?>"></script>
    
    <!-- Select Plugin Js -->
    <script src="<?php echo base_url('assets/admin/'.$a_theme.'/plugins/bootstrap-select/js/bootstrap-select.js')?>"></script>
    
    <!-- Slimscroll Plugin Js -->
    <script src="<?php echo base_url('assets/admin/'.$a_theme.'/plugins/jquery-slimscroll/jquery.slimscroll.js')?>"></script>
    
</head>

<body class="theme-red">
    <!-- Page Loader -->
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="preloader">
                <div class="spinner-layer pl-red">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
            <p>Please wait...</p>
        </div>
    </div>
    <!-- #END# Page Loader -->
    <!-- Overlay For Sidebars -->
    <div class="overlay"></div>
    <!-- #END# Overlay For Sidebars -->
    <!-- Search Bar -->
    <div class="search-bar">
        <div class="search-icon">
            <i class="material-icons">search</i>
        </div>
        <input type="text" placeholder="START TYPING...">
        <div class="close-search">
            <i class="material-icons">close</i>
        </div>
    </div>
    <!-- #END# Search Bar -->
    <!-- Top Bar -->
    <nav class="navbar">
        <div class="container-fluid">
            <div class="navbar-header">
                <a href="javascript:void(0);" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false"></a>
                <a href="javascript:void(0);" class="bars"></a>
                <a class="navbar-brand" href="<?= base_url() ?>"><?= $app_name ?></a>
            </div>
        </div>
    </nav>
    <!-- #Top Bar -->