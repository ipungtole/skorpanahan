
    
    <!-- Waves Effect Plugin Js -->
    <script src="<?php echo base_url('assets/admin/'.$a_theme.'/plugins/node-waves/waves.js')?>"></script>
    
    <!-- Jquery CountTo Plugin Js -->
    <script src="<?php echo base_url('assets/admin/'.$a_theme.'/plugins/jquery-countto/jquery.countTo.js')?>"></script>
    
    <!-- Morris Plugin Js -->
    <script src="<?php echo base_url('assets/admin/'.$a_theme.'/plugins/raphael/raphael.min.js')?>"></script>
    <script src="<?php echo base_url('assets/admin/'.$a_theme.'/plugins/morrisjs/morris.js')?>"></script>
    
    <!-- ChartJs -->
    <script src="<?php echo base_url('assets/admin/'.$a_theme.'/plugins/chartjs/Chart.bundle.js')?>"></script>
    
    <!-- Flot Charts Plugin Js -->
    <!-- <script src="plugins/flot-charts/jquery.flot.js"></script>
    <script src="plugins/flot-charts/jquery.flot.resize.js"></script>
    <script src="plugins/flot-charts/jquery.flot.pie.js"></script>
    <script src="plugins/flot-charts/jquery.flot.categories.js"></script>
    <script src="plugins/flot-charts/jquery.flot.time.js"></script> -->
    
    <!-- Sparkline Chart Plugin Js -->
    <script src="<?php echo base_url('assets/admin/'.$a_theme.'/plugins/jquery-sparkline/jquery.sparkline.js')?>"></script>
    <script src="<?php echo base_url('assets/admin/'.$a_theme.'/plugins/jconfirm/jquery-confirm.js')?>"></script>
    
    <!-- Custom Js -->
    <script src="<?php echo base_url('assets/admin/'.$a_theme.'/js/admin.js')?>"></script>
    <script src="<?php echo base_url('assets/admin/'.$a_theme.'/js/pages/index.js')?>"></script>
    
    <!-- Demo Js -->
    <script src="<?php echo base_url('assets/admin/'.$a_theme.'/js/demo.js')?>"></script>
    <script>
        function logout(){
            $.confirm({
					icon: 'fa fa-sign-out',
					title: 'Logout ?',
					content: 'Apakah Anda Akan Keluar Dari Aplikasi ?',
					theme: 'modern',
					closeIcon: true,
					type: 'red',
					animation: 'rotate',
					buttons: {
							Ya: function () {
							    window.location.href = "<?= base_url('dashboard/login/logout') ?>";
							},


							Tidak: function () {
								$.alert('Tidak Jadi Deh...!');
							}
						}
				});
        }
    </script>
</body>

</html>
