<!DOCTYPE html>
<html lang="en">
<head>
	<title><?= $title ?></title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="images/icons/favicon.ico"/>
<!--===============================================================================================-->
	<!-- <link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css"> -->
	<!-- bootstrap & fontawesome -->
	<?php 
		echo link_tag('assets/'.$theme.'/vendor/bootstrap/css/bootstrap.min.css');
		echo link_tag('assets/'.$theme.'/fonts/font-awesome-4.7.0/css/font-awesome.min.css');
		echo link_tag('assets/'.$theme.'/fonts/Linearicons-Free-v1.0.0/icon-font.min.css');
		echo link_tag('assets/'.$theme.'/vendor/animate/animate.css');
		echo link_tag('assets/'.$theme.'/vendor/css-hamburgers/hamburgers.min.css');
		echo link_tag('assets/'.$theme.'/vendor/animsition/css/animsition.min.css');
		echo link_tag('assets/'.$theme.'/vendor/select2/select2.min.css');
		echo link_tag('assets/'.$theme.'/vendor/daterangepicker/daterangepicker.css');
		echo link_tag('assets/'.$theme.'/css/util.css');
		echo link_tag('assets/'.$theme.'/css/main.css');
	?>
</head>
<body>
	
	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100">
				
				<form method="post" id="login-form" class="login100-form validate-form">
					<?= $csrf ?>
					<span class="login100-form-title p-b-34">
						Account Login<br><br>
						<div id="error">
							<!-- error will be shown here ! -->
						</div>
					</span>
					
					<div class="wrap-input100 rs1-wrap-input100 validate-input m-b-20" data-validate="Type user name">
						<input id="first-name" class="input100" type="text" name="username" placeholder="User name">
						<span class="focus-input100"></span>
					</div>
					<div class="wrap-input100 rs2-wrap-input100 validate-input m-b-20" data-validate="Type password">
						<input class="input100" type="password" name="password" placeholder="Password">
						<span class="focus-input100"></span>
					</div>
					
					<div class="container-login100-form-btn">
						
						<button type="submit" id="btn-login" class="login100-form-btn">
							Sign in
						</button>
					</div>

				</form>

				<div class="login100-more" style="background-image: url('<?= base_url() ?>assets/theme1/images/backgroud.jpg');"></div>
			</div>
		</div>
	</div>
	<div id="dropDownSelect1"></div>
	

	<script type="text/javascript" src="<?php echo base_url('assets/'.$theme.'/vendor/jquery/jquery-3.2.1.min.js');?>" ></script>
	<script type="text/javascript" src="<?php echo base_url('assets/'.$theme.'/vendor/animsition/js/animsition.min.js');?>" ></script>
	<script type="text/javascript" src="<?php echo base_url('assets/'.$theme.'/vendor/bootstrap/js/popper.js');?>" ></script>
	<script type="text/javascript" src="<?php echo base_url('assets/'.$theme.'/vendor/bootstrap/js/bootstrap.min.js');?>" ></script>
	<script type="text/javascript" src="<?php echo base_url('assets/'.$theme.'/js/jquery.validate.min.js');?>" ></script>


	<script>
	$('document').ready(function(){ 
	
		/* validation */
		$("#login-form").validate({
		rules:
		{
				password: {
				required: true,
				},
				username: {
				required: true,
				},
		},
		messages:
		{
				password:{
						required: "Masukkan Password"
						},
				username: "Masukkan Username",
		},
		submitHandler: submitForm	
		});  
		/* validation */
		
		/* login submit */
		function submitForm(){		
				var data = $("#login-form").serialize();
					
				$.ajax({
			type      : 'POST',
			url       : '<?php echo base_url('dashboard/login/auth');?>',
			data      : data,
			dataType  : 'JSON',
			beforeSend: function(){	
			$("#error").fadeOut();
			$("#btn-login").html('<span class="glyphicon glyphicon-transfer"></span> &nbsp; Authentication ...');
			},
			success :  function(response){
				if(response.status){
				$("#btn-login").html('<i class="fa fa-spinner fa-pulse"></i> &nbsp; Signing In ...');
				setTimeout(' window.location.href = "<?php echo base_url();?>dashboard"; ',4000);
				}else{    
					$("#error").fadeIn(1000, function(){
						$('#error').slideDown();
						$("#error").html('<div class="alert alert-danger"> <span class="glyphicon glyphicon-info-sign"></span> &nbsp; '+response.pesan+' !</div>');
										$("#btn-login").html('<i class="ace-icon fa fa-key"></i><span class="bigger-110"></span> &nbsp; Login');
						$("#error").fadeTo(2000, 500).slideUp(500, function() {
							$("#error").slideUp(500);
						});				
						
					});
				}
			}
			});
					return false;
			}
		/* login submit */
	});
	</script>

</body>
</html>