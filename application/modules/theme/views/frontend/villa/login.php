<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Sign In | <?= $title ?></title>
    <!-- Favicon-->
    <link rel="icon" href="../../favicon.ico" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
	<?php 
		echo link_tag('assets/'.$a_theme.'/plugins/bootstrap/css/bootstrap.css');
		echo link_tag('assets/'.$a_theme.'/plugins/animate-css/animate.css');
		echo link_tag('assets/'.$a_theme.'/css/style.css');
	?>
</head>

<body class="login-page">
    <div class="login-box">
        <div class="logo">
            <a href="javascript:void(0);"><?= $title ?></a>
            <small>Login Page</small>
        </div>
        <div class="card">
            <div class="body">
                <form method="post" id="login-form">
					<?= $csrf ?>
					<div class="msg">Sign in to start your session</div>
					<div id="error"></div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">person</i>
                        </span>
                        <div class="form-line">
                            <input type="text" class="form-control" name="username" placeholder="Username" required autofocus>
                        </div>
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">lock</i>
                        </span>
                        <div class="form-line">
                            <input type="password" class="form-control" name="password" placeholder="Password" required>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-5 p-t-1">
                            <!-- <input type="checkbox" name="rememberme" id="rememberme" class="filled-in chk-col-pink">
                            <label for="rememberme">Remember Me</label> -->
                        </div>
                        <div class="col-xs-6">
                            <button class="btn btn-block bg-pink waves-effect" id="btn-login" type="submit">SIGN IN</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Jquery Core Js -->
    <script src="<?php echo base_url('assets/'.$a_theme.'/plugins/jquery/jquery.min.js');?>"></script>

    <!-- Bootstrap Core Js -->
    <script src="<?php echo base_url('assets/'.$a_theme.'/plugins/bootstrap/js/bootstrap.js');?>"></script>

  
    <!-- Validation Plugin Js -->
    <script src="<?php echo base_url('assets/'.$a_theme.'/plugins/jquery-validation/jquery.validate.js');?>"></script>

    <!-- Custom Js -->
	
	<script>
	$('document').ready(function(){ 
	
		/* validation */
		$("#login-form").validate({
		rules:
		{
				password: {
				required: true,
				},
				username: {
				required: true,
				},
		},
		messages:
		{
				password:{
						required: "Masukkan Password"
						},
				username: "Masukkan Username",
		},
		submitHandler: submitForm	
		});  
		/* validation */
		
		/* login submit */
		function submitForm(){		
				var data = $("#login-form").serialize();
					
				$.ajax({
			type      : 'POST',
			url       : '<?php echo base_url('dashboard/login/auth');?>',
			data      : data,
			dataType  : 'JSON',
			beforeSend: function(){	
				$("#error").fadeOut();
				$("#btn-login").html('<i class="material-icons">swap_horiz</i> Authentication ...');
			},
			success :  function(response){
				if(response.status){
					$("#btn-login").html(' Signing In ...');
					setTimeout(' window.location.href = "<?php echo base_url();?>kpanel"; ',4000);
				}else{
					$("#error").fadeIn(1000, function(){
						$('#error').slideDown();
						$("#error").html('<div class="alert alert-danger">  '+response.pesan+' !</div>');
						$("#btn-login").html('SIGN IN');
						$("#error").fadeTo(2000, 500).slideUp(500, function() {
							$("#error").slideUp(500);
						});				
						
					});
				}
			}
			});
					return false;
			}
		/* login submit */
	});
	</script>
    <!-- <script src="../../js/pages/examples/sign-in.js"></script> -->
</body>

</html>