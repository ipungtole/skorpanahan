
<?php
$st = ($sub_title)?' : '.$sub_title:''
?>
<!doctype html>
<html class="no-js" lang="zxx">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title><?= $title.$st ?></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="manifest" href="site.webmanifest">
		<link rel="shortcut icon" type="image/png" href="<?= 'images/'.$favicon ?>">

        <!-- CSS here -->
        <?php 
            echo link_tag('assets/web/'.$themes.'/css/bootstrap.min.css');
            echo link_tag('assets/web/'.$themes.'/css/owl.carousel.min.css');
            echo link_tag('assets/web/'.$themes.'/css/flaticon.css');
            echo link_tag('assets/web/'.$themes.'/css/slicknav.css');
            echo link_tag('assets/web/'.$themes.'/css/animate.min.css');
            echo link_tag('assets/web/'.$themes.'/css/magnific-popup.css');
            echo link_tag('assets/web/'.$themes.'/css/fontawesome-all.min.css');
            echo link_tag('assets/web/'.$themes.'/css/themify-icons.css');
            echo link_tag('assets/web/'.$themes.'/css/slick.css');
            echo link_tag('assets/web/'.$themes.'/css/nice-select.css');
            echo link_tag('assets/web/'.$themes.'/css/style.css');
        
        ?>
   </head>

   <body>
    <!-- Preloader Start -->
    
    <div id="preloader-active">
        <div class="preloader d-flex align-items-center justify-content-center">
            <div class="preloader-inner position-relative">
                <div class="preloader-circle"></div>
                <div class="preloader-img pere-text">
                    <img src="images/preload.svg" alt="">
                </div>
            </div>
        </div>
    </div>
    <!-- Preloader Start -->
    <header>
        <!-- Header Start -->
       <div class="header-area">
            <div class="main-header ">
                <!-- <div class="header-top top-bg d-none d-lg-block">
                   <div class="container">
                    <div class="row justify-content-between align-items-center">
                        <div class="col-lg-8">
                            <div class="header-info-left">
                                <ul>                          
                                    <li><?= $email ?></li>
                                    <li><?= $phone ?></li>
                                    <li><?= $alamat ?></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="header-info-right f-right">
                                <ul class="header-social">
                                    <?php
                                        $sosmed = $this->global->get_data_order('m_social_media',['deleted' => null,'status' => 1],'order','asc')->result();
                                        if($sosmed){
                                            foreach ($sosmed as $sos) {
                                                echo '<li><a href="'.$sos->url_sosmed.'" target="blank">'.$sos->icon.'</a></li>';
                                            }
                                        }
                                    ?>
                                </ul>
                            </div>
                        </div>
                       </div>
                   </div>
                </div> -->
               <div class="header-bottom  header-sticky">
                    <div class="container">
                        <div class="row align-items-center">
                            <!-- Logo -->
                            <div class="col-xl-3 col-lg-3 col-md-1">
                                <div class="logo">
                                   
                                    <br>
                                  <a href="<?= base_url() ?>"><?= img(array('src'=>base_url('images/'.$logo), 'alt'=> 'Logo')) ?></a>
                                  
                                </div>
                            </div>
                            <div class="col-xl-9 col-lg-9 col-md-9">
                                <!-- Main-menu -->
                                <div class="main-menu f-left d-none d-lg-block">
                                    <div class="alamat">
                                        <ul>                          
                                            <li><?= $email ?></li>
                                            <li><?= $phone ?></li>
                                            <li><?= $alamat ?></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="main-menu f-right d-none d-lg-block">
                                    <nav>               
                                        <?php
                                            echo '<ul id="navigation">';
                                                foreach ($menu as $parent) {
                                                    $submenu = $this->front->menu(['parent' => $parent->id_menu]);
                                                    if($submenu){
                                                        $link = ($parent->slug)?$parent->slug:$parent->nama_modul;
                                                        echo '<li><a href="'.base_url($link).'">'.$parent->nama_menu.'</a>';
                                                        echo'<ul class="submenu">'; 
                                                        foreach ($submenu as $child) {
                                                            $slink = ($child->slug)?$child->slug:$child->nama_modul;
                                                            echo '<li><a href="'.base_url($slink).'">'.$child->nama_menu.'</a></li>';
                                                        }
                                                        echo'</ul>';
                                                        echo'</li>';
                                                    }else{
                                                        $link = ($parent->slug)?$parent->slug:$parent->nama_modul;
                                                        echo '<li><a href="'.base_url($link).'">'.$parent->nama_menu.'</a></li>';
                                                    }
                                                }
                                            // var_dump($bahasa);
                                            $flag = [
                                                'src'    => base_url('plugins/images/flag/'.$lang['flag']),
                                                'width'  => '21',
                                            ];
                                            // echo '<li><a href="#"> '.img($flag).' | '.$lang['language'].' </a>';
                                            //     echo'<ul class="submenu">'; 
                                            //         foreach ($bahasa as $lang) {
                                            //             $flags = [
                                            //                 'src'    => base_url('plugins/images/flag/'.$lang->flag),
                                            //                 'width'  => '21',
                                            //             ];
                                            //             echo '<li><a href="#" onclick="language('."'".$lang->code."'".');"> '.img($flags).' | '.$lang->language.'</a></li>';
                                            //         }
                                            //     echo'</ul>';
                                            echo '</li>';
                                            echo '</ul>'
                                        ?>

                                    </nav>
                                    
                                </div>
                                
                            </div>
                            <!-- Mobile Menu -->
                            
                            <div class="col-12">
                                <div class="mobile_menu d-block d-lg-none"></div>
                                
                            </div>
                            
                        </div>
                    </div>
                    
               </div>
            </div>
       </div>
        <!-- Header End -->
    </header>

    <main>
