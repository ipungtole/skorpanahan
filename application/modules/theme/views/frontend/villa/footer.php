</main>
    <footer>
        <!-- Footer Start-->
        <div class="footer-area footer-bg" data-background="assets/img/service/footer_bg.jpg">
            <div class="container">
                <!-- Footer bottom -->
                <div class="row pt-padding">
                 <div class="col-xl-7 col-lg-7 col-md-7">
                    <div class="footer-copy-right">
                         <p>
                            Copyright &copy;<?= (date('Y') != 2020)?"2020 - ".date('Y'):date('Y') ?> All rights reserved | <?= $title ?> Themes By <a href="https://colorlib.com" target="_blank">Colorlib</a>
  
                    </div>
                 </div>
                  <div class="col-xl-5 col-lg-5 col-md-5">
                        <!-- social -->
                        <div class="footer-social f-right">
                            <?php
                                $sosmed = $this->global->get_data_order('m_social_media',['deleted' => null,'status' => 1],'order','asc')->result();
                                if($sosmed){
                                    foreach ($sosmed as $sos) {
                                        echo '<a href="'.$sos->url_sosmed.'" target="blank">'.$sos->icon.'</a>';
                                    }
                                }
                            ?>
                        </div>
                 </div>
             </div>
            </div>
        </div>
        <!-- Footer End-->
    </footer>

    <!-- Modal -->
    <div class="modal fade" id="register_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <form>
                <div class="form-group row">
                    <label for="inputPassword3" class="col-sm-2 col-form-label">Nama</label>
                    <div class="col-sm-10">
                    <input type="password" class="form-control" id="inputPassword3" placeholder="Password">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputPassword3" class="col-sm-2 col-form-label">NIK</label>
                    <div class="col-sm-10">
                    <input type="password" class="form-control" id="inputPassword3" placeholder="Password">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputPassword3" class="col-sm-2 col-form-label"></label>
                    <div class="col-sm-10">
                    <a href="#" class="genric-btn primary-border circle">Simpan</a>
                    </div>
                </div>
                </form>
        </div>
        </div>
    </div>
    </div>

	<!-- JS here -->
	
        <!-- All JS Custom Plugins Link Here here -->
        <script src="<?php echo base_url('assets/web/'.$themes.'/js/vendor/modernizr-3.5.0.min.js')?>"></script>
        
		<!-- Jquery, Popper, Bootstrap -->
        <script src="<?php echo base_url('assets/web/'.$themes.'/js/vendor/jquery-1.12.4.min.js')?>"></script>
        <script src="<?php echo base_url('assets/web/'.$themes.'/js/popper.min.js')?>"></script>
        <script src="<?php echo base_url('assets/web/'.$themes.'/js/bootstrap.min.js')?>"></script>
	    <!-- Jquery Mobile Menu -->
        <script src="<?php echo base_url('assets/web/'.$themes.'/js/jquery.slicknav.min.js')?>"></script>
        
		<!-- Jquery Slick , Owl-Carousel Plugins -->
        <script src="<?php echo base_url('assets/web/'.$themes.'/js/owl.carousel.min.js')?>"></script>
        <script src="<?php echo base_url('assets/web/'.$themes.'/js/slick.min.js')?>"></script>
		<!-- One Page, Animated-HeadLin -->
        <script src="<?php echo base_url('assets/web/'.$themes.'/js/wow.min.js')?>"></script>
        <script src="<?php echo base_url('assets/web/'.$themes.'/js/animated.headline.js')?>"></script>
        <script src="<?php echo base_url('assets/web/'.$themes.'/js/jquery.magnific-popup.js')?>"></script>
        
		<!-- Scrollup, nice-select, sticky -->
        <script src="<?php echo base_url('assets/web/'.$themes.'/js/jquery.scrollUp.min.js')?>"></script>
        <script src="<?php echo base_url('assets/web/'.$themes.'/js/jquery.nice-select.min.js')?>"></script>
        <script src="<?php echo base_url('assets/web/'.$themes.'/js/jquery.sticky.js')?>"></script>
        
        <!-- contact js -->
		<!-- Jquery Plugins, main Jquery -->	
        <script src="<?php echo base_url('assets/web/'.$themes.'/js/plugins.js')?>"></script>
        <script src="<?php echo base_url('assets/web/'.$themes.'/js/main.js')?>"></script>
       <script type="text/javascript">

        $( "#register" ).click(function() {
            // alert( "Handler for .click() called." );
            $('#register_modal').modal('show');
        });
        var menu = document.getElementById('menu');
        var closeIcon = document.getElementById("closeIcon");

        // menu.addEventListener('click', handleMenuClick);

        function handleMenuClick(event) {
        if (event.target instanceof HTMLAnchorElement) {
            closeIcon.checked = false;
        }
        }

        function language(code){
            $.ajax({
                type: 'GET',
                url: "<?= base_url('language?select=') ?>"+ code,
                dataType: 'json',
                success: function (data) {
                    location.reload();
                }
            });
        }
       </script>
    </body>
</html>