<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Page extends MY_Controller {
    function __construct(){
		parent::__construct();
		$this->load->library('template');
		$this->load->model('dashboard/m_global','global');
		
        $akses = modules::run('dashboard/login/role_acess');
        
    	if(!$akses){
    		$forbidden = modules::run('dashboard/login/forbidden');
    		echo $forbidden;
    		exit;
    	}

    }

    public function index(){
        $act            = $this->input->get('act');
        $data['rules']  = modules::run('dashboard/login/get_role_acess');
        $data['lang']   = $this->global->get_data('m_plugins',['id_plugins' => '1'],null)->row()->status;
            
    	switch ($act) {
            case 'list':
                $this->access('list_data');
    			$this->list($data['rules']->view_button,$data['rules']->edit_button,$data['rules']->delete_button,$data['rules']->link_menu);
                break;
            case 'add':
                $this->access('add_button');
                $this->add();
                break;
            case 'edit':
                $this->access('edit_button');
                $this->update();
                break;
            case 'delete':
                $this->access('delete_button');
                $id  = $this->input->get('id');
                $this->delete($id);
                break;
            case 'detail':
                $id  = $this->input->get('id');
                $this->detail($id);
                break;
            case 'bahasa':
                $this->get_language();
                break;
            default:
                $this->access('list_data');
				$this->template->dashboards($data['rules']->link_menu,$data);
    			break;
    	}
    }

  
    private function access($act){
        $data = modules::run('dashboard/login/get_role_acess');
        if(!$data->$act){
            $forbidden = modules::run('dashboard/login/forbidden');
            echo $forbidden;
            exit;
        }
    }

    private function list($view,$edit,$delete,$url){
        $method		= $this->input->method();
		if($method != 'post'){
			$forbidden = modules::run('dashboard/login/forbidden');
    		echo $forbidden;
			exit;
		}	
        $url_exp    = explode('/',$url);
        $alias      = $url_exp[1];
        $this->load->model($url_exp[0].'/m_'.$url_exp[1],$alias);
         $bahasa     = $this->input->post('bahasa'); 

        $where      = ['language' => $bahasa];
        $list 		= $this->$alias->get_datatables($where);
		$data 		= array();
        $no 		= $this->input->post('start');
        // $id         = '$pages->id_'.$alias;
		
		foreach ($list as $pages) {

			$b_view = ($view)?'<a class="btn bg-teal btn-xs waves-effect" href="javascript:void(0)" title="View" onclick="view('."'".$pages->id_pages."'".')"><i class="material-icons">chat</i> View</a> ':'';

			$b_edi 	= ($edit)?'<a class="btn bg-orange btn-xs waves-effect" href="javascript:void(0)" title="Edit" onclick="edit('."'".$pages->id_pages."'".')"><i class="material-icons">border_color</i> Edit</a> ':'';
			
			$b_del	= ($delete)?'<a class="btn bg-red btn-xs waves-effect" href="javascript:void(0)" title="Hapus" onclick="hapus('."'".$pages->id_pages."'".')"><i class="material-icons ">delete</i> Delete</a> ':'';

			$no++;
			$row = array();
			$row[] = $no;
			$row[] = $pages->judul;
			$row[] = $pages->slug;
			$row[] = ($pages->status)?'<i class="material-icons font-bold col-teal">done</i>':'<i class="material-icons font-bold col-pink">clear</i>';
			
			$row[] = $b_view.$b_edi.$b_del;
			
			$data[] = $row;
		}
	
		$csrf_name = $this->security->get_csrf_token_name();
		$csrf_hash = $this->security->get_csrf_hash(); 

		$output = array(
						"draw" 				=> $this->input->post('draw'),
						"recordsTotal" 		=> $this->$alias->count_all($where),
						"recordsFiltered" 	=> $this->$alias->count_filtered($where),
						"data" 				=> $data,
				);
		$json_data[$csrf_name] = $csrf_hash; 
		echo json_encode($output);
    }

    private function add(){
        $method		= $this->input->method();
		if($method != 'post'){
			$forbidden = modules::run('dashboard/login/forbidden');
    		echo $forbidden;
			exit;
        }
        
        
        $this->validasi();
		$data = [
			'judul'         => $this->input->post('judul'),
			'slug'          => $this->input->post('slug'),
			'konten'        => $this->input->post('konten'),
			'language'      => $this->input->post('language'),
			'status'        => $this->input->post('status')
		];
        $this->global->insert('m_pages',$data);
        echo json_encode(['status' => true,'pesan' => 'Data berhasil di simpan']);
    }

    private function get_language(){
        $data = $this->global->get_data('p_language',['deleted' => null])->result();
        if($data){
            $result = array_merge(['status' => true,'result' => (array)$data]);
            echo json_encode($result);
        }else{
            echo json_encode(['status' => false,'pesan' => 'data tidak ditemukan']);
        }
    }

    private function update(){
        $method     = $this->input->method();
        if($method != 'post'){
            $forbidden = modules::run('dashboard/login/forbidden');
            echo $forbidden;
            exit;
        }
        
        $id         = $this->input->post('id_page');
        $this->validasi();
        $data = [
			'judul'         => $this->input->post('judul'),
			'slug'          => $this->input->post('slug'),
			'konten'        => $this->input->post('konten'),
			'status'        => $this->input->post('status')
		];
        $this->global->update_tabel('m_pages',$data,['id_pages' => $id]);
        echo json_encode(['status' => true,'pesan' => 'Data berhasil di update']);
    }

    private function delete($id){
        $this->global->update_tabel('m_pages',['delete' => 1],['id_pages' => $id]);
        echo json_encode(['status' => true,'pesan' => 'Data berhasil di hapus']);
    }

    private function detail($id){
        $data = $this->global->get_data('m_pages',['id_pages' => $id])->row();
        if($data){
            $result = array_merge(['status' => true],(array)$data);
            echo json_encode($result);
        }else{
            echo json_encode(['status' => false,'pesan' => 'data tidak ditemukan']);
        }
    }

    private function validasi(){
        $data                   = array();
        $data['error_string']   = array();
        $data['inputerror']     = array();
        $data['status']         = TRUE;

        if($this->input->post('judul') == ''){
            $data['inputerror'][] = 'judul';
            $data['error_string'][] = 'Judul harus di isi';
            $data['status'] = FALSE;
        }

        if($this->input->post('konten') == ''){
            $data['inputerror'][] = 'konten';
            $data['error_string'][] = 'Konten harus di isi';
            $data['status'] = FALSE;
        }

      
        if($data['status'] === FALSE){
            echo json_encode($data);
            exit();
        }
    }

}
