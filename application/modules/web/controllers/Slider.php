<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Slider extends MY_Controller {
    function __construct(){
		parent::__construct();
		$this->load->library('template');
		$this->load->model('dashboard/m_global','global');
		
        $akses = modules::run('dashboard/login/role_acess');
        
    	if(!$akses){
    		$forbidden = modules::run('dashboard/login/forbidden');
    		echo $forbidden;
    		exit;
    	}

    }

    public function index(){
        $act            = $this->input->get('act');
        $data['rules']  = modules::run('dashboard/login/get_role_acess');
      
    	switch ($act) {
            case 'list':
                $this->access('list_data');
    			$this->list($data['rules']->view_button,$data['rules']->edit_button,$data['rules']->delete_button,$data['rules']->link_menu);
                break;
            case 'add':
                $this->access('add_button');
                $this->add();
                break;
            case 'edit':
                $this->access('edit_button');
                $this->update();
                break;
            case 'delete':
                $this->access('delete_button');
                $id  = $this->input->get('id');
                $this->delete($id);
                break;
            case 'detail':
                $id  = $this->input->get('id');
                $this->detail($id);
                break;
            default:
                $this->access('list_data');
				$this->template->dashboards($data['rules']->link_menu,$data);
    			break;
    	}
    }

    private function _create_thumbs($file_name){
        // Image resizing config
        $config = array(
            // Large Image
            // array(
            //     'image_library' => 'GD2',
            //     'source_image'  => './assets/images/'.$file_name,
            //     'maintain_ratio'=> FALSE,
            //     'width'         => 700,
            //     'height'        => 467,
            //     'new_image'     => './assets/images/large/'.$file_name
            //     ),
            // Medium Image
            array(
                'image_library' => 'GD2',
                'source_image'  => 'images/'.$file_name,
                'maintain_ratio'=> TRUE,
                'width'         => 1700,
                'new_image'     => './images/'.$file_name
                ),
            // Small Image
            array(
                'image_library' => 'GD2',
                'source_image'  => 'images/'.$file_name,
                'maintain_ratio'=> FALSE,
                'width'         => 100,
                'height'        => 67,
                'new_image'     => 'images/small/'.$file_name
            ));
 
        $this->load->library('image_lib', $config[0]);
        foreach ($config as $item){
            $this->image_lib->initialize($item);
            if(!$this->image_lib->resize()){
                return false;
            }
            $this->image_lib->clear();
        }
    }

    private function _do_upload(){
        $config['upload_path']          = 'images/';
		$config['allowed_types']        = 'gif|jpg|jpeg|png|svg';
		// $config['allowed_types']        = 'svg';
        $config['max_size']             = 5000; //set max size allowed in Kilobyte
        $config['max_width']            = 8000; // set max width image allowed
        $config['max_height']           = 5000; // set max height allowed
        $config['file_name']            = round(microtime(true) * 1000); //just milisecond timestamp fot unique name
 
        $this->load->library('upload', $config);

        if(!$this->upload->do_upload('gambar')){
            $data['inputerror'][] 	= 'gambar';
            $data['error_string'][] = 'Upload error: '.$this->upload->display_errors('',''); 
            $data['status'] 		= FALSE;
            echo json_encode($data);
            exit();
        }
		$rre = $this->upload->data('file_name');
		$ex = explode('.',$rre);
		if($ex[1] != 'svg'){
            $this->_create_thumbs($this->upload->data('file_name'));
        }
		
        return $this->upload->data('file_name');
    }

  
    private function access($act){
        $data = modules::run('dashboard/login/get_role_acess');
        if(!$data->$act){
            $forbidden = modules::run('dashboard/login/forbidden');
            echo $forbidden;
            exit;
        }
    }

    private function list($view,$edit,$delete,$url){
        $method		= $this->input->method();
		if($method != 'post'){
			$forbidden = modules::run('dashboard/login/forbidden');
    		echo $forbidden;
			exit;
		}	
        $url_exp    = explode('/',$url);
        $alias      = $url_exp[1];
        $this->load->model($url_exp[0].'/m_'.$url_exp[1],$alias);
        
        $list 		= $this->$alias->get_datatables();
		$data 		= array();
        $no 		= $this->input->post('start');
        // $id         = '$pages->id_'.$alias;
		
		foreach ($list as $pages) {

			$b_view = ($view)?'<a class="btn bg-teal btn-xs waves-effect" href="javascript:void(0)" title="View" onclick="view('."'".$pages->id_gallery."'".')"><i class="material-icons">chat</i> View</a> ':'';

			$b_edi 	= ($edit)?'<a class="btn bg-orange btn-xs waves-effect" href="javascript:void(0)" title="Edit" onclick="edit('."'".$pages->id_gallery."'".')"><i class="material-icons">border_color</i> Edit</a> ':'';
			
			$b_del	= ($delete)?'<a class="btn bg-red btn-xs waves-effect" href="javascript:void(0)" title="Hapus" onclick="hapus('."'".$pages->id_gallery."'".')"><i class="material-icons ">delete</i> Delete</a> ':'';

			$no++;
			$row = array();
			$row[] = $no;
			$row[] = img('images/small/'.$pages->nama_gambar.'');
			$row[] = $pages->caption_gambar;
			
			$row[] = ($pages->status)?'<i class="material-icons font-bold col-teal">done</i>':'<i class="material-icons font-bold col-pink">clear</i>';
			
			
			$row[] = $b_view.$b_edi.$b_del;
			
			$data[] = $row;
		}
	
		$csrf_name = $this->security->get_csrf_token_name();
		$csrf_hash = $this->security->get_csrf_hash(); 

		$output = array(
						"draw" 				=> $this->input->post('draw'),
						"recordsTotal" 		=> $this->$alias->count_all(),
						"recordsFiltered" 	=> $this->$alias->count_filtered(),
						"data" 				=> $data,
				);
		$json_data[$csrf_name] = $csrf_hash; 
		echo json_encode($output);
    }

    private function add(){
        $method		= $this->input->method();
		if($method != 'post'){
			$forbidden = modules::run('dashboard/login/forbidden');
    		echo $forbidden;
			exit;
        }
        
        $this->validasi();
		$data = [
			'type'              => 'slider',
			'caption_gambar'    => $this->input->post('caption'),
			'status'            => $this->input->post('status')
        ];
        if(!empty($_FILES['gambar']['name'])){
			$upload = $this->_do_upload();
			$data['nama_gambar'] = $upload;
		}
        $this->global->insert('m_gallery',$data);
        echo json_encode(['status' => true,'pesan' => 'Data berhasil di simpan']);
    }

    private function update(){
        $method     = $this->input->method();
        if($method != 'post'){
            $forbidden = modules::run('dashboard/login/forbidden');
            echo $forbidden;
            exit;
        }
        
        $id         = $this->input->post('id_gallery');
        $this->validasi();
        $data = [
            'type'              => 'slider',
			'caption_gambar'    => $this->input->post('caption'),
			'status'            => $this->input->post('status')
        ];
        if(!empty($_FILES['gambar']['name'])){
            $uploads = $this->_do_upload();
            $detail = $this->global->get_data('m_gallery',['id_gallery' => $id])->row();
            #DELETED FILES
            if(file_exists('images/'.$detail->nama_gambar)){
                unlink('images/'.$detail->nama_gambar);
            }
            if(file_exists('images/small/'.$detail->nama_gambar)){
                unlink('images/small/'.$detail->nama_gambar);
            }
            #END DELETED FILES
            
            $data['nama_gambar'] = $uploads;
        }
        
        $this->global->update_tabel('m_gallery',$data,['id_gallery' => $id]);
        echo json_encode(['status' => true,'pesan' => 'Data berhasil di update']);
    }

    private function delete($id){
        $detail = $this->global->get_data('m_gallery',['id_gallery' => $id])->row();
        #DELETED FILES
        if(file_exists('images/'.$detail->nama_gambar)){
            unlink('images/'.$detail->nama_gambar);
        }
        if(file_exists('images/small/'.$detail->nama_gambar)){
            unlink('images/small/'.$detail->nama_gambar);
        }
        #END DELETED FILES
        $this->global->update_tabel('m_gallery',['deleted' => 1],['id_gallery' => $id]);
        echo json_encode(['status' => true,'pesan' => 'Data berhasil di hapus']);
    }

    private function detail($id){
        $data = $this->global->get_data('m_gallery',['id_gallery' => $id])->row();
        if($data){
            $result = array_merge(['status' => true],(array)$data);
            echo json_encode($result);
        }else{
            echo json_encode(['status' => false,'pesan' => 'data tidak ditemukan']);
        }
    }

    private function validasi(){
        $data                   = array();
        $data['error_string']   = array();
        $data['inputerror']     = array();
        $data['status']         = TRUE;

        if($this->input->post('caption') == ''){
            $data['inputerror'][] = 'caption';
            $data['error_string'][] = 'Keterangan harus di isi';
            $data['status'] = FALSE;
        }

        if($data['status'] === FALSE){
            echo json_encode($data);
            exit();
        }
    }

}
