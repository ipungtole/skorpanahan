<?php defined('BASEPATH') OR exit('No direct script access allowed');

Class M_menu extends CI_Model {
    var $table = 'm_menufront';
    var $column_order = array('id_menu','nama_menu','url','status'); //set column field database for datatable orderable
    var $column_search = array('nama_menu','url','m_menufront.status'); //set column field database for datatable searchable just firstname , lastname , address are searchable
    var $order = array('order' => 'asc'); // default order 
 
 
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }
 
    private function _get_datatables_query($where){
         
        // $this->db->from($this->table);
        $this->db->select('
                            m_menufront.*,
                            m_modul.nama_modul,
                            m_modul.templates,
                            m_pages.slug,
                            m_pages.konten
                        ');
        $this->db->from('m_menufront');
        $this->db->join('m_modul','m_modul.id_modul = m_menufront.modul','left');
        $this->db->join('m_pages','m_pages.id_pages = m_menufront.id_pages','left');
        $this->db->where($where);
        $this->db->where(['m_menufront.delete' => null]);
        $this->db->order_by('m_menufront.order','asc');

		
        $i = 0;
     
        foreach ($this->column_search as $item) // loop column 
        {
            if($this->input->post('search')) // if datatable send POST for search
            {
                 
                if($i===0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
 
                if(count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }
         
        if(isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
 
    function get_datatables($where){
        $this ->_get_datatables_query($where);
        if($this->input->post('length') != -1)
        $this->db->limit($this->input->post('length'),$this->input->post('start'));
        $query = $this->db->get();
        return $query->result();
    }
 
    function count_filtered($where){
        $this->_get_datatables_query($where);
        $query = $this->db->get();
        return $query->num_rows();
    }
 
    public function count_all($where){
        $this->_get_datatables_query($where);
        return $this->db->count_all_results();
    }

    public function detail_menu($where){
        $this->db->select('
                            m_menufront.*,
                            m_modul.nama_modul,
                            m_modul.templates,
                            m_pages.slug
                        ');
        $this->db->from('m_menufront');
        $this->db->join('m_modul','m_modul.id_modul = m_menufront.modul','left');
        $this->db->join('m_pages','m_pages.id_pages = m_menufront.id_pages','left');
        $this->db->where($where);
        $this->db->where(['m_menufront.delete' => null,'m_menufront.status' => 1]);
        $this->db->order_by('m_menufront.order','asc');
        $query = $this->db->get();
        if($query){
            return $query;
        }else{
            return false;
        }
    }
    
}
