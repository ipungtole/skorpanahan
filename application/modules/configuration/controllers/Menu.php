<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Menu extends MY_Controller {
    function __construct(){
		parent::__construct();
		$this->load->library('template');
		$this->load->model('dashboard/m_global','global');
		
        $akses = modules::run('dashboard/login/role_acess');
        
    	if(!$akses){
    		$forbidden = modules::run('dashboard/login/forbidden');
    		echo $forbidden;
    		exit;
    	}

    }

    public function index(){
        $act            = $this->input->get('act');
        $data['rules']  = modules::run('dashboard/login/get_role_acess');
      
    	switch ($act) {
            case 'list':
                $this->access('list_data');
    			$this->list($data['rules']->view_button,$data['rules']->edit_button,$data['rules']->delete_button,$data['rules']->link_menu);
                break;
            case 'add':
                $this->access('add_button');
                $this->add();
                break;
            case 'edit':
                $this->access('edit_button');
                $this->update();
                break;
            case 'order':
                $type   = $this->input->get('type');
                $urutan = $this->input->get('urutan');
                $this->order($urutan,$type);
                break;
            case 'delete':
                $this->access('delete_button');
                $id  = $this->input->get('id');
                $this->delete($id);
                break;
            case 'detail':
                $id  = $this->input->get('id');
                $this->detail($id);
                break;
            case 'parent':
                $this->get_parent();
                break;
            default:
                $this->access('list_data');
				$this->template->dashboards($data['rules']->link_menu,$data);
    			break;
    	}
    }

  
    private function access($act){
        $data = modules::run('dashboard/login/get_role_acess');
        if(!$data->$act){
            $forbidden = modules::run('dashboard/login/forbidden');
            echo $forbidden;
            exit;
        }
    }

    private function order($urutan,$type){
        $urutan_awal = (int)$urutan;
        if(!is_numeric($urutan_awal)){
            echo 'acess denied';
            exit;    
        }
        if($type == 'up'){
            if($urutan_awal == 1){
                echo json_encode(['status' => false,'pesan' => 'Menu Awal gak bisa di naikkan lagi']);
                exit;
            }
            $di_ganti = $this->global->get_data('tbl_menu',['urutan_menu' => $urutan_awal])->row();
            $me_ganti = $this->global->get_data('tbl_menu',['urutan_menu' => (int)$urutan_awal-1])->row();
            $this->global->update_tabel('tbl_menu',['urutan_menu' => (int)$urutan_awal-1],['id_menu' => $di_ganti->id_menu]);
            $this->global->update_tabel('tbl_menu',['urutan_menu' => $urutan_awal],['id_menu' => $me_ganti->id_menu]);
            echo json_encode(['status' => true,'pesan' => 'Data berhasil di update']);
        }else{
            $this->db->select_max('urutan_menu');
            $result = $this->db->get('tbl_menu')->row();
            if($urutan_awal == $result->urutan_menu){
                echo json_encode(['status' => false,'pesan' => 'Menu Terakhir gak bisa di turunkan lagi']);
                exit;
            }

            $di_ganti = $this->global->get_data('tbl_menu',['urutan_menu' => $urutan_awal])->row();
            $me_ganti = $this->global->get_data('tbl_menu',['urutan_menu' => (int)$urutan_awal+1])->row();
            $this->global->update_tabel('tbl_menu',['urutan_menu' => (int)$urutan_awal+1],['id_menu' => $di_ganti->id_menu]);
            $this->global->update_tabel('tbl_menu',['urutan_menu' => $urutan_awal],['id_menu' => $me_ganti->id_menu]);
            echo json_encode(['status' => true,'pesan' => 'Data berhasil di update']);
         
        }
    }

    private function list($view,$edit,$delete,$url){
        $method		= $this->input->method();
		if($method != 'post'){
			$forbidden = modules::run('dashboard/login/forbidden');
    		echo $forbidden;
			exit;
		}	
        $url_exp    = explode('/',$url);
        $alias      = $url_exp[1];
        $this->load->model($url_exp[0].'/m_'.$url_exp[1],$alias);
        
        $list 		= $this->$alias->get_datatables();
		$data 		= array();
        $no 		= $this->input->post('start');
        $this->db->select_max('urutan_menu');
        $result = $this->db->get('tbl_menu')->row();
		
		foreach ($list as $pages) {

			$b_view = ($view)?'<a class="btn bg-teal btn-xs waves-effect" href="javascript:void(0)" title="View" onclick="view('."'".$pages->id_menu."'".')"><i class="material-icons">chat</i> View</a> ':'';

			$b_edi 	= ($edit)?'<a class="btn bg-orange btn-xs waves-effect" href="javascript:void(0)" title="Edit" onclick="edit('."'".$pages->id_menu."'".')"><i class="material-icons">border_color</i> Edit</a> ':'';
			
			$b_del	= ($delete)?'<a class="btn bg-red btn-xs waves-effect" href="javascript:void(0)" title="Hapus" onclick="hapus('."'".$pages->id_menu."'".')"><i class="material-icons ">delete</i> Delete</a> ':'';

			$no++;
			$row = array();
			$row[] = $no;
			$row[] = ($pages->id_parent == null)?'<strong>'.$pages->nama_menu.'</strong>':$pages->nama_menu;
			$row[] = $pages->link_menu;
            $row[] = $pages->icon_menu;
            if($no == 1){
                $row[] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a class="btn bg-deep-orange btn-xs waves-effect" href="javascript:void(0)" title="Down" onclick="order('."'".$pages->urutan_menu."'".','."'down'".')"><i class="material-icons ">keyboard_arrow_down</i></a> ';
            }else if($no == $result->urutan_menu){
                $row[] = '<a class="btn bg-light-green btn-xs waves-effect" href="javascript:void(0)" title="Up" onclick="order('."'".$pages->urutan_menu."'".','."'up'".')"><i class="material-icons ">keyboard_arrow_up</i></a>';
            }else{
                $row[] = '<a class="btn bg-light-green btn-xs waves-effect" href="javascript:void(0)" title="Up" onclick="order('."'".$pages->urutan_menu."'".','."'up'".')"><i class="material-icons ">keyboard_arrow_up</i></a> <a class="btn bg-deep-orange btn-xs waves-effect" href="javascript:void(0)" title="Down" onclick="order('."'".$pages->urutan_menu."'".','."'down'".')"><i class="material-icons ">keyboard_arrow_down</i></a> ';
            }
			
			
			$row[] = $b_view.$b_edi.$b_del;
			
			$data[] = $row;
		}
	
		$csrf_name = $this->security->get_csrf_token_name();
		$csrf_hash = $this->security->get_csrf_hash(); 

		$output = array(
						"draw" 				=> $this->input->post('draw'),
						"recordsTotal" 		=> $this->$alias->count_all(),
						"recordsFiltered" 	=> $this->$alias->count_filtered(),
						"data" 				=> $data,
				);
		$json_data[$csrf_name] = $csrf_hash; 
		echo json_encode($output);
    }

    private function add(){
        $method		= $this->input->method();
		if($method != 'post'){
			$forbidden = modules::run('dashboard/login/forbidden');
    		echo $forbidden;
			exit;
        }
        
        
        $this->validasi();
        $this->db->select_max('urutan_menu');
        $result = $this->db->get('tbl_menu')->row();

		 $data = [
			'nama_menu'   => $this->input->post('nama'),
			'id_parent'   => ($this->input->post('parent'))?$this->input->post('parent'):null,
			'link_menu'   => $this->input->post('link'),
			'icon_menu'   => $this->input->post('icon'),
			'urutan_menu' => (int)$result->urutan_menu+1
        ];
        $this->global->insert('tbl_menu',$data);
        echo json_encode(['status' => true,'pesan' => 'Data berhasil di simpan']);
    }

    private function update(){
        $method     = $this->input->method();
        if($method != 'post'){
            $forbidden = modules::run('dashboard/login/forbidden');
            echo $forbidden;
            exit;
        }
        
        $id         = $this->input->post('id');
        $this->validasi();
        $data = [
			'nama_menu'   => $this->input->post('nama'),
			'id_parent'   => ($this->input->post('parent'))?$this->input->post('parent'):null,
			'link_menu'   => $this->input->post('link'),
			'icon_menu'   => $this->input->post('icon')
        ];
        $this->global->update_tabel('tbl_menu',$data,['id_menu' => $id]);
        echo json_encode(['status' => true,'pesan' => 'Data berhasil di update']);
    }

    private function get_parent(){
        $data = $this->global->get_data('tbl_menu',null)->result();
        if($data){
            $result = array_merge(['status' => true,'result' => (array)$data]);
            echo json_encode($result);
        }else{
            echo json_encode(['status' => false,'pesan' => 'data tidak ditemukan']);
        }
    }

    private function delete($id){
        $this->global->update_tabel('m_config',['delete' => 1],['ID_CONFIG' => $id]);
        echo json_encode(['status' => true,'pesan' => 'Data berhasil di hapus']);
    }

    private function detail($id){
        $data = $this->global->get_data('tbl_menu',['id_menu' => $id])->row();
        if($data){
            $result = array_merge(['status' => true],(array)$data);
            echo json_encode($result);
        }else{
            echo json_encode(['status' => false,'pesan' => 'data tidak ditemukan']);
        }
    }

    private function validasi(){
        $data                   = array();
        $data['error_string']   = array();
        $data['inputerror']     = array();
        $data['status']         = TRUE;

        if($this->input->post('nama') == ''){
            $data['inputerror'][] = 'nama';
            $data['error_string'][] = 'Nama harus di isi';
            $data['status'] = FALSE;
        }

        if($this->input->post('link') == ''){
            $data['inputerror'][] = 'link';
            $data['error_string'][] = 'Link harus di isi';
            $data['status'] = FALSE;
        }

      
        if($data['status'] === FALSE){
            echo json_encode($data);
            exit();
        }
    }

}
