<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Configuration extends MY_Controller {
    function __construct(){
		parent::__construct();
		$this->load->library('template');
		$this->load->model('dashboard/m_global','global');
	
	}

    public function index(){
		$this->template->dashboards('isi');
    }
}