<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends MY_Controller {
    function __construct(){
		parent::__construct();
		$this->load->library('template');
		$this->load->model('dashboard/m_global','global');
		
        $akses = modules::run('dashboard/login/role_acess');
        
    	if(!$akses){
    		$forbidden = modules::run('dashboard/login/forbidden');
    		echo $forbidden;
    		exit;
    	}

    }

    public function index(){
        $act            = $this->input->get('act');
        $data['rules']  = modules::run('dashboard/login/get_role_acess');
      
    	switch ($act) {
            case 'list':
                $this->access('list_data');
    			$this->list($data['rules']->view_button,$data['rules']->edit_button,$data['rules']->delete_button,$data['rules']->link_menu);
                break;
            case 'add':
                $this->access('add_button');
                $this->add();
                break;
            case 'edit':
                $this->access('edit_button');
                $this->update();
                break;
            case 'delete':
                $this->access('delete_button');
                $id  = $this->input->get('id');
                $this->delete($id);
                break;
            case 'role':
                $data = $this->global->get_data('tbl_level_user',null)->result();
                echo json_encode($data);
                break;
            case 'detail':
                $id  = $this->input->get('id');
                $this->detail($id);
                break;
            default:
                $this->access('list_data');
				$this->template->dashboards($data['rules']->link_menu,$data);
    			break;
    	}
    }

  
    private function access($act){
        $data = modules::run('dashboard/login/get_role_acess');
        if(!$data->$act){
            $forbidden = modules::run('dashboard/login/forbidden');
            echo $forbidden;
            exit;
        }
    }

    private function list($view,$edit,$delete,$url){
        $method		= $this->input->method();
		if($method != 'post'){
			$forbidden = modules::run('dashboard/login/forbidden');
    		echo $forbidden;
			exit;
		}	
        $url_exp    = explode('/',$url);
        $alias      = $url_exp[1];
        $this->load->model($url_exp[0].'/m_'.$url_exp[1],$alias);
        
        $list 		= $this->$alias->get_datatables();
		$data 		= array();
        $no 		= $this->input->post('start');
        // $id         = '$pages->id_'.$alias;
		
		foreach ($list as $pages) {

			$b_view = ($view)?'<a class="btn bg-teal btn-xs waves-effect" href="javascript:void(0)" title="View" onclick="view('."'".$pages->id_user."'".')"><i class="material-icons">chat</i> View</a> ':'';

			$b_edi 	= ($edit)?'<a class="btn bg-orange btn-xs waves-effect" href="javascript:void(0)" title="Edit" onclick="edit('."'".$pages->id_user."'".')"><i class="material-icons">border_color</i> Edit</a> ':'';
			
			$b_del	= ($delete)?'<a class="btn bg-red btn-xs waves-effect" href="javascript:void(0)" title="Hapus" onclick="hapus('."'".$pages->id_user."'".')"><i class="material-icons ">delete</i> Delete</a> ':'';

			$no++;
			$row = array();
			$row[] = $no;
			$row[] = $pages->username;
			$row[] = $pages->nama_lengkap_user;
			$row[] = $pages->nama_level_user;
			$row[] = $pages->last_login;
			$row[] = ($pages->status)?'<i class="material-icons font-bold col-teal">done</i>':'<i class="material-icons font-bold col-pink">clear</i>';
			
			
			$row[] = $b_view.$b_edi.$b_del;
			
			$data[] = $row;
		}
	
		$csrf_name = $this->security->get_csrf_token_name();
		$csrf_hash = $this->security->get_csrf_hash(); 

		$output = array(
						"draw" 				=> $this->input->post('draw'),
						"recordsTotal" 		=> $this->$alias->count_all(),
						"recordsFiltered" 	=> $this->$alias->count_filtered(),
						"data" 				=> $data,
				);
		$json_data[$csrf_name] = $csrf_hash; 
		echo json_encode($output);
    }

    private function add(){
        $method		= $this->input->method();
		if($method != 'post'){
			$forbidden = modules::run('dashboard/login/forbidden');
    		echo $forbidden;
			exit;
        }
        $this->load->library('encryption');
        $usr    = $this->input->post('username');
        $psw    = $this->input->post('password');
        $rol    = $this->input->post('role');
        $nama   = $this->input->post('nama');
        $email  = $this->input->post('email');
        $status  = $this->input->post('status');
        
        
        $this->validasi();
		$data = [
			'username'      => $usr,
			'password'      => $this->encryption->encrypt($psw),
			'id_level_user' => $rol,
			'status'        => $status
        ];
        $insert = $this->global->insert('tbl_user',$data);
        $data2 = [
            'id_user'           => $insert,
            'email'             => $email,
            'nama_lengkap_user' => $nama,
        ];
        $insert2 = $this->global->insert('tbl_user_detail',$data2);
        echo json_encode(['status' => true,'pesan' => 'Data berhasil di simpan']);
    }

    private function update(){
        $method     = $this->input->method();
        if($method != 'post'){
            $forbidden = modules::run('dashboard/login/forbidden');
            echo $forbidden;
            exit;
        }
        $this->load->library('encryption');
        $id     = $this->input->post('id');
        $usr    = $this->input->post('username');
        $psw    = $this->input->post('password');
        $rol    = $this->input->post('role');
        $nama   = $this->input->post('nama');
        $email  = $this->input->post('email');
        $status  = $this->input->post('status');
        // $this->validasi();
        $data = [
			'username'      => $usr,
			// 'password'      => $this->encryption->encrypt($psw),
			'id_level_user' => $rol,
			'status'        => $status
        ];
        if($psw){
            $data['password'] = $this->encryption->encrypt($psw);
        }
        $this->global->update_tabel('tbl_user',$data,['id_user' => $id]);
        $data2 = [
            'email'             => $email,
            'nama_lengkap_user' => $nama,
        ];
        $this->global->update_tabel('tbl_user_detail',$data2,['id_user' => $id]);
        echo json_encode(['status' => true,'pesan' => 'Data berhasil di update']);
    }

    private function delete($id){
        $this->global->update_tabel('tbl_user',['delete' => 1],['id_user' => $id]);
        echo json_encode(['status' => true,'pesan' => 'Data berhasil di hapus']);
    }

    private function detail($id){
        $this->load->model('configuration/m_user','user');
        $data = $this->user->detail($id);
        if($data){
            $result = array_merge(['status' => true],(array)$data);
            echo json_encode($result);
        }else{
            echo json_encode(['status' => false,'pesan' => 'data tidak ditemukan']);
        }
    }

    private function validasi(){
        $data                   = array();
        $data['error_string']   = array();
        $data['inputerror']     = array();
        $data['status']         = TRUE;

        if($this->input->post('username') == ''){
            $data['inputerror'][] = 'username';
            $data['error_string'][] = 'Username harus di isi';
            $data['status'] = FALSE;
        }

        if($this->input->post('password') == ''){
            $data['inputerror'][] = 'password';
            $data['error_string'][] = 'Password harus di isi';
            $data['status'] = FALSE;
        }

        if($this->input->post('nama') == ''){
            $data['inputerror'][] = 'nama';
            $data['error_string'][] = 'Nama harus di isi';
            $data['status'] = FALSE;
        }

        if($this->input->post('email') == ''){
            $data['inputerror'][] = 'email';
            $data['error_string'][] = 'Email harus di isi';
            $data['status'] = FALSE;
        }

        if($this->input->post('username')){
            $datab = $this->global->get_data('tbl_user',['username' => $this->input->post('username')])->result();
            if($datab){
                $data['inputerror'][] = 'username';
                $data['error_string'][] = 'Username sudah ada yang menggunakan';
                $data['status'] = FALSE;
            }
        }

      
        if($data['status'] === FALSE){
            echo json_encode($data);
            exit();
        }
    }

}
