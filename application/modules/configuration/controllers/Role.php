<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Role extends MY_Controller {
    function __construct(){
		parent::__construct();
		$this->load->library('template');
		$this->load->model('dashboard/m_global','global');
		$this->load->model('configuration/m_role','roles');

		$akses = modules::run('dashboard/login/role_acess');
    	if(!$akses){
    		$forbidden = modules::run('dashboard/login/forbidden');
    		echo $forbidden;
    		exit;
    	}

	}

    public function index(){
		#GET ROLES
		$get_role 	= modules::run('dashboard/login/get_role_acess');
		#BUTTON
		$data['add']= $get_role->add_button;
		$view 		= $get_role->view_button;
		$edit 		= $get_role->edit_button;
		$delete 	= $get_role->delete_button;
		
		$act = $this->input->get('act');
    	switch ($act) {
			case 'list':
				$this->access('list_data');
    			$this->list($view,$edit,$delete);
				break;
			case 'add':
				$this->access('add_button');
				$action = $this->input->get('method');
				$data['menu_list']	= $this->global->get_data('tbl_menu',null)->result_array();
				if($action){
					echo $this->add();
				}else{
					$this->template->dashboards('role_add',$data);
				}
				break;
			case 'edit':
				$this->access('edit_button');
				$role 			= $this->input->get('role');
				$data['menu_list']	= $this->global->get_data('tbl_menu',null)->result_array();
				$this->template->dashboards('role_add',$data);
				break;
			case 'update':
				$this->access('edit_button');
				$this->update();
				break;
			case 'get_rules':
				$role 			= $this->input->get('role');
				$this->get_role($role);
				break;
			default:
				$this->access('list_data');
				$this->template->dashboards('role',$data);
    			break;
    	}
	}

	private function add(){
		$nama 	= $this->input->post('nama_role');
		$ket 	= $this->input->post('keterangan');
		$data = [
			'nama_level_user'		=> $nama,
			'keterangan_level_user'	=> $ket,
			'aktif'					=> 1
		];
		$insert = $this->global->insert('tbl_level_user',$data);
		$menu	= $this->global->get_data('tbl_menu',null)->result_array();
		foreach ($menu as $key) {
			$list 	= $this->input->post(''.$key['id_menu'].'_list');
			$view 	= $this->input->post(''.$key['id_menu'].'_views');
			$add 	= $this->input->post(''.$key['id_menu'].'_add');
			$edit 	= $this->input->post(''.$key['id_menu'].'_edit');
			$delete = $this->input->post(''.$key['id_menu'].'_delete');
			$pdf 	= $this->input->post(''.$key['id_menu'].'_pdf');
			$aktv = ( $list != null || $view != null || $add != null || $edit != null || $delete != null || $pdf != null)?1:0;
			$data2 = [
					'id_menu'			=> $key['id_menu'],
					'id_level_user'		=> $insert,
					'list_data'			=> $list,
					'view_button'		=> $view,
					'add_button'		=> $add,
					'edit_button'		=> $edit,
					'delete_button'		=> $delete,
					'export_pdf_button'	=> $pdf,
					'aktif'				=> $aktv
				];
			$this->global->insert('tbl_hak_akses',$data2);
		}
		echo json_encode(['status' => true,'pesan' => 'berhasil menambahkan role baru']);

	}

	private function update(){
		$id 	= $this->input->post('id');
		$nama 	= $this->input->post('nama_role');
		$ket 	= $this->input->post('keterangan');
		
		$list 	= $this->input->post('list');
		#UPDATE TABEL LEVEL
		$data = [
			'nama_level_user'		=> $nama,
			'keterangan_level_user'	=> $ket,
			'aktif'					=> 1
		];
		$this->global->update_tabel('tbl_level_user',$data,['id_level_user' => $id]);
		$menu	= $this->global->get_data('tbl_menu',null)->result_array();
		foreach ($menu as $key) {
			$list 	= $this->input->post(''.$key['id_menu'].'_list');
			$view 	= $this->input->post(''.$key['id_menu'].'_views');
			$add 	= $this->input->post(''.$key['id_menu'].'_add');
			$edit 	= $this->input->post(''.$key['id_menu'].'_edit');
			$delete = $this->input->post(''.$key['id_menu'].'_delete');
			$pdf 	= $this->input->post(''.$key['id_menu'].'_pdf');

			$cek = $this->global->get_data('tbl_hak_akses',['id_menu' => $key['id_menu'],'id_level_user' => $id])->result_array();
			$aktv = ( $list != null || $view != null || $add != null || $edit != null || $delete != null || $pdf != null)?1:0;
			$data2 = [
					'list_data'			=> $list,
					'view_button'		=> $view,
					'add_button'		=> $add,
					'edit_button'		=> $edit,
					'delete_button'		=> $delete,
					'export_pdf_button'	=> $pdf,
					'aktif'				=> $aktv
				];

			if($cek){
				$this->global->update_tabel('tbl_hak_akses',$data2,['id_menu' => $key['id_menu'],'id_level_user' => $id]);
			}else{
				$cek_menu = $this->global->get_data('tbl_menu',['id_menu' => $key['id_menu']])->row();
				if($cek_menu->id_parent){
					$cek_ha = $this->global->get_data('tbl_hak_akses',['id_menu' => $key['id_menu'],'id_level_user' => $id])->row();
					if(!$cek_ha){
						$this->global->insert('tbl_hak_akses',['id_menu' => $key['id_menu'],'id_level_user' => $id,'aktif' => $aktv]);
					}
				}else{
					$akt = ($cek_menu->id_parent == null || $list != null || $view != null || $add != null || $edit != null || $delete != null || $pdf != null)?1:0;
						$insert = [
							'id_menu'			=> $key['id_menu'],
							'id_level_user'		=> $id,
							'list_data'			=> $list,
							'view_button'		=> $view,
							'add_button'		=> $add,
							'edit_button'		=> $edit,
							'delete_button'		=> $delete,
							'export_pdf_button'	=> $pdf,
							'aktif'				=> $akt
						];
						$this->global->insert('tbl_hak_akses',$insert);
				}
			}
			$this->global->update_tabel('tbl_hak_akses',$data2,['id_menu' => $key['id_menu'],'id_level_user' => $id]);
		}
		echo json_encode(['status' => true,'pesan' => 'berhasil update role']);
	}

	private function access($act){
        $data = modules::run('dashboard/login/get_role_acess');
        if(!$data->$act){
            $forbidden = modules::run('dashboard/login/forbidden');
            echo $forbidden;
            exit;
        }
	}
	
	private function get_role($id){
		$data 	= $this->global->get_data('tbl_hak_akses',['id_level_user' => $id])->result_array();
		$data2 	= $this->global->get_data('tbl_level_user',['id_level_user' => $id])->row();
		if($data2){
			$result = array_merge(['status' => true,'detail' => $data2,'result' => $data]);
            echo json_encode($result);
		}else{
			echo json_encode(['status' => false,'pesan' => 'data tidak di temukan']);
		}
	}
	
	private function list($view,$edit,$delete){
		$method		= $this->input->method();
		if($method != 'post'){
			$forbidden = modules::run('dashboard/login/forbidden');
    		echo $forbidden;
			exit;
		}			
		$list 		= $this->roles->get_datatables();
		$data 		= array();
		$no 		= $this->input->post('start');
		
		foreach ($list as $pages) {

			$b_view = ($view)?'<a class="btn bg-teal btn-xs waves-effect" href="javascript:void(0)" title="View" onclick="view('."'".$pages->id_level_user."'".')"><i class="material-icons">chat</i> View</a> ':'';

			$b_edi 	= ($edit)?'<a class="btn bg-orange btn-xs waves-effect" href="javascript:void(0)" title="Edit" onclick="edit('."'".$pages->id_level_user."'".')"><i class="material-icons">border_color</i> Edit</a> ':'';
			
			$b_del	= ($delete)?'<a class="btn bg-red btn-xs waves-effect" href="javascript:void(0)" title="Hapus" onclick="delete_pengguna('."'".$pages->id_level_user."'".')"><i class="material-icons ">delete</i> Delete</a> ':'';

			$no++;
			$row = array();
			$row[] = $no;
			$row[] = $pages->nama_level_user;
			$row[] = $pages->keterangan_level_user;
			$row[] = $pages->aktif;
			
			$row[] = $b_view.$b_edi.$b_del;
			
			$data[] = $row;
		}
	
		$csrf_name = $this->security->get_csrf_token_name();
		$csrf_hash = $this->security->get_csrf_hash(); 

		$output = array(
						"draw" 				=> $this->input->post('draw'),
						"recordsTotal" 		=> $this->roles->count_all(),
						"recordsFiltered" 	=> $this->roles->count_filtered(),
						"data" 				=> $data,
				);
		$json_data[$csrf_name] = $csrf_hash; 
		echo json_encode($output);
	}
}