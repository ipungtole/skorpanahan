<?php
	$role 	= $this->input->get('role');
	$act 	= $this->input->get('act');
?>
<section class="content">
	<div class="container-fluid">
		<div class="block-header">
			<h2>
				<ol class="breadcrumb breadcrumb-bg-pink">
					<li><a href="javascript:void(0);"><i class="material-icons">home</i> Home</a></li>
					<?php
					$aks = $akses[0]['link_menu'];
					if ($aks) {
						$exp = explode('/', $aks);
						$last = $this->uri->total_segments();
						$end = $this->uri->segment($last);
						foreach ($exp as $key) {
							if ($key == $end) {
								$d 	= 'class="active"';
								$a 	= '';
								$a1 = '';
								$en = $akses[0]['nama_menu'];
							} else {
								$a 	= '<a href="javascript:void(0);">';
								$d 	= '';
								$a1 = '</a>';
								$en = $key;
							}
							echo '<li ' . $d . '> ' . $a . ucwords(str_replace('_', ' ', $en)) . $a1 . '</li>';
						}
					}
					?>
				</ol>
			</h2>
		</div>
		<!-- Basic Examples -->
		<div class="row clearfix">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="card">
					<div class="header">
						<h2>
							<?= $akses[0]['nama_menu'] ?>
						</h2>
						<ul class="header-dropdown m-r--5">
							
						</ul>
					</div>
					<div class="body">
                        <form action="#" id="form" class="form-horizontal">
                                <?= $csrf ?>
                                <div class="form-body">
                                    <div class="form-group">
                                        <label class="control-label col-md-3"> Nama</label>
                                        <div class="col-md-4">
											<div class="form-line">
												<input type="hidden" name="id" id="id">
												<input type="text" name="nama_role" id="nama_role" class="form-control" placeholder="Nama Role">
												<span class="help-block"></span>
                                        	</div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3"> Keterangan</label>
                                        <div class="col-md-4">
											<div class="form-line">
												<input type="text" name="keterangan" id="keterangan" class="form-control" placeholder="Keterangan">
												<span class="help-block"></span>
											</div>
										</div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3"> Hak Akses</label>
                                        <div class="col-md-9">                                            
											<?php
												function has_children2($rows,$id) {
													foreach ($rows as $row) {
														if ($row['id_parent'] == $id)
														return true;
													}
													return false;
												}
												function build_menu2($rows,$parent=null){  
													$ex = explode('/',uri_string());
													// $result = "<ul class='ml-menu'>";
													$result = '<ul class="list-group">';
													foreach ($rows as $row){
														if ($row['id_parent'] == $parent){
															$a      = "<i class='material-icons'>".$row['icon_menu']."</i>";
															$a2     = "<i class='material-icons'>".$row['icon_menu']."</i>";
															
															$check = '<span class="pull-right">
																			<input type="checkbox" id="'.$row['id_menu'].'_list" name="'.$row['id_menu'].'_list" class="filled-in chk-col-red" value="1">
																			<label for="'.$row['id_menu'].'_list">List</label>&nbsp;&nbsp;
																			<input type="checkbox" id="'.$row['id_menu'].'_views" name="'.$row['id_menu'].'_views" class="filled-in chk-col-red" value="1">
												 							<label for="'.$row['id_menu'].'_views">Views</label>&nbsp;&nbsp;
												 							<input type="checkbox" id="'.$row['id_menu'].'_add" name="'.$row['id_menu'].'_add" class="filled-in chk-col-red" value="1">
												 							<label for="'.$row['id_menu'].'_add">Add</label>&nbsp;&nbsp;
												 							<input type="checkbox" id="'.$row['id_menu'].'_edit" name="'.$row['id_menu'].'_edit" class="filled-in chk-col-red" value="1">
												 							<label for="'.$row['id_menu'].'_edit">Edit</label>&nbsp;&nbsp;
												 							<input type="checkbox" id="'.$row['id_menu'].'_delete" name="'.$row['id_menu'].'_delete" class="filled-in chk-col-red" value="1">
												 							<label for="'.$row['id_menu'].'_delete">Delete</label>&nbsp;&nbsp;
												 							<input type="checkbox" id="'.$row['id_menu'].'_pdf" name="'.$row['id_menu'].'_pdf" class="filled-in chk-col-red" value="1">
												 							<label for="'.$row['id_menu'].'_pdf">Exp. PDF</label>&nbsp;&nbsp;
																		</span>';

															$has    = (has_children($rows,$row['id_menu']))?$a2:$a; 
															$ck    	= (has_children($rows,$row['id_menu']))?$check:''; 
															$result.= "<li class='list-group-item'> $has {$row['nama_menu']  } $ck";
															
															if (has_children2($rows,$row['id_menu'])){
																$result.= build_menu2($rows,$row['id_menu']);
															}
															$has    = (has_children2($rows,$row['id_menu']))?'':$check;
															$result.= $has;
															$result.= "</li>";
														}
														
													}
													$result.= "</ul>";

													return $result;
												}                
												echo build_menu2($menu_list);
												
											?>
                        				</div>
                                    </div>
                        			<div class="form-group">
                                        <label class="control-label col-md-3"></label>
                                        <div class="col-md-7">
                                            <button class="btn btn-danger waves-effect" id="btnCancel" onclick="cancel()"><i class="material-icons">call_missed</i> Cancel</button>
										<button class="btn btn-primary waves-effect" id="btnSave" onclick="simpan()" type="submit"><i class="material-icons">save</i> Simpan</button>
                                        </div>
                                    </div>
                                </div>
                            </form> 
					</div>
				</div>
			</div>
		</div>
		<!-- #END# Basic Examples -->

	</div>
</section>

<script src="<?php echo base_url('assets/admin/' . $a_theme . '/plugins/jquery-datatable/jquery.dataTables.js') ?>"></script>
<script src="<?php echo base_url('assets/admin/' . $a_theme . '/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js') ?>"></script>
<script type="text/javascript">
	var save_method; //for save method string
	var table;
	var base_url = '<?php echo base_url(); ?>';
	var hash = '<?php echo $csrf_hash; ?>';

	<?php
		if($act == 'edit'){
			echo 'get_data();';
		}
	?>

	$(document).ready(function() {
		table = $('#list_data').DataTable({
			"responsive": true,
			"processing": true, //Feature control the processing indicator.
			"serverSide": true, //Feature control DataTables' server-side processing mode.
			"order": [], //Initial no order.

			// Load data for the table's content from an Ajax source
			"ajax": {
				"url": "<?php echo site_url($url . '?act=list') ?>",
				"type": "POST",
				"data": {
					rania_altha: hash
				},
			},

			//Set column definition initialisation properties.
			"columnDefs": [{
				"targets": [-1], //last column
				"orderable": false, //set not orderable
			}, ],

		});
	});

	<?php
		#START IF ROLE EDIT
		if($act == 'edit'){
	?>

	function get_data(){
		$.ajax({
			url: "<?php echo site_url($url . '?act=get_rules&role=') ?>" + <?= $role ?>,
			type: 'GET',
			dataType: 'json',
			success: function(data) {
				$("[name='id']").val(data.detail.id_level_user);
				$("[name='nama_role']").val(data.detail.nama_level_user);
				$("[name='keterangan']").val(data.detail.keterangan_level_user);
				// $("#0_list").attr("checked",true);
				if (data.result) {
					for (i = 0; i < data.result.length; i++) {
						if(data.result[i]['list_data']){
							$("#"+data.result[i]['id_menu']+"_list").attr("checked",true);
							// $("#"+data.result[i]['id_menu']+"_list").val("1");
						}
						if(data.result[i]['view_button']){
							$("#"+data.result[i]['id_menu']+"_views").attr("checked",true);
							// $("#"+data.result[i]['id_menu']+"_views").val("1");
						}
						if(data.result[i]['add_button']){
							$("#"+data.result[i]['id_menu']+"_add").attr("checked",true);
							// $("#"+data.result[i]['id_menu']+"_add").val("1");
						}
						if(data.result[i]['edit_button']){
							$("#"+data.result[i]['id_menu']+"_edit").attr("checked",true);
							// $("#"+data.result[i]['id_menu']+"_edit").val("1");
						}
						if(data.result[i]['delete_button']){
							$("#"+data.result[i]['id_menu']+"_delete").attr("checked",true);
							// $("#"+data.result[i]['id_menu']+"_delete").val("1");
						}
						if(data.result[i]['export_pdf_button']){
							$("#"+data.result[i]['id_menu']+"_pdf").attr("checked",true);
							// $("#"+data.result[i]['id_menu']+"_pdf").val("1");
						}
						// data.result[i]['id_inventaris']
						// var optmrtg = "<option value='" + data.result[i]['id_inventaris'] + "' ";
						// optmrtg += " >" + data.result[i]['kode_inventaris'] + ' - ' + data.result[i]['nama_barang'] + "</option>";
						// fcb.append(optmrtg);
					}
				}
			}
		});
	}

	function simpan(){
		$('#btnSave').text('saving...'); //change button text
		$('#btnSave').attr('disabled', true); //set button disable 
		var urls;

		var formData = new FormData($('#form')[0]);
		if (save_method == 'add') {
			urls = '<?php echo site_url($url . '?act=add') ?>';
		} else {
			urls = '<?php echo site_url($url . '?act=update') ?>';
		}
		// ajax adding data to database
		$.ajax({
			url: urls,
			type: "POST",
			data: formData,
			contentType: false,
			processData: false,
			dataType: "JSON",
			success: function(data) {

				if (data.status) { //if success close modal and reload ajax table
					if (save_method == 'add') {

						$.confirm({
							icon: 'fa fa-smile-o',
							title: 'Yeaah....',
							content: data.pesan,
							theme: 'modern',
							closeIcon: true,
							type: 'green',
							animation: 'rotate',
							buttons: {
								IsiLagi: function() {
									$('#form').trigger("reset");
									table.ajax.reload(null, false);
								},
								Tidak: function() {
									$('#modals').modal('hide');
									table.ajax.reload(null, false);
								}
							}
						});

					} else {
						$.confirm({
							icon: 'fa fa-smile-o',
							title: 'Yeaaho....',
							content: data.pesan,
							theme: 'modern',
							closeIcon: true,
							type: 'orange',
							animation: 'rotate',
							buttons: {
								Ok: function() {
									$('#modals').modal('hide');
									table.ajax.reload(null, false);
								}
							}
						});
						$('#btnSave').html('<i class="material-icons">save</i>  Simpan'); //change button text
					$('#btnSave').attr('disabled', false); //set button enable 
					}

				} else {
					for (var i = 0; i < data.inputerror.length; i++) {
						if (data.inputerror[i] == 'cabang' || data.inputerror[i] == 'nama_kapal') {
							$('[name="' + data.inputerror[i] + '"]').parent().addClass('error'); //select parent twice to select div form-group class and add has-error class
							$('[name="' + data.inputerror[i] + '"]').next().next().text(data.error_string[i]); //select span help-block class set text error string
						} else {
							$('[name="' + data.inputerror[i] + '"]').parent().parent().addClass('has-error'); //select parent twice to select div form-group class and add has-error class
							$('[name="' + data.inputerror[i] + '"]').next().text(data.error_string[i]);
						}
					}



					$('#btnSave').html('<i class="material-icons">save</i>  Simpan'); //change button text
					$('#btnSave').attr('disabled', false); //set button enable 
				}
			},
			error: function(jqXHR, textStatus, errorThrown) {

			}
		});
	}

	<?php
		#FINISH IF ROLE EDIT
	}else{
		?>
		function simpan(){
		$('#btnSave').text('saving...'); //change button text
		$('#btnSave').attr('disabled', true); //set button disable 
		var urls;

		var formData = new FormData($('#form')[0]);
		urls = '<?php echo site_url($url . '?act=add&method=add_data') ?>';
		
		// ajax adding data to database
		$.ajax({
			url: urls,
			type: "POST",
			data: formData,
			contentType: false,
			processData: false,
			dataType: "JSON",
			success: function(data) {

				if (data.status) { //if success close modal and reload ajax table
					$.confirm({
							icon: 'fa fa-smile-o',
							title: 'Yeaah....',
							content: data.pesan,
							theme: 'modern',
							closeIcon: true,
							type: 'green',
							animation: 'rotate',
							buttons: {
								IsiLagi: function() {
									$('#form').trigger("reset");
									table.ajax.reload(null, false);
								},
								Tidak: function() {
									$('#modals').modal('hide');
									table.ajax.reload(null, false);
								}
							}
						});
					$('#btnSave').attr('disabled', false); //set button enable 
					

				} else {
					for (var i = 0; i < data.inputerror.length; i++) {
						if (data.inputerror[i] == 'cabang' || data.inputerror[i] == 'nama_kapal') {
							$('[name="' + data.inputerror[i] + '"]').parent().addClass('error'); //select parent twice to select div form-group class and add has-error class
							$('[name="' + data.inputerror[i] + '"]').next().next().text(data.error_string[i]); //select span help-block class set text error string
						} else {
							$('[name="' + data.inputerror[i] + '"]').parent().parent().addClass('has-error'); //select parent twice to select div form-group class and add has-error class
							$('[name="' + data.inputerror[i] + '"]').next().text(data.error_string[i]);
						}
					}



					$('#btnSave').html('<i class="material-icons">save</i>  Simpan'); //change button text
					$('#btnSave').attr('disabled', false); //set button enable 
				}
			},
			error: function(jqXHR, textStatus, errorThrown) {

			}
		});
	}
	<?php
		}
	?>
</script>
