<section class="content">
	<div class="container-fluid">
		<div class="block-header">
			<h2>
				<ol class="breadcrumb breadcrumb-bg-pink">
					<li><a href="javascript:void(0);"><i class="material-icons">home</i> Home</a></li>
					<?php
					$aks = $akses[0]['link_menu'];
					if ($aks) {
						$exp = explode('/', $aks);
						$last = $this->uri->total_segments();
						$end = $this->uri->segment($last);
						foreach ($exp as $key) {
							if ($key == $end) {
								$d 	= 'class="active"';
								$a 	= '';
								$a1 = '';
								$en = $akses[0]['nama_menu'];
							} else {
								$a 	= '<a href="javascript:void(0);">';
								$d 	= '';
								$a1 = '</a>';
								$en = $key;
							}
							echo '<li ' . $d . '> ' . $a . ucwords(str_replace('_', ' ', $en)) . $a1 . '</li>';
						}
					}
					?>
				</ol>
			</h2>
		</div>
		<!-- Basic Examples -->
		<div class="row clearfix">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="card">
					<div class="header">
						<h2>
							<?= $akses[0]['nama_menu'] ?>
						</h2>
						
					</div>
					<div class="body">
					 	<?php
                            if ($add) {
                                echo '
                                <ol class="breadcrumb align-right">
                                    <li>
                                        <a class="btn bg-cyan btn-sm waves-effect" href="javascript:void(0)" title="Add" onclick="add()"><i class="material-icons">library_add</i> Tambah Baru</a>
                                    </li>
                                </ol>';
                            }
                        ?>
						<div class="table-responsive">
							<table id="list_data" class="table table-bordered table-striped table-hover dataTable">
								<thead>
									<tr>
										<th>No</th>
										<th>Role</th>
										<th>Deskripsi</th>
										<th>Status</th>
										<th>Action</th>
									</tr>
								</thead>
								<tfoot>
									<tr>
										<th>Name</th>
										<th>Role</th>
										<th>Deskripsi</th>
										<th>Status</th>
										<th>Action</th>
									</tr>
								</tfoot>
								<tbody>

								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- #END# Basic Examples -->

	</div>
</section>

<script src="<?php echo base_url('assets/admin/' . $a_theme . '/plugins/jquery-datatable/jquery.dataTables.js') ?>"></script>
<script src="<?php echo base_url('assets/admin/' . $a_theme . '/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js') ?>"></script>
<script type="text/javascript">
	var save_method; //for save method string
	var table;
	var base_url = '<?php echo base_url(); ?>';
	var hash = '<?php echo $csrf_hash; ?>';

	$(document).ready(function() {
		table = $('#list_data').DataTable({
			"responsive": true,
			"processing": true, //Feature control the processing indicator.
			"serverSide": true, //Feature control DataTables' server-side processing mode.
			"order": [], //Initial no order.

			// Load data for the table's content from an Ajax source
			"ajax": {
				"url": "<?php echo site_url($url . '?act=list') ?>",
				"type": "POST",
				"data": {
					rania_altha: hash
				},
			},

			//Set column definition initialisation properties.
			"columnDefs": [{
				"targets": [-1], //last column
				"orderable": false, //set not orderable
			}, ],

		});
	});

	function add(){
		window.open('<?= site_url($url . '?act=add') ?>','_self');
	}

	function edit(id){
		window.open('<?= site_url($url . '?act=edit&role=') ?>'+id,'_self');
	}

</script>
