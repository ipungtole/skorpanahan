<?php
echo link_tag('assets/admin/' . $a_theme . '/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css');
echo link_tag('assets/admin/' . $a_theme . '/plugins/bootstrap-datepicker/css/bootstrap-datepicker.css');

?>
<section class="content">
	<div class="container-fluid">
		<div class="block-header">
			<h2>
				<ol class="breadcrumb breadcrumb-bg-pink">
					<li><a href="javascript:void(0);"><i class="material-icons">home</i> Home</a></li>
					<?php
					$aks = $akses[0]['link_menu'];
					if ($aks) {
						$exp = explode('/', $aks);
						$last = $this->uri->total_segments();
						$end = $this->uri->segment($last);
						foreach ($exp as $key) {
							if ($key == $end) {
								$d 	= 'class="active"';
								$a 	= '';
								$a1 = '';
								$en = $akses[0]['nama_menu'];
							} else {
								$a 	= '<a href="javascript:void(0);">';
								$d 	= '';
								$a1 = '</a>';
								$en = $key;
							}
							echo '<li ' . $d . '> ' . $a . ucwords(str_replace('_', ' ', $en)) . $a1 . '</li>';
						}
					}
					?>
				</ol>
			</h2>
		</div>
		<!-- Basic Examples -->
		<div class="row clearfix">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="card">
					<div class="header">
						<h2>
							<?= $akses[0]['nama_menu'] ?>
						</h2>

					</div>
					<div class="body">

						<div class="table-responsive">
							<?php
								if ($rules->add_button) {
									echo '
									<ol class="breadcrumb align-right">
										<li>
											<a class="btn bg-cyan btn-sm waves-effect" href="javascript:void(0)" title="Add" onclick="add()"><i class="material-icons">library_add</i> Tambah Baru</a>
										</li>
									</ol>';
								}
                        	?>
							<table id="list_data" class="table table-bordered table-striped table-hover dataTable table-responsive">
								<thead>
									<tr>
										<th>No</th>
										<th>Nama</th>
										<th>Link</th>
										<th>Icon</th>
										<th>order</th>
										<th>Action</th>
									</tr>
								</thead>
							</table>
						</div>

					</div>
				</div>
			</div>
		</div>
		<!-- #END# Basic Examples -->

	</div>
</section>

<div class="modal fade" id="modals" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title" id="defaultModalLabel">Modal title</h4>
			</div>
			<div class="modal-body">
				<form id="form" class="form-horizontal">
					<?= $csrf ?>
					<div class="form-group">
						<label class="control-label col-md-3"> Nama Menu</label>
						<div class="col-md-9">
							<input type="text" name="nama" id="nama" class="form-control" placeholder="Nama">
							<span class="help-block"></span>
							<input type="hidden" name="id" id="id">
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3"> Parent</label>
						<div class="col-md-9">
							<select name="parent" class="form-control show-tick"></select>
							<span class="help-block"></span>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3"> Link</label>
						<div class="col-md-9">
							<input type="text" name="link" id="link" class="form-control" placeholder="Link">
							<span class="help-block"></span>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3"> Icon</label>
						<div class="col-md-9">
							<input type="text" name="icon" id="icon" class="form-control" placeholder="Icon">
							<span class="help-block"></span>
						</div>
					</div>


				</form>
			</div>
			<div class="modal-footer">
				<button type="button" onclick="simpan()" class="btn btn-link waves-effect">SIMPAN</button>
				<button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
			</div>
		</div>
	</div>
</div>

<script src="<?php echo base_url('assets/admin/' . $a_theme . '/plugins/jquery-datatable/jquery.dataTables.js') ?>"></script>
<script src="<?php echo base_url('assets/admin/' . $a_theme . '/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js') ?>"></script>
<script src="<?php echo base_url('assets/admin/' . $a_theme . '/plugins/momentjs/moment.js') ?>"></script>
<script src="<?php echo base_url('assets/admin/' . $a_theme . '/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js') ?>"></script>
<script src="<?php echo base_url('assets/admin/' . $a_theme . '/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js') ?>"></script>
<script type="text/javascript">
	var save_method; //for save method string
	var table;
	var base_url = '<?php echo base_url(); ?>';
	// var hash2       ;
	var hash = '<?= $this->security->get_csrf_hash(); ?>';
	getparent();
	$(function() {
		$("input").change(function() {
			$(this).parent().parent().removeClass('has-error');
			$(this).next().empty();
		});

		table = $('#list_data').DataTable({
			"responsive": true,
			"processing": true, //Feature control the processing indicator.
			"serverSide": true, //Feature control DataTables' server-side processing mode.
			"order": [], //Initial no order.

			// Load data for the table's content from an Ajax source
			"ajax": {
				"url": "<?php echo site_url($url . '?act=list') ?>",
				"type": "POST",
				"data": {
					rania_altha: hash
				},
			},

			//Set column definition initialisation properties.
			"columnDefs": [{
				"targets": [-1], //last column
				"orderable": false, //set not orderable
			}, ],

		});
	});

	function add() {
		$('.form-group').removeClass('has-error'); // clear error class
		$('.help-block').empty(); // clear error string
		$('#form').trigger("reset");
		$('#modals').modal('show');
		$('.modal-title').text('Tambah <?= $akses[0]['nama_menu'] ?> Baru');
		save_method = 'add';
	}

	function edit(param) {
		detail(param);
		save_method = 'edit';
		$('#modals').modal('show');
		$('.modal-title').text('Edit <?= $akses[0]['nama_menu'] ?> ');
	}

	function getparent() {
		parents = $("[name='parent']");
		$.ajax({
			url: "<?php echo site_url($url . '?act=parent') ?>",
			type: 'GET',
			dataType: 'json',
		}).done(function(response) {
			// $('.util-spin').hide();
			// fcb.removeAttr('disabled');
			// $('.input').removeAttr('disabled');
			parents.html("<option value='0'>--- Pilih Parent ---</option>");
			if (response.result) {
				for (i = 0; i < response.result.length; i++) {
					var parent = "<option value='" + response.result[i]['id_menu'] + "' ";
					parent += " >" + response.result[i]['nama_menu'] + "</option>";
					parents.append(parent).selectpicker('refresh');

				}
			}
		});
	}

	function detail(id) {
		$.ajax({
			url: "<?php echo site_url($url . '?act=detail&id=') ?>" + id,
			type: 'GET',
			dataType: 'json',
			success: function(data) {
				$("[name='id']").val(data.id_menu);
				$("[name='nama']").val(data.nama_menu);
				$("[name='link']").val(data.link_menu);
				$("[name='parent']").val(data.id_parent);
				$("[name='parent']").selectpicker('refresh');
				$("[name='icon']").val(data.icon_menu);

			}
		});
	}


	function simpan() {
		$('#btnSave').text('saving...'); //change button text
		$('#btnSave').attr('disabled', true); //set button disable 
		var urls;

		var formData = new FormData($('#form')[0]);
		if (save_method == 'add') {
			urls = '<?php echo site_url($url . '?act=add') ?>';
		} else {
			urls = '<?php echo site_url($url . '?act=edit') ?>';
		}
		// ajax adding data to database
		$.ajax({
			url: urls,
			type: "POST",
			data: formData,
			contentType: false,
			processData: false,
			dataType: "JSON",
			success: function(data) {

				if (data.status) { //if success close modal and reload ajax table
					if (save_method == 'add') {

						$.confirm({
							icon: 'fa fa-smile-o',
							title: 'Yeaah....',
							content: data.pesan,
							theme: 'modern',
							closeIcon: true,
							type: 'green',
							animation: 'rotate',
							buttons: {
								IsiLagi: function() {
									$('#form').trigger("reset");
									table.ajax.reload(null, false);
								},
								Tidak: function() {
									$('#modals').modal('hide');
									table.ajax.reload(null, false);
								}
							}
						});

					} else {
						$.confirm({
							icon: 'fa fa-smile-o',
							title: 'Yeaah....',
							content: data.pesan,
							theme: 'modern',
							closeIcon: true,
							type: 'orange',
							animation: 'rotate',
							buttons: {
								Ok: function() {
									$('#modals').modal('hide');
									table.ajax.reload(null, false);
								}
							}
						});
					}

				} else {
					for (var i = 0; i < data.inputerror.length; i++) {
						if (data.inputerror[i] == 'cabang' || data.inputerror[i] == 'nama_kapal') {
							$('[name="' + data.inputerror[i] + '"]').parent().addClass('error'); //select parent twice to select div form-group class and add has-error class
							$('[name="' + data.inputerror[i] + '"]').next().next().text(data.error_string[i]); //select span help-block class set text error string
						} else {
							$('[name="' + data.inputerror[i] + '"]').parent().parent().addClass('has-error'); //select parent twice to select div form-group class and add has-error class
							$('[name="' + data.inputerror[i] + '"]').next().text(data.error_string[i]);
						}
					}



					$('#btnSave').html('<i class="material-icons">save</i>  Simpan'); //change button text
					$('#btnSave').attr('disabled', false); //set button enable 
				}
			},
			error: function(jqXHR, textStatus, errorThrown) {

			}
		});
	}

	function order(id,param){
		$.ajax({
			url: "<?php echo site_url($url . '?act=order&urutan=') ?>" + id +'&type='+ param,
			type: 'GET',
			dataType: 'json',
			success: function(data) {
				table.ajax.reload(null, false);
			}
		});
	}

	function hapus(id) {
		$.confirm({
			icon: 'fa fa-trash',
			title: 'Hapus',
			content: 'Apakah Anda Akan Menghapus Data ini ?',
			theme: 'modern',
			closeIcon: true,
			type: 'red',
			animation: 'rotate',
			buttons: {
				Ya: function() {
					$.ajax({
						url: '<?php echo site_url($url . '?act=delete&id=') ?>' + id,
						type: "GET",
						dataType: "JSON",
						success: function(data) {
							if (data.status) {
								var icon = 'fa fa-check';
								var judul = 'Hapus Data Berhasil';
								var warna = 'green';
							} else {
								var icon = 'fa fa-times';
								var judul = 'Hapus Data Gagal';
								var warna = 'red';

							}
							$.alert({
								icon: icon,
								title: judul,
								content: '' + data.pesan + '',
								theme: 'modern',
								closeIcon: true,
								type: warna,
								animation: 'rotate'
							});
							table.ajax.reload(null, false);
						},
						error: function(jqXHR, textStatus, errorThrown) {
							alert('Error get data from ajax');
						}
					});
				},

				Tidak: function() {

				}
			}
		});
	}
</script>
