<?php defined('BASEPATH') OR exit('No direct script access allowed');

Class M_user extends CI_Model {
    var $table = 'tbl_user';
    var $column_order = array('tbl_user.id_user','tbl_user.username','tbl_user_detail.nama_lengkap_user','tbl_level_user.nama_level_user','tbl_user.status'); //set column field database for datatable orderable
    var $column_search = array('tbl_user.username'); //set column field database for datatable searchable just firstname , lastname , address are searchable
    var $order = array('tbl_user.id_level_user' => 'asc'); // default order 
 
 
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }
 
    private function _get_datatables_query(){
         
        // $this->db->from($this->table);
        $this->db->select('
            tbl_user.username,
            tbl_user.id_user,
            tbl_user.status,
            tbl_user.last_login,
            tbl_user_detail.email,
            tbl_user_detail.nama_lengkap_user,
            tbl_level_user.nama_level_user
            ');
        $this->db->from('tbl_user');
        $this->db->join('tbl_user_detail','tbl_user_detail.id_user = tbl_user.id_user','left');
        $this->db->join('tbl_level_user','tbl_level_user.id_level_user = tbl_user.id_level_user','left');
        $this->db->where(['tbl_user.delete' => '']);

		
        $i = 0;
     
        foreach ($this->column_search as $item) // loop column 
        {
            if($this->input->post('search')) // if datatable send POST for search
            {
                 
                if($i===0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
 
                if(count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }
         
        if(isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
 
    function get_datatables(){
        $this ->_get_datatables_query();
        if($this->input->post('length') != -1)
        $this->db->limit($this->input->post('length'),$this->input->post('start'));
        $query = $this->db->get();
        return $query->result();
    }
 
    function count_filtered( ){
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
 
    public function count_all(){
        $this->_get_datatables_query();
        return $this->db->count_all_results();
    }

    public function detail($id){
         $this->db->select('
            tbl_user.username,
            tbl_user.id_user,
            tbl_user.last_login,
            tbl_user.id_level_user as role,
            tbl_user.status,
            tbl_user_detail.email,
            tbl_user_detail.nama_lengkap_user,
            tbl_level_user.nama_level_user
            ');
        $this->db->from('tbl_user');
        $this->db->join('tbl_user_detail','tbl_user_detail.id_user = tbl_user.id_user','left');
        $this->db->join('tbl_level_user','tbl_level_user.id_level_user = tbl_user.id_level_user','left');
        $this->db->where(['tbl_user.delete' => null,'tbl_user.id_user' => $id]);
        $query = $this->db->get();
        if($query->result()){
            return $query->row();
        }else{
            return false;
        }
    }
    
}
