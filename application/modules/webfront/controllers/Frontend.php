<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Frontend extends MY_Controller {
	function __construct(){
		parent::__construct();
		$this->load->library('template');
		$this->load->model('dashboard/m_global','global');
		$this->load->model('webfront/m_frontend','front');
	}

	public function index(){
		$this->get_default_language();
		$detail['lang']			= $this->session->userdata('bahasa');
		$detail['bahasa'] 		= $this->global->get_data('p_language',['deleted' => null],null)->result();
		$detail['sub_title'] 	= '';
		$themes 				= $this->global->get_data('m_config',['NAMA_CONFIG' => 'themes'],null)->row()->DETAIL_CONFIG;
		$this->template->frontend('webfront/'.$themes.'/home',$detail);
	}

	private function get_default_language(){
		$lang_selected = $this->session->userdata('bahasa');
		if(!$lang_selected){
			$p_language 	= $this->global->get_data('m_plugins',['id_plugins' => '1'],null)->row()->status;
			if($p_language){
				$g_lang 	= $this->global->get_data('p_language',['default' => 1,'deleted' => null],null)->row();
				$data = [
					'id_language'	=> $g_lang->id_language,
					'language'		=> $g_lang->language,
					'flag'			=> $g_lang->flag,
					'code'			=> $g_lang->code
				];
				$this->session->set_userdata('bahasa', $data);
			}
		}
		
	}
	
	public function pages($urls = null){
		$this->get_default_language();
		$bahasa 				= $this->session->userdata('bahasa');
		$detail['lang']			= $this->session->userdata('bahasa');
		$detail['bahasa'] 		= $this->global->get_data('p_language',['deleted' => null],null)->result();
		$themes 				= $this->global->get_data('m_config',['NAMA_CONFIG' => 'themes'],null)->row()->DETAIL_CONFIG;
		$page 					= $this->front->menu(['m_pages.slug' => $urls]);
		if($page){
			$page = $this->global->get_data('m_pages',['id_parent' => $page[0]->id_parent,'language' => $bahasa['id_language'] ,'deleted' => null],null)->result();
			// var_dump($page2);echo $this->db->last_query(); exit;
			$detail['sub_title'] = $page[0]->judul;
			$detail['page'] 	 = $page[0];
			$this->template->frontend('webfront/'.$themes.'/pages',$detail);
		}else{
			$modul = $this->front->menu(['m_modul.nama_modul' => $urls]);
			if($modul){
				// $id_lang 				= $bahasa['id_language'];
				$detail['id_bahasa'] 	= $bahasa['id_language'];
				$id_mod 				= $modul[0]->modul;
				$title 					= $this->global->get_data('t_dictionary',['id_modul' => $id_mod,'id_language' => $detail['id_bahasa'] ,'deleted' => null],null)->result();
				$detail['sub_title'] 	= ($title)?$title[0]->words:$modul[0]->alias;
				#LOAD TEMPLATE
				$this->template->frontend('webfront/'.$themes.'/'.$modul[0]->templates.'',$detail);
			}else{
				redirect('/', 'refresh');
			}
		}
	}

	public function language(){
		$p_language 	= $this->global->get_data('m_plugins',['id_plugins' => '1'],null)->row()->status;
		if(!$p_language){
			echo json_encode(['status' => false,'message' => 'plugin language is disabled, plaese contact administrator to activate this feature']);
			exit;
		}
		$select = $this->input->get('select');
		$g_lang = $this->global->get_data('p_language',['code' => $select,'deleted' => null],null)->row();
		if(!$g_lang){
			echo json_encode(['status' => false,'message' => 'the language your selected is not found, plaese contact administrator to add your language']);
			exit;
		}
		$data = [
			'id_language'	=> $g_lang->id_language,
			'language'		=> $g_lang->language,
			'flag'			=> $g_lang->flag,
			'code'			=> $g_lang->code
		];
		$this->session->set_userdata('bahasa', $data);
		echo json_encode(['status' => true,'language' => $data['language'],'message' => 'sucessfully change language']);
	}

	public function get_language(){
		$lang	= $this->session->userdata('bahasa');
		return $lang;
	}

}