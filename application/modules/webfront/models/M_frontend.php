<?php defined('BASEPATH') OR exit('No direct script access allowed');

Class M_frontend extends CI_Model {

    public function __construct(){
        parent::__construct();
    }
 
    public function menu($where){
        $this->db->select('
                            m_menufront.*,
                            m_modul.nama_modul,
                            m_modul.templates,
                            m_modul.alias,
                            m_pages.slug,
                            m_pages.konten,
                            m_pages.id_parent
                        ');
        $this->db->from('m_menufront');
        $this->db->join('m_modul','m_modul.id_modul = m_menufront.modul','left');
        $this->db->join('m_pages','m_pages.id_pages = m_menufront.id_pages','left');
        $this->db->where($where);
        $this->db->where(['m_menufront.delete' => null,'m_menufront.status' => 1]);
        $this->db->order_by('m_menufront.order','asc');
        $query = $this->db->get();
        if($query){
            return $query->result();
        }else{
            return false;
        }

    }
    
}
