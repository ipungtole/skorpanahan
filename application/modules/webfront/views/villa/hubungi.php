<div class="slider-area ">
    <?php
        $kolom_1 = $this->global->get_data('t_dictionary',['id_parent' => '10','id_language' => $id_bahasa,'deleted' => null],null)->row();
        $kolom_2 = $this->global->get_data('t_dictionary',['id_parent' => '13','id_language' => $id_bahasa,'deleted' => null],null)->row();
        $baris_1 = $this->global->get_data('t_dictionary',['id_parent' => '16','id_language' => $id_bahasa,'deleted' => null],null)->row();
        $baris_2 = $this->global->get_data('t_dictionary',['id_parent' => '19','id_language' => $id_bahasa,'deleted' => null],null)->row();
        $baris_3 = $this->global->get_data('t_dictionary',['id_parent' => '22','id_language' => $id_bahasa,'deleted' => null],null)->row();
        $baris_4 = $this->global->get_data('t_dictionary',['id_parent' => '7','id_language' => $id_bahasa,'deleted' => null],null)->row();
        $bawah    = $this->global->get_data('t_dictionary',['id_parent' => '25','id_language' => $id_bahasa,'deleted' => null],null)->row();
        $samping  = $this->global->get_data('t_dictionary',['id_parent' => '28','id_language' => $id_bahasa,'deleted' => null],null)->row();
        $samping1 = $this->global->get_data('t_dictionary',['id_parent' => '31','id_language' => $id_bahasa,'deleted' => null],null)->row();
        $malam    = $this->global->get_data('t_dictionary',['id_parent' => '34','id_language' => $id_bahasa,'deleted' => null],null)->row();
        $harga    = $this->global->get_data('m_config',['ID_CONFIG' => '16','DELETE' => null],null)->row();
        $hargaw    = $this->global->get_data('m_config',['ID_CONFIG' => '17','DELETE' => null],null)->row();
        
        $data = $this->global->get_data('m_gallery',['type' => 'slider','deleted' => null,'status' => 1],null)->row();
        echo '<div class="single-slider slider-height2 d-flex align-items-center" data-background="'.'images/'.$data->nama_gambar.'">
                <div class="container">
                    <div class="row">
                        <div class="col-xl-12">
                            <div class="hero-cap text-center">
                                <h2>'.$sub_title.'</h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>';
    ?>
</div>
    <!-- slider Area End-->
    <!-- ================ contact section start ================= -->
    <section class="contact-section">
            <div class="container">
                <div class="row">
                <div class="col-12">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d1971.3074574189698!2d115.15815!3d-8.8221989!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2dd25b01e4f57fe5%3A0xb863b6d2c5a2575f!2sVilla%20lussy%20bali!5e0!3m2!1sid!2sid!4v1608700985182!5m2!1sid!2sid" width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                    </div>
                    <div class="col-12">
                        <h2 class="contact-title"></h2>
                    </div>
                    
                    <div class="col-lg-8">
                        <!-- <form class="form-contact contact_form" action="contact_process.php" method="post" id="contactForm" novalidate="novalidate">
                            <div class="row">
                                <div class="col-12">
                                    <div class="form-group">
                                        <textarea class="form-control w-100" name="message" id="message" cols="30" rows="9" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter Message'" placeholder=" Enter Message"></textarea>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <input class="form-control valid" name="name" id="name" type="text" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter your name'" placeholder="Enter your name">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <input class="form-control valid" name="email" id="email" type="email" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter email address'" placeholder="Email">
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="form-group">
                                        <input class="form-control" name="subject" id="subject" type="text" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter Subject'" placeholder="Enter Subject">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group mt-3">
                                <button type="submit" class="button button-contactForm boxed-btn">Send</button>
                            </div>
                        </form> -->
                        <table class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th scope="col"><?= $kolom_1->words ?></th>
                                <th scope="col"><?= $kolom_2->words ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td><?= $baris_1->words ?></td>
                                <td><?= $harga->DETAIL_CONFIG ?>€/<?= $malam->words ?></td>
                            </tr>
                            <tr>
                                <td><?= $baris_2->words ?></td>
                                <td><?= $hargaw->DETAIL_CONFIG ?>€</td>
                            </tr>
                            <tr>
                                <td><?= $baris_3->words ?></td>
                                <td><?= $baris_4->words ?></td>
                            </tr>
                            
                        </tbody>
                        </table>
                         <?= $bawah->words ?><br><br>
                    </div>
                    <div class="col-lg-3 offset-lg-1">
                        <div class="media contact-info">
                            <span class="contact-info__icon"><i class="ti-home"></i></span>
                            <div class="media-body">
                                <h3><?= $alamat ?></h3>
                                <p>Bali, Indonesia</p>
                            </div>
                        </div>
                        <div class="media contact-info">
                            <span class="contact-info__icon"><i class="ti-tablet"></i></span>
                            <div class="media-body">
                                <h3><?= $phone ?></h3>
                                <p><?= $samping1->words ?></p>
                            </div>
                        </div>
                        <div class="media contact-info">
                            <span class="contact-info__icon"><i class="ti-email"></i></span>
                            <div class="media-body">
                                <h3><?= $email ?></h3>
                                <p><?= $samping->words ?></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    <!-- ================ contact section end ================= -->
    