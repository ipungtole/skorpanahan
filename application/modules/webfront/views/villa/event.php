
<!-- slider Area End-->
<div class="slider-area ">
    <?php
        $data = $this->global->get_data('m_gallery',['type' => 'slider','deleted' => null,'status' => 1],null)->row();
        echo '<div class="single-slider slider-height2 d-flex align-items-center" data-background="'.'images/'.$data->nama_gambar.'">
                <div class="container">
                    <div class="row">
                        <div class="col-xl-12">
                            <div class="hero-cap text-center">
                                <h2>'.$sub_title.'</h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>';
    ?>
</div>

<!-- Favourite Places Start -->
<div class="favourite-place place-padding">
    <div class="container">
        <!-- Section Tittle -->
        <div class="row">
            <div class="col-lg-12">
                <div class="section-tittle text-center">
                    <span>Picture made villa lussy</span>
                    <h2><?= $sub_title ?></h2>
                </div>
            </div>
        </div>
				<div class="row">
                    <?php
                        $gallery = $this->global->get_data_order('m_event',['status_event' => 1,'deleted' => null],'id_event','desc')->result();
                        // var_dump($gallery);
                        $i= 1;
                        foreach ($gallery as $gl) {
                            $i++;
                            echo '
                            <div class="col-xl-4 col-lg-4 col-md-6">
                                <div class="single-place mb-30">
                                    <div class="place-img">
                                        <img src="'.base_url('images/'.$gl->image_event).'" alt="" data-pagespeed-url-hash="527648263" onload="pagespeed.CriticalImages.checkImageForCriticality(this);">
                                    </div>
                                    <div class="place-cap">
                                        <div class="place-cap-top">
                                            <span><i class="fas fa-star"></i><span>8.0 Superb</span> </span>
                                            <h3><a href="#">'.$gl->nama_event.'</a></h3>
                                            <p class="dolor">$1870 <span>/ Per Person</span></p>
                                        </div>
                                        <div class="place-cap-bottom">
                                            <ul>
                                                <li id="register"><i class="far fa-clock"></i>Register</li>
                                                <li><i class="fas fa-map-marker-alt"></i>Los Angeles</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>';
                          
                        }
                    ?>



				
        </div>
    </div>
</div>