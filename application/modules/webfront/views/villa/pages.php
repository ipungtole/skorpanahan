<div class="slider-area ">
    <?php
        $data = $this->global->get_data('m_gallery',['type' => 'slider','deleted' => null,'status' => 1],null)->row();
        echo '<div class="single-slider slider-height2 d-flex align-items-center" data-background="'.'images/'.$data->nama_gambar.'">
                <div class="container">
                    <div class="row">
                        <div class="col-xl-12">
                            <div class="hero-cap text-center">
                                <h2>'.$sub_title.'</h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>';
    ?>
</div>

<?php
    if($page->konten){
        echo $page->konten;
    }else{
        echo "Halaman Belum Tersedia";
    }
?>