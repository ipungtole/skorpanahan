<!-- slider Area Start-->
<div class="slider-area ">
    <!-- Mobile Menu -->
    <!-- <div class="slider-active"> -->
    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
        <?php
            $data = $this->global->get_data('m_gallery',['type' => 'slider','deleted' => null,'status' => 1],null)->result();
            echo '<ol class="carousel-indicators">';
            $i = 0;
            foreach ($data as $slide) {
                $no = $i++;
                $active = ($no == 0)?'class="active"':'';
                echo '<li data-target="#carouselExampleIndicators" data-slide-to="'.$no.'" '.$active.'></li>';
            }
            echo '</ol>';
            echo '<div class="carousel-inner">';
            $ii = 0;
            foreach ($data as $slides2) {
                $no2        = $ii++;
                $active2    = ($no2 == 0)?' active':'';
                echo '<div class="carousel-item'.$active2.'">
                        <img class="d-block w-100" src="'.base_url('images/'.$slides2->nama_gambar).'" alt="First slide">
                    </div>';
            }
            echo '</div>
                    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                   ';
            
            // echo '<div class="single-slider hero-overly  slider-height d-flex align-items-center" data-background="'.'images/'.$data->nama_gambar.'">
            //         <div class="container">
            //             <div class="row">
            //                 <div class="col-xl-9 col-lg-9 col-md-9">
            //                     <div class="hero__caption">
            //                         <h1>'.$data->caption_gambar.' </h1>
            //                         <p>Where would you like to go?</p>
            //                     </div>
            //                 </div>
            //             </div>
            //         </div>
            //     </div>';

            // var_dump($data);exit;
        ?>
        </div>
    </div>
</div>
<!-- slider Area End-->
<div class="favourite-place place-padding">
            <div class="container">
                <!-- Section Tittle -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="section-tittle text-center">
                            <?php
                                // $lang = $this->global->get_data('t_dictionary',['id_language' => $lang['id_language'],'id_parent' => 1,'deleted' => null],null)->row();
                                // echo "<h2>$lang->words</h2>";
                                $bahasawel = $this->global->get_data_order('t_dictionary',['id_modul' => 2],'id_dictionary','desc')->result();
                                $hit = 0;
                                foreach ($bahasawel as $bs) {
                                    $hit++;
                                    if($hit == 1){
                                        // echo "<h2>$bs->words</h2>";
                                        echo "<h1>$bs->words</h1>";
                                    }else{
                                        echo "<h1>$bs->words</h1>";
                                    }
                                }
                                // var_dump($bahasawel);
                            ?>
                            <bR>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <?php
                        $gallery = $this->global->get_data('m_gallery',['home' => 1,'deleted' => null],null)->result();
                        if($gallery){
                            foreach ($gallery as $gal) {
                                echo '<div class="col-xl-4 col-lg-4 col-md-6">
                                        <div class="single-place mb-30">
                                            <div class="place-img">
                                                <img src="images/'.$gal->nama_gambar.'" alt="'.$gal->caption_gambar.'">
                                            </div>
                                            <div class="place-cap">
                                                <div class="place-cap-top">
                                                </div>
                                            </div>
                                        </div>
                                    </div>';
                            }
                        }
                    ?>
                    


                </div>
            </div>
        </div>
<!-- Our Services Start -->
<!-- <div class="our-services servic-padding">
    <div class="container">
        <div class="row d-flex justify-contnet-center">
            <?php
                $partner = $this->global->get_data('m_partner',['deleted' => null],null)->result();
                if($partner){
                    foreach ($partner as $pa) {
                        echo '<div class="col-xl-3 ">
                            <div class="single-services text-center mb-30">
                                <div class="services-ion">
                                    <img class="d-block w-100" src="'.base_url('images/partner/'.$pa->images).'" alt="'.$pa->partner_name.'">
                                </div>
                                <div class="services-cap">
                                    <h5>'.$pa->partner_name.'<br>'.$pa->links.'</h5>
                                </div>
                            </div>
                        </div>';
                    }
                    
                }
            ?>
            
        </div>
    </div>
</div> -->
    <!-- Our Services End -->