
<!-- slider Area End-->
<div class="slider-area ">
    <?php
        $data = $this->global->get_data('m_gallery',['type' => 'slider','deleted' => null,'status' => 1],null)->row();
        echo '<div class="single-slider slider-height2 d-flex align-items-center" data-background="'.'images/'.$data->nama_gambar.'">
                <div class="container">
                    <div class="row">
                        <div class="col-xl-12">
                            <div class="hero-cap text-center">
                                <h2>'.$sub_title.'</h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>';
    ?>
</div>

<!-- Favourite Places Start -->
<div class="favourite-place place-padding">
    <div class="container">
        <!-- Section Tittle -->
        <div class="row">
            <div class="col-lg-12">
                <div class="section-tittle text-center">
                    <span>Picture made villa lussy</span>
                    <h2><?= $sub_title ?></h2>
                </div>
            </div>
        </div>
				<div class="row gallery-item">
                    <?php
                        $gallery = $this->global->get_data_order('m_gallery',['type' => 'gallery','status' => 1,'deleted' => null],'id_gallery','desc')->result();
                        // var_dump($gallery);
                        $i= 1;
                        foreach ($gallery as $gl) {
                            $i++;
                            #OPSI 1
                            // $col = ($i <= 4)?'4':'6';
                            #OPSI 2
                            // $col = ($i <= 5)?'3':'4';
                            $col = ($i <= 7)?'2':'3';
                            echo '<div class="col-md-'.$col.'">
                                    <a href="'.base_url('images/'.$gl->nama_gambar).'" class="img-pop-up">
                                        <div class="single-gallery-image" style="background: url('.base_url('images/small/'.$gl->nama_gambar).');"></div>
                                    </a>
                                </div>';
                            #OPSI 1
                            // if($i == 6){
                            //     $i = 1;
                            // }
                            #OPSI 2
                            // if($i == 8){
                            //     $i = 1;
                            // }
                            if($i == 11){
                                $i = 1;
                            }
                        }
                    ?>



				
        </div>
    </div>
</div>
<!-- Favourite Places End -->