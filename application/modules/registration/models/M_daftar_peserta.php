<?php defined('BASEPATH') OR exit('No direct script access allowed');

Class M_daftar_peserta extends CI_Model {
    var $table = 't_peserta';
    var $column_order = array('id_peserta','nama_peserta','nik'); //set column field database for datatable orderable
    var $column_search = array('nama_peserta','nik'); //set column field database for datatable searchable just firstname , lastname , address are searchable
    var $order = array('id_peserta' => 'desc'); // default order 
 
 
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }
 
    private function _get_datatables_query(){
         
        $this->db->select('t_peserta.*,m_event.nama_event,m_status.nama_status');
        $this->db->from($this->table);
        $this->db->join('m_event','m_event.id_event = t_peserta.id_event','left');
        $this->db->join('m_status','m_status.id_status = t_peserta.status_peserta','left');
        $this->db->where(['t_peserta.deleted' => null,'m_event.status_event' => 1]);

		
        $i = 0;
     
        foreach ($this->column_search as $item) // loop column 
        {
            if($this->input->post('search')) // if datatable send POST for search
            {
                 
                if($i===0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
 
                if(count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }
         
        if(isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
 
    function get_datatables(){
        $this ->_get_datatables_query();
        if($this->input->post('length') != -1)
        $this->db->limit($this->input->post('length'),$this->input->post('start'));
        $query = $this->db->get();
        return $query->result();
    }
 
    function count_filtered( ){
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
 
    public function count_all(){
        $this->_get_datatables_query();
        return $this->db->count_all_results();
    }
    
}
