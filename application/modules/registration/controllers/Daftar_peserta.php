<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Daftar_peserta extends MY_Controller {
    function __construct(){
		parent::__construct();
		$this->load->library('template');
        $this->load->model('dashboard/m_global','global');
        $this->load->helper('rupiah_helper');
		
        $akses = modules::run('dashboard/login/role_acess');
        
    	if(!$akses){
    		$forbidden = modules::run('dashboard/login/forbidden');
    		echo $forbidden;
    		exit;
    	}

    }

    public function index(){
        $act            = $this->input->get('act');
        $data['rules']  = modules::run('dashboard/login/get_role_acess');
        // var_dump($data['rules']);exit;
      
    	switch ($act) {
            case 'list':
                $this->access('list_data');
    			$this->list($data['rules']->view_button,$data['rules']->edit_button,$data['rules']->delete_button,$data['rules']->verification_button,$data['rules']->link_menu);
                break;
            case 'add':
                $this->access('add_button');
                $this->add();
                break;
            case 'edit':
                $this->access('edit_button');
                $this->update();
                break;
            case 'delete':
                $this->access('delete_button');
                $id  = $this->input->get('id');
                $this->delete($id);
                break;
            case 'verification':
                $this->access('verification_button');
                $id     = $this->input->get('id');
                $verif  = $this->input->get('verif');
                $this->verification($id,$verif);
                break;
            case 'detail':
                $id  = $this->input->get('id');
                $this->detail($id);
                break;
            case 'events':
                $this->events();
                break;
            default:
                $this->access('list_data');
				$this->template->dashboards($data['rules']->link_menu,$data);
    			break;
    	}
    }

  
    private function access($act){
        $data = modules::run('dashboard/login/get_role_acess');
        if(!$data->$act){
            $forbidden = modules::run('dashboard/login/forbidden');
            echo $forbidden;
            exit;
        }
    }

    private function list($view,$edit,$delete,$verif,$url){
        $method		= $this->input->method();
		if($method != 'post'){
			$forbidden = modules::run('dashboard/login/forbidden');
    		echo $forbidden;
			exit;
		}	
        $url_exp    = explode('/',$url);
        $alias      = $url_exp[1];
        $this->load->model($url_exp[0].'/m_'.$url_exp[1],$alias);
        
        $list 		= $this->$alias->get_datatables();
		$data 		= array();
        $no 		= $this->input->post('start');
        // $id         = '$pages->id_'.$alias;
		
		foreach ($list as $pages) {

			$b_view = ($view)?'<a class="btn bg-teal btn-xs waves-effect" href="javascript:void(0)" title="View" onclick="view('."'".$pages->id_peserta."'".')"><i class="material-icons">chat</i> View</a> ':'';

			$b_edi 	= ($edit)?'<a class="btn bg-orange btn-xs waves-effect" href="javascript:void(0)" title="Edit" onclick="edit('."'".$pages->id_peserta."'".')"><i class="material-icons">border_color</i> Edit</a> ':'';
			
			$b_del	= ($delete)?'<a class="btn bg-red btn-xs waves-effect" href="javascript:void(0)" title="Hapus" onclick="hapus('."'".$pages->id_peserta."'".')"><i class="material-icons ">delete</i> Delete</a> ':'';
			
            $b_verif= ($verif)?'<a class="btn bg-blue btn-xs waves-effect" href="javascript:void(0)" title="Validasi" onclick="validasi('."'".$pages->id_peserta."'".')"><i class="material-icons ">verified_user</i> Validasi</a> ':'';

            $staper = ($pages->status_peserta)?'':$b_verif;

			$no++;
			$row = array();
			$row[] = $no;
			$row[] = $pages->nik;
			$row[] = $pages->nama_peserta;
			$row[] = $pages->nama_event;
			$row[] = $pages->tanggal_daftar;
			$row[] = $pages->nama_status;
			
			
			$row[] = $staper.$b_view.$b_edi.$b_del;
			
			$data[] = $row;
		}
	
		$csrf_name = $this->security->get_csrf_token_name();
		$csrf_hash = $this->security->get_csrf_hash(); 

		$output = array(
						"draw" 				=> $this->input->post('draw'),
						"recordsTotal" 		=> $this->$alias->count_all(),
						"recordsFiltered" 	=> $this->$alias->count_filtered(),
						"data" 				=> $data,
				);
		$json_data[$csrf_name] = $csrf_hash; 
		echo json_encode($output);
    }

    private function add(){
        $method		= $this->input->method();
		if($method != 'post'){
			$forbidden = modules::run('dashboard/login/forbidden');
    		echo $forbidden;
			exit;
        }
        
        
        $this->validasi();
		$data = [
			'nama_peserta'      => $this->input->post('nama_peserta'),
			'nik'               => $this->input->post('nik'),
			'id_event'          => $this->input->post('event'),
			'tanggal_daftar'    => date('Y-m-d H:i:s')
		];
        $this->global->insert('t_peserta',$data);
        echo json_encode(['status' => true,'pesan' => 'Data berhasil di simpan']);
    }

    private function update(){
        $method     = $this->input->method();
        if($method != 'post'){
            $forbidden = modules::run('dashboard/login/forbidden');
            echo $forbidden;
            exit;
        }
        
        $id         = $this->input->post('id_peserta');
        $this->validasi();
        $data = [
			'nama_peserta'      => $this->input->post('nama_peserta'),
			'nik'               => $this->input->post('nik'),
			'id_event'          => $this->input->post('event')
		];
        $this->global->update_tabel('t_peserta',$data,['id_peserta' => $id]);
        echo json_encode(['status' => true,'pesan' => 'Data berhasil di update']);
    }

    private function delete($id){
        $this->global->update_tabel('t_peserta',['deleted' => 1],['id_peserta' => $id]);
        echo json_encode(['status' => true,'pesan' => 'Data berhasil di hapus']);
    }

    private function verification($id,$verif){
        $data = $this->global->get_data('t_peserta',['id_peserta' => $id])->row();
        if($data){
            if(!$data->status_peserta){
                $this->global->update_tabel('t_peserta',['status_peserta' => $verif],['id_peserta' => $id]);
                $result = ['status' => true,'pesan' => 'berhasil memverifikasi peserta'];
            }else{
                $result = ['status' => false,'pesan' => 'Peserta Sudah Di Verifikasi'];
            }
            echo json_encode($result);
        }else{
            echo json_encode(['status' => false,'pesan' => 'data tidak ditemukan']);
        }
    }

    private function detail($id){
        $data = $this->global->get_data('t_peserta',['id_peserta' => $id])->row();
        if($data){
            $result = array_merge(['status' => true],(array)$data);
            echo json_encode($result);
        }else{
            echo json_encode(['status' => false,'pesan' => 'data tidak ditemukan']);
        }
    }

    private function events(){
        $data = $this->global->get_data('m_event',['status_event' => 1])->result();
        if($data){
            $result = array_merge(['status' => true,'result' => (array)$data]);
            echo json_encode($result);
        }else{
            echo json_encode(['status' => false,'pesan' => 'data tidak ditemukan']);
        }
    }

    private function validasi(){
        $data                   = array();
        $data['error_string']   = array();
        $data['inputerror']     = array();
        $data['status']         = TRUE;

        if($this->input->post('nama_peserta') == ''){
            $data['inputerror'][] = 'nama_peserta';
            $data['error_string'][] = 'Nama Pesrta harus di isi';
            $data['status'] = FALSE;
        }

        if($this->input->post('nik') == ''){
            $data['inputerror'][] = 'nik';
            $data['error_string'][] = 'Nik harus di isi';
            $data['status'] = FALSE;
        }

        if($this->input->post('event') == ''){
            $data['inputerror'][] = 'event';
            $data['error_string'][] = 'Event harus di isi';
            $data['status'] = FALSE;
        }
        if( $this->input->get('act') == 'add'){
            if($this->input->post('nik')){
                $datab = $this->global->get_data('t_peserta',['nik' => $this->input->post('nik'),'id_event' => $this->input->post('event')])->row();
                if($datab){
                    $data['inputerror'][] = 'nik';
                    $data['error_string'][] = 'Nik sudah terdaftar';
                    $data['status'] = FALSE;
                }
            }
        }
      
        if($data['status'] === FALSE){
            echo json_encode($data);
            exit();
        }
    }

}
