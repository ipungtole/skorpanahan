<?php
echo link_tag('assets/admin/' . $a_theme . '/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css');
echo link_tag('assets/admin/' . $a_theme . '/plugins/bootstrap-datepicker/css/bootstrap-datepicker.css');

?>
<section class="content">
	<div class="container-fluid">
		<div class="block-header">
			<h2>
				<ol class="breadcrumb breadcrumb-bg-pink">
					<li><a href="javascript:void(0);"><i class="material-icons">home</i> Home</a></li>
					<?php
					$aks = $akses[0]['link_menu'];
					if ($aks) {
						$exp = explode('/', $aks);
						$last = $this->uri->total_segments();
						$end = $this->uri->segment($last);
						foreach ($exp as $key) {
							if($key == $end){
								$d 	= 'class="active"';
								$a 	= '';
								$a1 = '';
								$en = $akses[0]['nama_menu'];
							}else{
								$a 	= '<a href="javascript:void(0);">';
								$d 	= '';
								$a1 = '</a>';
								$en = $key;
							}
							echo '<li '.$d.'> ' . $a . ucwords(str_replace('_',' ',$en)) . $a1 . '</li>';
						}
					}
					?>
				</ol>
			</h2>
		</div>
		<!-- Basic Examples -->
		<div class="row clearfix">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="card">
					<div class="header">
						<h2>
							<?= $akses[0]['nama_menu'] ?>
						</h2>

					</div>
					<div class="body">
						<?php
                            if ($rules->add_button) {
                                echo '
                                <ol class="breadcrumb align-right">
                                    <li>
                                        <a class="btn bg-cyan btn-sm waves-effect" href="javascript:void(0)" title="Add" onclick="add()"><i class="material-icons">library_add</i> Tambah Baru</a>
                                    </li>
                                </ol>';
                            }
                        ?>
						<div class="table-responsive">
							<table id="list_data" class="table table-bordered table-striped table-hover dataTable table-responsive">
								<thead>
									<tr>
										<th>No</th>
										<th>Gambar</th>
										<th>Nama</th>
										<th>Tanggal</th>
										<th>Lap</th>
										<th>Status</th>
										<th>Action</th>
									</tr>
								</thead>
							</table>
						</div>

					</div>
				</div>
			</div>
		</div>
		<!-- #END# Basic Examples -->

	</div>
</section>

<div class="modal fade" id="modals" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title" id="defaultModalLabel">Modal title</h4>
			</div>
			<div class="modal-body">
				<form id="form" class="form-horizontal">
					<?= $csrf ?>
					<div class="form-group">
						<label class="control-label col-md-5"> Nama Event</label>
						<div class="col-md-7">
							<input type="text" name="nama_event" id="nama_event" class="form-control" placeholder="Nama Event">
							<span class="help-block"></span>
							<input type="hidden" name="id_event" id="id_event">
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-5"> Tanggal Event</label>
						<div class="col-md-7">
							<input type="text" name="date_event" id="date_event" class="form-control" placeholder="Tanggal Event">
							<span class="help-block"></span>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-5"> Gambar</label>
						<div class="col-md-7">
							<input name="gambar" id="photo" onchange="validasiFile();" type="file">
                                <div id="thumbnail">
                                    <img id="blah" alt="Gambar Thumbnail" >
                                  </div>
                                <span class="help-block"></span>
							<input type="hidden" name="id_gallery" id="id_gallery">
						</div>
					</div>
					<div class="form-group" id="photo-preview">
						<label class="control-label col-md-5">Photo</label>
						<div class="col-md-7">
							(No photo)
							<span class="help-block"></span>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-5"> Lap</label>
						<div class="col-md-7">
							<input type="text" name="lap" id="lap" class="form-control" placeholder="Jumlah Panahan">
							<span class="help-block"></span>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-5"> Lokasi</label>
						<div class="col-md-7">
							<input type="text" name="lokasi_event" id="lokasi_event" class="form-control" placeholder="Lokasi">
							<span class="help-block"></span>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-5"> Tanggal Pendaftaran Mulai</label>
						<div class="col-md-7">
							<input type="text" name="start_date_registration" id="start_date_registration" class="form-control" placeholder="Tanggal Pendaftaran Mulai">
							<span class="help-block"></span>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-5"> Tanggal Pendaftaran Selesai</label>
						<div class="col-md-7">
							<input type="text" name="finish_date_registration" id="finish_date_registration" class="form-control" placeholder="Tanggal Pendaftaran Selesai">
							<span class="help-block"></span>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-5"> Keterangan</label>
						<div class="col-md-7">
							<input type="text" name="description" id="description" class="form-control" placeholder="Keterangan">
							<span class="help-block"></span>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-5"> Status </label>
						<div class="col-md-7">
							<div class="form">
								<input name="status" type="radio" class="with-gap" value="1" id="radio_3" checked/>
								<label for="radio_3">Aktif</label>
								<input name="status" type="radio" class="with-gap" value="0" id="radio_4">
								<label for="radio_4">Non Aktif</label>
								
							</div>
						</div>
					</div>


				</form>
			</div>
			<div class="modal-footer">
				<button type="button" onclick="simpan()" class="btn btn-link waves-effect">SIMPAN</button>
				<button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
			</div>
		</div>
	</div>
</div>

<script src="<?php echo base_url('assets/admin/' . $a_theme . '/plugins/jquery-datatable/jquery.dataTables.js') ?>"></script>
<script src="<?php echo base_url('assets/admin/' . $a_theme . '/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js') ?>"></script>
<script src="<?php echo base_url('assets/admin/' . $a_theme . '/plugins/momentjs/moment.js') ?>"></script>
<script src="<?php echo base_url('assets/admin/' . $a_theme . '/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js') ?>"></script>
<script src="<?php echo base_url('assets/admin/' . $a_theme . '/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js') ?>"></script>
<script type="text/javascript">
	var save_method; //for save method string
	var table;
	var base_url = '<?php echo base_url(); ?>';
	// var hash2       ;
	var hash = '<?= $this->security->get_csrf_hash(); ?>';

	$(function() {
		// $("input").change(function() {
		// 	$(this).parent().parent().removeClass('has-error');
		// 	$(this).next().empty();
		// });

		$('#date_event').bootstrapMaterialDatePicker({
            time: false
        });

		$('#start_date_registration').bootstrapMaterialDatePicker({
            format: 'YYYY-MM-DD HH:mm:ss'
        });

		$('#finish_date_registration').bootstrapMaterialDatePicker({
            format: 'YYYY-MM-DD HH:mm:ss'
        });


		table = $('#list_data').DataTable({
			"responsive": true,
			"processing": true, //Feature control the processing indicator.
			"serverSide": true, //Feature control DataTables' server-side processing mode.
			"order": [], //Initial no order.

			// Load data for the table's content from an Ajax source
			"ajax": {
				"url": "<?php echo site_url($url . '?act=list') ?>",
				"type": "POST",
				"data": {
					rania_altha: hash
				},
			},

			//Set column definition initialisation properties.
			"columnDefs": [{
				"targets": [-1], //last column
				"orderable": false, //set not orderable
			}, ],

		});
	});

	function add() {
		$('.form-group').removeClass('has-error'); // clear error class
		$('.help-block').empty(); // clear error string
		$('#form').trigger("reset");
		$('#modals').modal('show');
		$('#thumbnail').hide();
    	$('.help-block').empty(); // clear error strdeletetokoing
    	$('#photo-preview').hide();
		$('.modal-title').text('Tambah <?= $akses[0]['nama_menu'] ?> Baru');
		save_method = 'add';
	}

	function edit(param) {
		detail(param);
		save_method = 'edit';
		$('#modals').modal('show');
		$('.modal-title').text('Edit <?= $akses[0]['nama_menu'] ?> ');
	}

	function detail(id) {
		$.ajax({
			url: "<?php echo site_url($url . '?act=detail&id=') ?>" + id,
			type: 'GET',
			dataType: 'json',
			success: function(data) {
				$("[name='id_event']").val(data.id_event);
				$("[name='nama_event']").val(data.nama_event);
				$("[name='date_event']").val(data.date_event);
				$("[name='lap']").val(data.lap);
				$("[name='lokasi_event']").val(data.lokasi_event);
				$("[name='start_date_registration']").val(data.start_date_registration);
				$("[name='finish_date_registration']").val(data.finish_date_registration);
				$("[name='description']").val(data.description);
				$("input[name=status_event][value="+data.status_event+"]").attr('checked', 'checked');
				if(data.image_event){
					$('#label-photo').text('Change Photo'); // label photo upload
					$('#photo-preview div').html('<img src="'+base_url+'images/small/'+data.image_event+'" class="img-responsive">'); // show photo
					$('#photo-preview div').append('<input type="checkbox" name="remove_photo" value="'+data.nama_gambar+'"/> Remove photo when saving'); // remove photo
				}else{
					$('#label-photo').text('Upload Photo'); // label photo upload
					$('#photo-preview div').text('(No photos)');
				}
			}
		});
	}


	function simpan() {
		$('#btnSave').text('saving...'); //change button text
		$('#btnSave').attr('disabled', true); //set button disable 
		var urls;

		var formData = new FormData($('#form')[0]);
		if (save_method == 'add') {
			urls = '<?php echo site_url($url . '?act=add') ?>';
		} else {
			urls = '<?php echo site_url($url . '?act=edit') ?>';
		}
		// ajax adding data to database
		$.ajax({
			url: urls,
			type: "POST",
			data: formData,
			contentType: false,
			processData: false,
			dataType: "JSON",
			success: function(data) {

				if (data.status) { //if success close modal and reload ajax table
					if (save_method == 'add') {

						$.confirm({
							icon: 'fa fa-smile-o',
							title: 'Yeaah....',
							content: data.pesan,
							theme: 'modern',
							closeIcon: true,
							type: 'green',
							animation: 'rotate',
							buttons: {
								IsiLagi: function() {
									$('#form').trigger("reset");
									table.ajax.reload(null, false);
								},
								Tidak: function() {
									$('#modals').modal('hide');
									table.ajax.reload(null, false);
								}
							}
						});

					} else {
						$.confirm({
							icon: 'fa fa-smile-o',
							title: 'Yeaah....',
							content: data.pesan,
							theme: 'modern',
							closeIcon: true,
							type: 'orange',
							animation: 'rotate',
							buttons: {
								Ok: function() {
									$('#modals').modal('hide');
									table.ajax.reload(null, false);
								}
							}
						});
					}

				} else {
					for (var i = 0; i < data.inputerror.length; i++) {
						if (data.inputerror[i] == 'cabang' || data.inputerror[i] == 'nama_kapal') {
							$('[name="' + data.inputerror[i] + '"]').parent().addClass('error'); //select parent twice to select div form-group class and add has-error class
							$('[name="' + data.inputerror[i] + '"]').next().next().text(data.error_string[i]); //select span help-block class set text error string
						} else {
							$('[name="' + data.inputerror[i] + '"]').parent().parent().addClass('has-error'); //select parent twice to select div form-group class and add has-error class
							$('[name="' + data.inputerror[i] + '"]').next().text(data.error_string[i]);
						}
					}



					$('#btnSave').html('<i class="material-icons">save</i>  Simpan'); //change button text
					$('#btnSave').attr('disabled', false); //set button enable 
				}
			},
			error: function(jqXHR, textStatus, errorThrown) {

			}
		});
	}

	function hapus(id) {
		$.confirm({
			icon: 'fa fa-trash',
			title: 'Hapus',
			content: 'Apakah Anda Akan Menghapus Data ini ?',
			theme: 'modern',
			closeIcon: true,
			type: 'red',
			animation: 'rotate',
			buttons: {
				Ya: function() {
					$.ajax({
						url: '<?php echo site_url($url . '?act=delete&id=') ?>' + id,
						type: "GET",
						dataType: "JSON",
						success: function(data) {
							if (data.status) {
								var icon = 'fa fa-check';
								var judul = 'Hapus Data Berhasil';
								var warna = 'green';
							} else {
								var icon = 'fa fa-times';
								var judul = 'Hapus Data Gagal';
								var warna = 'red';

							}
							$.alert({
								icon: icon,
								title: judul,
								content: '' + data.pesan + '',
								theme: 'modern',
								closeIcon: true,
								type: warna,
								animation: 'rotate'
							});
							table.ajax.reload(null, false);
						},
						error: function(jqXHR, textStatus, errorThrown) {
							alert('Error get data from ajax');
						}
					});
				},

				Tidak: function() {

				}
			}
		});
	}

	function validasiFile(){
      var inputFile = document.getElementById('photo');
      var pathFile = inputFile.value;
      // var ekstensiOk = /(\.jpg|\.jpeg|\.png|\.gif)$/i;
      var ekstensiOk = /(\.jpg|\.jpeg|\.png|\.gif|\.webp|\.jp2|\.svg)$/i;

      if(!ekstensiOk.exec(pathFile)){
          // alert('Silakan upload file yang memiliki ekstensi .jpeg/.jpg/.png/.gif');
          swal({
            title: "Warning!",
            text: "Silakan upload file yang memiliki ekstensi .jpeg/.jpg/.png/.gif!",
            type: "warning",
          });
          inputFile.value = '';
          return false;
      }else{
          //Pratinjau gambar
          $('#thumbnail').show();
          if (inputFile.files && inputFile.files[0]) {
              var reader = new FileReader();
              reader.onload = function(e) {
                  document.getElementById('thumbnail').innerHTML = '<br><img width="100%" src="'+e.target.result+'"/>';
              };
              reader.readAsDataURL(inputFile.files[0]);
        $("#gambar").hide();
          }
      }
      // alert('textStatus');
    }
</script>
