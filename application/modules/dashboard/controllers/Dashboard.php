<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends MY_Controller {
	function __construct(){
		parent::__construct();
		$this->load->library('template');
		$this->load->model('dashboard/m_global','global');
		$session 	= $this->session->userdata('logged_in');
		
		if(!$session){
			redirect('lcontrol', 'refresh');
		}
	}

	public function index(){
		// $a = modules::run('dashboard/login')->tes('a'); 
		// $a = modules::load('dashboard/login')->tes('ab'); 
		// echo $a;
		// echo "yes";
		$this->template->dashboards('isi');
    }

}