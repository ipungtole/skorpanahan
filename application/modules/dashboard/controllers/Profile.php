<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends MY_Controller {
	function __construct(){
        parent::__construct();
        $this->load->library('template');
		$this->load->model('dashboard/m_global','global');

		$session 	= $this->session->userdata('logged_in');
		if(!$session){
			redirect('lcontrol', 'refresh');
		}
	}

	public function index(){
		$act = $this->input->get('act');
		switch ($act) {
			case 'edit':
				$this->ubah_password();
				break;
			case 'edit_profile':
				$this->ubah_profile();
				break;
			case 'detail':
				$session	= modules::run('dashboard/login/session');
				$data 		= $this->global->profile_detail(['tbl_user_detail.id_user' => $session['id_user']])->row();
				echo json_encode($data);
				break;
			default:
				$this->template->all_acess('profile');
				break;
		}
	}

	private function ubah_password(){
		$this->validasi();

		#LOAD ENCRYPT DATA
		$this->load->library('encryption');
		#LOAD SESSION
		$session	= modules::run('dashboard/login/session');
		$data = [
				'password'	=> $this->encryption->encrypt($this->input->post('new_password'))
			];
		$this->global->update_tabel('tbl_user', $data,['id_user' => $session['id_user']]);
		echo json_encode(['status' => true,'pesan' => 'password berhasil di rubah']);
		
	}

	private function ubah_profile(){
		#LOAD SESSION
		$session	= modules::run('dashboard/login/session');
		$data = [
				'nama_lengkap_user'	=> $this->input->post('NameSurname'),
				'email'				=> $this->input->post('email'),
				'alamat_user'		=> $this->input->post('alamat'),
				'no_telp_user'		=> $this->input->post('no_telp_user')
			];
		if(!empty($_FILES['gambar']['name'])){
            $uploads = $this->_do_upload();
            $detail = $this->global->get_data('tbl_user_detail',['id_user' => $session['id_user']])->row();
			#DELETED FILES
			if($detail->gambar_user){
				if(file_exists('images/'.$detail->gambar_user)){
					unlink('images/'.$detail->gambar_user);
				}
			}
            #END DELETED FILES
            
            $data['gambar_user'] = $uploads;
        }
		$this->global->update_tabel('tbl_user_detail', $data,['id_user' => $session['id_user']]);
		echo json_encode(['status' => true,'pesan' => 'Profile berhasil di rubah']);
	}

	private function _do_upload(){
        $config['upload_path']          = 'images/';
		$config['allowed_types']        = 'gif|jpg|jpeg|png|svg';
		// $config['allowed_types']        = 'svg';
        $config['max_size']             = 5000; //set max size allowed in Kilobyte
        $config['max_width']            = 8000; // set max width image allowed
        $config['max_height']           = 5000; // set max height allowed
        $config['file_name']            = round(microtime(true) * 1000); //just milisecond timestamp fot unique name
 
        $this->load->library('upload', $config);

        if(!$this->upload->do_upload('gambar')){
            $data['inputerror'][] 	= 'gambar';
            $data['error_string'][] = 'Upload error: '.$this->upload->display_errors('',''); 
            $data['status'] 		= FALSE;
            echo json_encode($data);
            exit();
        }
		$rre = $this->upload->data('file_name');
		$ex = explode('.',$rre);
		if($ex[1] != 'svg'){
            $this->_create_thumbs($this->upload->data('file_name'));
        }
		
        return $this->upload->data('file_name');
    }

	private function _create_thumbs($file_name){
        // Image resizing config
        $config = array(
            
            array(
                'image_library' => 'GD2',
                'source_image'  => 'images/'.$file_name,
                'maintain_ratio'=> FALSE,
                'width'         => 300,
                'height'         => 300,
                'new_image'     => './images/'.$file_name
                ));
 
        $this->load->library('image_lib', $config[0]);
        foreach ($config as $item){
            $this->image_lib->initialize($item);
            if(!$this->image_lib->resize()){
                return false;
            }
            $this->image_lib->clear();
        }
    }

	private function validasi(){
		$data                   = array();
        $data['error_string']   = array();
        $data['inputerror']     = array();
        $data['status']         = TRUE;
		
		#LOAD ENCRYPT DATA
		$this->load->library('encryption');
		#LOAD SESSION
		$session	= modules::run('dashboard/login/session');
		
        if($this->input->post('old_password') == ''){
            $data['inputerror'][] = 'old_password';
            $data['error_string'][] = 'Password Lama harus di isi';
            $data['status'] = FALSE;
		}
		
		if($this->input->post('old_password')){
			$get_user 		= $this->global->get_data('tbl_user',['id_user' => $session['id_user']])->result();
			$currentPass 	= $get_user[0]->password;
			$dPass 			= $this->encryption->decrypt($get_user[0]->password);
			if($dPass != $this->input->post('old_password')){
				$data['inputerror'][] = 'old_password';
            	$data['error_string'][] = 'Password Lama salah';
            	$data['status'] = FALSE;
			}
		}

        if($this->input->post('new_password') == ''){
            $data['inputerror'][] = 'new_password';
            $data['error_string'][] = 'New Password harus di isi';
            $data['status'] = FALSE;
		}

		if($this->input->post('retype_password') == ''){
            $data['inputerror'][] = 'retype_password';
            $data['error_string'][] = 'Masukkan New Password Lagi harus di isi';
            $data['status'] = FALSE;
		}

		if($this->input->post('new_password') != $this->input->post('retype_password')){
            $data['inputerror'][] = 'retype_password';
            $data['error_string'][] = 'Password Tidak Cocok';
            $data['status'] = FALSE;
		}

      
        if($data['status'] === FALSE){
            echo json_encode($data);
            exit();
        }
	}

}
