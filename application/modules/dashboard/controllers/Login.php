<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends MY_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model('dashboard/m_global','global');
	}

	public function index(){
		$session 	= $this->session->userdata('logged_in');
		
		$theme 			= $this->global->get_data('m_config',['nama_config' => 'themes'])->result()[0]->DETAIL_CONFIG;
		$data['title']	= $this->config('nama_web');
		$data['a_theme']= 'admin/'.$this->config('admin_themes');
		$data['csrf']   = '<input type="hidden" name="'.$this->security->get_csrf_token_name().'" value="'.$this->security->get_csrf_hash().'">';
		if(!$session){
			$this->load->view('theme/frontend/'.$theme.'/login',$data);
		}else{
			redirect('admin/dashboard', 'refresh');
		}
	}

	public function config($keys){
		$data = $this->global->get_data('m_config',null)->result();
		foreach ($data as $key) {
			$data[$key->NAMA_CONFIG]= $key->DETAIL_CONFIG;
		}
		return $data[$keys];
	}

	public function auth(){
		header('Access-Control-Allow-Origin: *');
		$username 	= $this->input->post('username');
		$f_password = $this->input->post('password');
		$get_user 	= $this->global->get_data('tbl_user',['username' => $username])->result();
		if(!$get_user){
			echo json_encode(['status' => false,'pesan' => 'data tidak di temukan']);
			exit;
		}
		$password 	= $get_user[0]->password;
		$desc_pass 	= $this->crypto('descrypt',$password);
		
		if(!$desc_pass){
			echo json_encode(['status' => false,'pesan' => 'password salah user anda akan di blokir']);
			exit;
		}

		if($desc_pass == $f_password){
			if(!$get_user[0]->status){
				echo json_encode(['status' => false,'pesan' => 'user di blokir silahkan hubungi administrator']);
				exit;
			}
			$data = [
				'id_user'	=> $get_user[0]->id_user,
				'id_level'	=> $get_user[0]->id_level_user,
				'username'	=> $get_user[0]->username
			];
			$this->session->set_userdata('logged_in', $data);
			$this->global->update_tabel('tbl_user', ['last_login' => date('Y-m-d H:i:s')],['id_user' => $data['id_user']]);
		
			echo json_encode(['status' => true,'pesan' => 'berhasil']);
		}else{
			echo json_encode(['status' => false,'pesan' => 'password salah']);
		}
	}
	
	private function crypto($type,$data){
		$this->load->library('encryption');
		if($type == 'encrypt'){
			$result = $this->encryption->encrypt($data);
		}else{
			$result = $this->encryption->decrypt($data);
		}
		return $result;
	}

	public function not_found(){
		$theme 			= $this->global->get_data('m_config',['nama_config' => 'themes'])->result()[0]->DETAIL_CONFIG;
		$data['title']	= $this->config('nama_web');
		$data['theme']	= $this->config('admin_themes');
		$data['a_theme']	= $this->config('admin_themes');
		$this->load->view('theme/backend/'.$data['theme'].'/404',$data);
	}

	public function forbidden(){
		$sessions = $this->session();
		if(!$sessions){
			redirect('admin/dashboard', 'refresh');
			exit;
		}
		$theme 			= $this->global->get_data('m_config',['nama_config' => 'themes'])->result()[0]->DETAIL_CONFIG;
		$data['title']	= $this->config('nama_web');
		$data['theme']	= $this->config('admin_themes');
		$data['a_theme']	= $this->config('admin_themes');
		$this->load->view('theme/backend/'.$data['theme'].'/403',$data);
	}

	public function session(){
		$session	= $this->session->userdata('logged_in');
		return $session;
	}

	public function get_role_acess(){
		$session           = $this->session->userdata('logged_in');
        $url                = current_url();
        $base               = base_url();
        $curr               = str_replace($base, '', $url);
        $explode            = explode('?', $curr);

        $role      = $this->global->get_menu_akses(['tbl_hak_akses.id_level_user' => $session['id_level'],'tbl_menu.link_menu' => $explode[0]])->result()[0];
        return $role;
	}

	public function role_acess(){
        $session           = $this->session->userdata('logged_in');
        $url                = current_url();
        $base               = base_url();
        $curr               = str_replace($base, '', $url);
        $explode            = explode('?', $curr);

        $role      = $this->global->get_menu_akses(['tbl_hak_akses.id_level_user' => $session['id_level'],'tbl_menu.link_menu' => $explode[0]])->result();
        
        if($role){
        	$act 	= $this->input->get('act');

        	#ACTION
        	$list 	= $role[0]->list_data;
        	$add 	= $role[0]->add_button;
        	$edit 	= $role[0]->edit_button;
        	$delete = $role[0]->delete_button;
        	$ex_pdf = $role[0]->export_pdf_button;
        	$verif = $role[0]->verification_button;

        	switch ($act) {
        		case 'list':
        			if($list){
        				return $role;
        			}
        			break;
        		case 'add':
        			if($add){
        				return $role;
        			}
        			break;
        		case 'edit':
        			if($edit){
        				return $role;
        			}
        			break;
        		case 'delete':
        			if($delete){
        				return $role;
        			}
					break;
				case 'pdf':
        			if($ex_pdf){
        				return $role;
        			}
        			break;
        		case '':
        			return $role;
        			break;
        		default:
        			return true;
        			break;
        	}
        }else{
        	return false;
        }
    }


	public function logout(){
		$this->session->unset_userdata('logged_in');
		session_destroy();
		redirect(base_url(), 'refresh');
	}

	public function tes($b){
		echo "hai".$b;
	}

}
