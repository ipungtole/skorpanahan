<?php defined('BASEPATH') OR exit('No direct script access allowed');

Class M_global extends CI_Model {

	public function insert($table,$data){
        $this->db->insert($table, $data);
        return $this->db->insert_id();
    }

	public function update_tabel($tabel,$data,$where){
		$this->db->update($tabel, $data, $where);
        return $this->db->affected_rows();
	}

	public function get_data($tabel,$where){
		$this->db->select('*');
		$this->db->from($tabel);
		if($where){
			$this->db->where($where);
		}
		$query = $this->db->get();
		if($query){
			return $query;
		}else{
			return false;
		}
	}

	public function profile_detail($where){
		$this->db->select('	tbl_user.username,
							tbl_user.last_login,
							tbl_user_detail.email,
							tbl_user_detail.nama_lengkap_user,
							tbl_user_detail.alamat_user,
							tbl_user_detail.tanggal_lahir_user,
							tbl_user_detail.jenis_kelamin_user,
							tbl_user_detail.no_telp_user,
							tbl_user_detail.gambar_user,
							tbl_level_user.nama_level_user,
							tbl_level_user.keterangan_level_user
							');
		$this->db->from('tbl_user');
		$this->db->join('tbl_user_detail','tbl_user_detail.id_user = tbl_user.id_user','left');
		$this->db->join('tbl_level_user','tbl_level_user.id_level_user = tbl_user.id_user','id_level_user');
		$this->db->where($where);
		$query = $this->db->get();
		if($query){
			return $query;
		}else{
			return false;
		}
	}



	public function get_data_order($tabel,$where,$id_order,$order){
		$this->db->select('*');
		$this->db->from($tabel);
		if($where){
			$this->db->where($where);
		}
		if($id_order){
			$this->db->order_by($id_order,$order);
		}
		$query = $this->db->get();
		if($query){
			return $query;
		}else{
			return false;
		}
	}


	public function get_menu_akses($where){
		$this->db->select('	tbl_menu.id_menu,
							tbl_menu.id_parent,
							tbl_menu.nama_menu,
							tbl_menu.link_menu,
							tbl_hak_akses.list_data,
							tbl_hak_akses.aktif,
							tbl_hak_akses.add_button,
							tbl_hak_akses.view_button,
							tbl_hak_akses.edit_button,
							tbl_hak_akses.delete_button,
							tbl_hak_akses.export_pdf_button,
							tbl_hak_akses.verification_button,
							tbl_menu.icon_menu
							');
		$this->db->from('tbl_menu');
		$this->db->join('tbl_hak_akses','tbl_hak_akses.id_menu = tbl_menu.id_menu','left');
		$this->db->where($where);
		$this->db->order_by('tbl_menu.urutan_menu','asc');
		$query = $this->db->get();
		if($query){
			return $query;
		}else{
			return false;
		}
	}









	
	function login($username, $password) {
	   $this -> db -> select('m_user.ID_USER,
	   						  m_user.USERNAME, 
	   						  m_user.PASSWORD, 
	   						  m_user.NAMA, 
	   						  m_user.ID_LEVEL,
	   						  m_cabang.id_cabang,
	   						  m_user.LAST_LOGIN,
	   						  m_user.FAIL,
	   						  m_user.STATUS');
	   $this -> db -> from('m_user');
	   $this -> db -> join('m_cabang','m_user.ID_USER = m_cabang.id_user_fk','left');
	   $this -> db -> where('m_user.USERNAME', $username);
	   $this -> db -> where('m_user.PASSWORD', md5(sha1($password)));
	   $this -> db -> where('m_user.DELETE is null');
	   $this -> db -> limit(1);
	 
	   $query = $this->db->get();
	   if($query -> num_rows() == 1){
		return $query->result();
	   }
	}

	public function delete($where,$tabel){
        $this->db->where($where);
        $this->db->delete($tabel);
        return true;
    }

	function get_produk($limit, $start,$keyword){
		if($keyword){
			$this -> db -> like('nama_produk', $keyword);
		}
        $query = $this->db->get('m_produk', $limit, $start);
        return $query;
    }

    public function lihat_keranjang($where){
    	$this->db->select('temp_order.*,m_produk.nama_produk,m_produk.foto');
    	$this->db->from('temp_order');
    	$this->db->join('m_produk','m_produk.id_produk = temp_order.id_produk','left');
    	$this->db->where($where);
    	$query = $this->db->get();
    	if($query){
			return $query->result();
		}else{
			return false;
		}
    } 

	

	public function remember($cookie) {
	   $this -> db -> select('ID_USER, USERNAME, PASSWORD, NAMA, ID_LEVEL,ID_CABANG,LAST_LOGIN,FAIL,STATUS');
	   $this -> db -> from('m_user');
	   $this -> db -> where('COOKIE', $cookie);
	   $this -> db -> where('DELETE is null');
	   $this -> db -> limit(1);
	 
	   $query = $this->db->get();
	   if($query -> num_rows() == 1){
		return $query->result();
	   }
	}

	public function get_username($tabel,$where){
        $this->db->from($tabel);
        $this->db->where($where);
        $query = $this->db->get();
		if($query){
			return $query->row();
		}else{
			return false;
		}
    }
	
	function detail($sess){
		$this -> db -> select('ID_USER, USERNAME, PASSWORD, NAMA, ID_LEVEL,AVATAR,LAST_LOGIN');
		$this -> db -> from('m_user');
		$this -> db -> where('ID_USER', $sess);
		$query = $this->db->get();
		return $query->result();
	}

	function hak_menu($level){
		$this->db->select(
			'm_hak_akses.*,
			 m_menu.ID_MENU,
			 m_menu.NAMA_MENU,
			 m_menu.MENU_ICON,
			 m_menu.MENU_URL,
			 m_menu.MENU_SINGLE,
			 m_menu.MENU_ORDER
			');
		$this->db->from('m_hak_akses');
		$this->db->join('m_menu', 'm_menu.ID_MENU = m_hak_akses.ID_MENU', 'left');
		$this -> db -> like('m_hak_akses.ID_LEVEL', $level);
		$this -> db -> where('m_hak_akses.STATUS', 1);
		$this -> db -> where('m_menu.MENU_PARENT', 0);
		$this -> db -> order_by('m_menu.MENU_ORDER', 'asc');
		$query = $this->db->get();
		return $query->result();
	}

	function hak_submenu($parent, $level){
		$this->db->select(
			'm_hak_akses.*,
			 m_menu.ID_MENU,
			 m_menu.NAMA_MENU,
			 m_menu.MENU_ICON,
			 m_menu.MENU_SINGLE,
			 m_menu.MENU_URL

			');
		$this->db->from('m_hak_akses');
		$this->db->join('m_menu', 'm_menu.ID_MENU = m_hak_akses.ID_MENU', 'left');
		$this -> db -> like('m_hak_akses.ID_LEVEL', $level);
		$this -> db -> where('m_hak_akses.STATUS', 1);
		$this -> db -> where('m_menu.MENU_PARENT', $parent);
		$query = $this->db->get();
		return $query->result();
	}

	function page_akses($link,$level){
		$this->db->select(
			'm_hak_akses.*,
			 m_menu.ID_MENU,
			 m_menu.MENU_URL
			');
		$this->db->from('m_hak_akses');
		$this->db->join('m_menu', 'm_menu.ID_MENU = m_hak_akses.ID_MENU', 'left');
		$this -> db -> like('m_hak_akses.ID_LEVEL', $level);
		$this -> db -> where('m_menu.MENU_URL', $link);
		$this -> db -> where('m_hak_akses.STATUS', 1);
		$query = $this->db->get();
		$result = $query->result();
		if($result){
			return true;
		}else{
			return false;
		}
	}
	
	function webconfig($config_name){
		$this->db->select('*');
		$this->db->from('m_config');
		$this->db->where('NAMA_CONFIG',$config_name);
		$query = $this->db->get();
		return $query->result();
	}
	
	function sosmed(){
		$this->db->select('*');
		$this->db->from('m_config');
		$this->db->where('TYPE_CONFIG','1');
		$query = $this->db->get();
		return $query->result();
	}
	
	function pesan(){
		$this->db->select('*');
		$this->db->from('m_pesan');
		$this->db->where('STATUS_PESAN != "1"');
		$this->db->order_by('TANGGAL_PESAN','desc');
		$this->db->limit('5');
		$query = $this->db->get();
		if($query){
			return $query->result();
		}else{
			return false;
		}
	}

	function whatsapp(){
		$this->db->select('*');
		$this->db->from('m_config');
		$this->db->where('NAMA_CONFIG','whatsapp');
		$query = $this->db->get();
		if($query){
			return $query->result();
		}else{
			return false;
		}
	}
	
	function pesan_count(){
		$this->db->select('*');
		$this->db->from('m_pesan');
		$this->db->where('STATUS_PESAN != "1"');
		$query = $this->db->get();
		if($query){
			return $query->num_rows();
		}else{
			return false;
		}
	}

	function menu_url($keyword){
		$this -> db -> select('MENU_ICON, MENU_URL,MENU_PARENT');
		$this -> db -> from('m_menu');
		$this -> db -> like('MENU_URL', $keyword);
		$query = $this->db->get();
		return $query->result();
	}

	function security_acess($link,$level){
		$this->db->select(
			'm_hak_akses.*,
			 m_menu.ID_MENU,
			 m_menu.MENU_URL,
			 m_menu.MENU_SINGLE
			');
		$this->db->from('m_hak_akses');
		$this->db->join('m_menu', 'm_menu.ID_MENU = m_hak_akses.ID_MENU', 'left');
		$this -> db -> like('m_hak_akses.ID_LEVEL', $level);
		$this -> db -> like('m_menu.MENU_URL', $link);
		$this -> db -> where('m_hak_akses.STATUS', 1);
		//$this -> db -> limit(1);
		$query = $this->db->get();
		$result = $query->result();
		if($result){
			return true;
		}else{
			return false;
		}
	}

	function judul_menu($menu_url){
		$this->db->select('NAMA_MENU');
		$this->db->from('m_menu');
		$this->db->where('MENU_URL',$menu_url);
		$query = $this->db->get();
		$result = $query->result_array();
		if($result){
			return $result[0];
		}else{
			return false;
		}
	}

	function menu_depan($parent){
		$this->db->select('id_menu,nama_menu,url,menu_parent,modul,order');
		$this->db->from('m_menu_depan');
		if($parent){
			$this->db->where('menu_parent',$parent);
		}else{
			$this->db->where('menu_parent','0');
		}
		$this->db->order_by('order','asc');
		$query = $this->db->get();
		$result = $query->result();
		if($result){
			return $result;
		}else{
			return false;
		}
	}

	function halaman($seo){
		$this->db->select('url,modul');
		$this->db->from('m_menu_depan');
		$this->db->where('url',$seo);
		$query = $this->db->get();
		$result = $query->result();
		if($result){
			return $result;
		}else{
			return false;
		}
	}

	public function update($where, $data){
        $this->db->update('m_user', $data, $where);
        return $this->db->affected_rows();
    }

    public function visitor($where){
    	$this->db->select('*');
		$this->db->from('t_visitor');
		if($where){
			$this->db->where($where);
		}
    	$query = $this->db->get();
    	$result = $query->result();
		if($result){
			 return $query;
		}else{
			return 0;
		}
    }

    public function top_country(){
    	$this->db->select('count(visitor_country) as jumlah_negara,visitor_country');
		$this->db->from('t_visitor');
		$this->db->group_by('visitor_country');
		$this->db->order_by('count(visitor_country)','desc');
		$this->db->limit('5');
		$query = $this->db->get();
		$result = $query->result();
		if($result){
			return $result;
		}else{
			return false;
		}
    }

    public function top_os(){
    	$this->db->select('count(visitor_os) as jumlah_os,visitor_os');
		$this->db->from('t_visitor');
		$this->db->where('visitor_os !=','UNKNOWN OS PLATFORM');
		$this->db->where('visitor_os !=','UNKNOWN PLATFORM');
		$this->db->group_by('visitor_os');
		$this->db->order_by('count(visitor_os)','desc');
		$this->db->limit('5');
		$query = $this->db->get();
		$result = $query->result();
		if($result){
			return $result;
		}else{
			return false;
		}
    }

    public function sum_top_os(){
    	$this->db->select('sum(jumlah_os) as jml');
		$this->db->from('v_top_os');
		$query = $this->db->get();
		$result = $query->result();
		if($result){
			return $result;
		}else{
			return false;
		}
	}
	
	public function get_id_menu_parent($param){
		$this->db->select('ID_MENU,MENU_SINGLE');
		$this->db->from('m_menu');
		$this->db->where('MENU_URL',$param);
		$query = $this->db->get();
		$result = $query->result_array();
		if($result){
			return $result[0];
		}else{
			return false;
		}
	}

	public function get_submenu2($level,$id){
		$this->db->select(
			'm_hak_akses.*,
			 m_menu.ID_MENU,
			 m_menu.NAMA_MENU,
			 m_menu.MENU_ICON,
			 m_menu.MENU_IMAGES,
			 m_menu.MENU_URL

			');
		$this->db->from('m_hak_akses');
		$this->db->join('m_menu', 'm_menu.ID_MENU = m_hak_akses.ID_MENU', 'left');
		$this -> db -> like('m_hak_akses.ID_LEVEL', $level);
		$this -> db -> where('m_hak_akses.STATUS', 1);
		$this -> db -> where('m_menu.MENU_PARENT', $id);
		$query = $this->db->get();
		$result = $query->result();
		if($result){
			return $result;
		}else{
			return false;
		}
	}

	public function get_kode_otomatis($id){
		$this->db->select('*');
		$this->db->from('m_kode');
		$this->db->where('id_kode',$id);
		$query = $this->db->get();
		$result = $query->result_array();
		if($result){
			return $result[0];
		}else{
			return false;
		}
	}


	public function get_kota($id,$param){
		$this->db->select('*');
		$this->db->from('m_kota');
		if($param == null){
			$this->db->where('id_provinsi',$id);
		}
		$query = $this->db->get();
		$result = $query->result();
		if($result){
			return $result;
		}else{
			return false;
		}
	}

	public function get_provinsi(){
		$this->db->select('*');
		$this->db->from('m_propinsi');
		$query = $this->db->get();
		$result = $query->result();
		if($result){
			return $result;
		}else{
			return false;
		}
	}
	
	public function get_layanan(){
		$this->db->select('*');
		$this->db->from('m_layanan');
		$this->db->where('delete',0);
		$query = $this->db->get();
		$result = $query->result();
		if($result){
			return $result;
		}else{
			return false;
		}
	}

	public function get_berat(){
		$this->db->select('*');
		$this->db->from('m_harga');
		$query = $this->db->get();
		$result = $query->result();
		if($result){
			return $result;
		}else{
			return false;
		}
	}

	public function get_paket(){
		$this->db->select('id_paket,nama_paket');
		$this->db->from('m_paket');
		$query = $this->db->get();
		$result = $query->result();
		if($result){
			return $result;
		}else{
			return false;
		}
	}

	public function get_cabang($id){
		$this->db->select('m_cabang.*,destinasi.kota as nama_kota,prov.nama_provinsi');
		$this->db->from('m_cabang');
		$this->db->join('m_kota destinasi','destinasi.id_kota = m_cabang.kota','left');
        $this->db->join('m_propinsi prov','prov.id_provinsi = destinasi.id_provinsi','left');
		$this->db->where('m_cabang.id_cabang',$id);
		$query = $this->db->get();
		$result = $query->result();
		if($result){
			return $result;
		}else{
			return false;
		}
	}

}