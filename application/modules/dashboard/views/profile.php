 <section class="content">
        <div class="container-fluid">
            <div class="row clearfix">
                <div class="col-xs-12 col-sm-3">
                    <div class="card profile-card">
                        <div class="profile-header">&nbsp;</div>
                        <div class="profile-body">
                            <div class="image-area">
                                <img src="../../images/user-lg.jpg" alt="Profile Image" />
                            </div>
                            <div class="content-area">
                                <h3 id="nama_bulat">Pungki Dwi P.</h3>
                                <p id="ket_level">Web Software Developer</p>
                                <p id="level">Administrator</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-9">
                    <div class="card">
                        <div class="body">
                            <div>
                                <ul class="nav nav-tabs" role="tablist">
                                    <!-- <li role="presentation" ><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Home</a></li> -->
                                    <li role="presentation" class="active"><a href="#profile_settings" aria-controls="settings" role="tab" data-toggle="tab">Profile Settings</a></li>
                                    <li role="presentation"><a href="#change_password_settings" aria-controls="settings" role="tab" data-toggle="tab">Change Password</a></li>
                                </ul>

                                <div class="tab-content">
                                   
                                    <div role="tabpanel" class="tab-pane fade in active" id="profile_settings">
                                        <form id="form_profile" class="form-horizontal">
                                            <?= $csrf ?>
                                            <div class="form-group">
                                                <label for="NameSurname" class="col-sm-2 control-label">Name Surname</label>
                                                <div class="col-sm-10">
                                                    <div class="form-line">
                                                        <input type="text" class="form-control" id="NameSurname" name="NameSurname" placeholder="Name Surname"  required>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="Email" class="col-sm-2 control-label">Email</label>
                                                <div class="col-sm-10">
                                                    <div class="form-line">
                                                        <input type="email" class="form-control" id="Email" name="email" placeholder="Email" required>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="NameSurname" class="col-sm-2 control-label">Address</label>
                                                <div class="col-sm-10">
                                                    <div class="form-line">
                                                        <input type="text" class="form-control" id="alamat" name="alamat" placeholder="Address" >
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="NameSurname" class="col-sm-2 control-label">Phone</label>
                                                <div class="col-sm-10">
                                                    <div class="form-line">
                                                        <input type="text" class="form-control" id="no_telp_user" name="no_telp_user" placeholder="Phone Number" >
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="NameSurname" class="col-sm-2 control-label">Avatar</label>
                                                <div class="col-sm-10">
                                                    <div class="form-line">
                                                        <input name="gambar" id="photo" onchange="validasiFile();" type="file">
                                <div id="thumbnail">
                                    <img id="blah" alt="Gambar Thumbnail" >
                                  </div>
                                <span class="help-block"></span>
							<input type="hidden" name="id_gallery" id="id_gallery">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-offset-2 col-sm-10">
                                                    <button type="submit" id="btnSaveprofile" onclick="simpan_profile()" class="btn btn-danger">SUBMIT</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    <div role="tabpanel" class="tab-pane fade in" id="change_password_settings">
                                        <form id="form" class="form-horizontal">
                                            <?= $csrf ?>
                                            <div class="form-group">
                                                <label for="OldPassword" class="col-sm-3 control-label">Old Password</label>
                                                <div class="col-sm-9 parents">
                                                    <div class="form-line">
                                                        <input type="password" class="form-control" id="old_password" name="old_password" placeholder="Old Password" required>
                                                        <label id="name-error" class="help-block error" for="old_password"></label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="NewPassword" class="col-sm-3 control-label">New Password</label>
                                                <div class="col-sm-9">
                                                    <div class="form-line">
                                                        <input type="password" class="form-control" id="new_password" name="new_password" placeholder="New Password" required>
                                                        <label id="name-error" class="help-block error" for="new_password"></label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="NewPasswordConfirm" class="col-sm-3 control-label">New Password (Confirm)</label>
                                                <div class="col-sm-9">
                                                    <div class="form-line">
                                                        <input type="password" class="form-control" id="retype_password" name="retype_password" placeholder="New Password (Confirm)" required>
                                                        <label id="name-error" class="help-block error" for="retype_password"></label>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="col-sm-offset-3 col-sm-9">
                                                    <button type="submit" id="btnSave" onclick="simpan()" class="btn btn-danger">SUBMIT</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <script>
    var base_url = '<?php echo base_url(); ?>';
    detail();
$('#thumbnail').hide();
    function validasiFile(){
      var inputFile = document.getElementById('photo');
      var pathFile = inputFile.value;
      // var ekstensiOk = /(\.jpg|\.jpeg|\.png|\.gif)$/i;
      var ekstensiOk = /(\.jpg|\.jpeg|\.png|\.gif|\.webp|\.jp2|\.svg)$/i;

      if(!ekstensiOk.exec(pathFile)){
          // alert('Silakan upload file yang memiliki ekstensi .jpeg/.jpg/.png/.gif');
          
          $.confirm({
                        icon: 'fa fa-smile-o',
                        title: 'No....',
                        content: "Silakan upload file yang memiliki ekstensi .jpeg/.jpg/.png/.gif!",
                        theme: 'modern',
                        closeIcon: true,
                        type: 'red',
                        animation: 'rotate',
                        buttons: {
                            Ok: function() {
                                
                            }
                        }
                    });
          inputFile.value = '';
          return false;
      }else{
          //Pratinjau gambar
          $('#thumbnail').show();
          if (inputFile.files && inputFile.files[0]) {
              var reader = new FileReader();
              reader.onload = function(e) {
                  document.getElementById('thumbnail').innerHTML = '<br><img width="50%" src="'+e.target.result+'"/>';
              };
              reader.readAsDataURL(inputFile.files[0]);
        $("#gambar").hide();
          }
      }
      // alert('textStatus');
    }

    function simpan() {
        $('#btnSave').text('saving...'); //change button text
        $('#btnSave').attr('disabled', true); //set button disable 
        
        var formData = new FormData($('#form')[0]);
        // ajax adding data to database
        $.ajax({
            url: '<?php echo site_url($url . '?act=edit') ?>',
            type: "POST",
            data: formData,
            contentType: false,
            processData: false,
            dataType: "JSON",
            success: function(data) {

                if (data.status) { //if success close modal and reload ajax table
                    $.confirm({
                        icon: 'fa fa-smile-o',
                        title: 'Yeaah....',
                        content: data.pesan,
                        theme: 'modern',
                        closeIcon: true,
                        type: 'green',
                        animation: 'rotate',
                        buttons: {
                            Ok: function() {
                                
                            }
                        }
                    });
                } else {
                    for (var i = 0; i < data.inputerror.length; i++) {
                        if (data.inputerror[i] == 'cabang' || data.inputerror[i] == 'nama_kapal') {
                            $('[name="' + data.inputerror[i] + '"]').parent().addClass('error'); //select parent twice to select div form-group class and add has-error class
                            $('[name="' + data.inputerror[i] + '"]').next().next().text(data.error_string[i]); //select span help-block class set text error string
                        } else {
                            // $('[name="'+data.inputerror[i]+'"]').parent().addClass('error focus'); //select parent twice to select div form-group class and add has-error class
                            $('[name="' + data.inputerror[i] + '"]').parents('.form-line').addClass('error');
                            $('[name="' + data.inputerror[i] + '"]').next().text(data.error_string[i]);

                        }
                    }



                }
                    $('#btnSave').html('<i class="material-icons">save</i>  Simpan'); //change button text
                    $('#btnSave').attr('disabled', false); //set button enable 
            },
            error: function(jqXHR, textStatus, errorThrown) {

            }
        });
    }

    function simpan_profile() {
        $('#btnSaveprofile').text('saving...'); //change button text
        $('#btnSaveprofile').attr('disabled', true); //set button disable 
        
        var formData = new FormData($('#form_profile')[0]);
        // ajax adding data to database
        $.ajax({
            url: '<?php echo site_url($url . '?act=edit_profile') ?>',
            type: "POST",
            data: formData,
            contentType: false,
            processData: false,
            dataType: "JSON",
            success: function(data) {

                if (data.status) { //if success close modal and reload ajax table
                    $.confirm({
                        icon: 'fa fa-smile-o',
                        title: 'Yeaah....',
                        content: data.pesan,
                        theme: 'modern',
                        closeIcon: true,
                        type: 'green',
                        animation: 'rotate',
                        buttons: {
                            Ok: function() {
                                detail();
                            }
                        }
                    });
                } else {
                    for (var i = 0; i < data.inputerror.length; i++) {
                        if (data.inputerror[i] == 'cabang' || data.inputerror[i] == 'nama_kapal') {
                            $('[name="' + data.inputerror[i] + '"]').parent().addClass('error'); //select parent twice to select div form-group class and add has-error class
                            $('[name="' + data.inputerror[i] + '"]').next().next().text(data.error_string[i]); //select span help-block class set text error string
                        } else {
                            // $('[name="'+data.inputerror[i]+'"]').parent().addClass('error focus'); //select parent twice to select div form-group class and add has-error class
                            $('[name="' + data.inputerror[i] + '"]').parents('.form-line').addClass('error');
                            $('[name="' + data.inputerror[i] + '"]').next().text(data.error_string[i]);

                        }
                    }



                }
                    $('#btnSaveprofile').html('<i class="material-icons">save</i>  Simpan'); //change button text
                    $('#btnSaveprofile').attr('disabled', false); //set button enable 
            },
            error: function(jqXHR, textStatus, errorThrown) {

            }
        });
    }

    function detail(){
        $.ajax({
            url: "<?php echo site_url($url . '?act=detail') ?>",
            type: 'GET',
            dataType: 'json',
            success: function(data) {
                $("#nama_bulat").html(data.nama_lengkap_user);
                $("#ket_level").html(data.keterangan_level_user);
                $("#level").html(data.nama_level_user);
                $("[name='NameSurname']").val(data.nama_lengkap_user);
                $("[name='email']").val(data.email);
                $("[name='alamat']").val(data.alamat_user);
                $("[name='no_telp_user']").val(data.no_telp_user);
                if(data.gambar_user){
                    $('.image-area').html('<img src="'+base_url+'images/'+data.gambar_user+'" width="128">');
                    $('.image').html('<img src="'+base_url+'images/'+data.gambar_user+'" width="55">');
                    
                }else{
                    $('.image-area').html('<img src="'+base_url+'images/avatar_blank.png" width="128">');
                }

            }
        });
    }
    </script>