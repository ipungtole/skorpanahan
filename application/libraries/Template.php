<?php if (! defined ('BASEPATH')) exit ('No direct script access allowed');

class Template extends MY_Controller{
    protected $_ci;
     
    function __construct(){
        $this->_ci = &get_instance();
    }
     
    function dashboards($content, $data = NULL){
        $session 	        = $this->session->userdata('logged_in');
        if(!$session){
            redirect('login', 'refresh');
            exit;
		}
        // #GREETINGS
        // $b = time();
        // $hour = date("G",$b);

        // if ($hour>=0 && $hour<=11){
        //     $hallo = "Selamat Pagi ";
        // }elseif ($hour >=12 && $hour<=14){
        //     $hallo = "Selamat Siang ";
        // }elseif ($hour >=15 && $hour<=17){
        //     $hallo = "Selamat Sore ";
        // }elseif ($hour >=17 && $hour<=18){
        //     $hallo = "Selamat Petang ";
        // }elseif ($hour >=19 && $hour<=23){
        //     $hallo = "Selamat Malam ";
        // }
        
        #USER SESSION
        $session 	        = $this->session->userdata('logged_in');
        $url                = current_url();
        $base               = base_url();
        $curr               = str_replace($base, '', $url);
        $explode            = explode('?', $curr);

        #DATA TITLE
        $data['a_theme']    = $this->config('admin_themes');
        $data['title']	    = $this->config('nama_web');
        $data['app_name']	= $this->config('app_name');
        $data['version']    = $this->config('app_version');
        $data['theme']	    = $this->config('themes');
		$data['user_info']	= $this->global->get_data('tbl_user_detail',['id_user' => $session['id_user']])->result()[0];
		
        #MENU AND ROLE
		$menu	            = $this->global->get_menu_akses(['tbl_hak_akses.id_level_user' => $session['id_level'],'tbl_hak_akses.aktif' => '1'])->result_array();
		$data['menu2']	    = $this->global->get_menu_akses(['tbl_hak_akses.id_level_user' => $session['id_level'],'tbl_hak_akses.aktif' => '1'])->result_array();
        $data['akses']      = $this->global->get_menu_akses(['tbl_hak_akses.id_level_user' => $session['id_level'],'tbl_menu.link_menu' => $explode[0]])->result_array();
        $data['csrf']       = '<input type="hidden" name="'.$this->_ci->security->get_csrf_token_name().'" value="'.$this->_ci->security->get_csrf_hash().'">';
        $data['csrf_hash']  = $this->_ci->security->get_csrf_hash();
        
        #LOADING THEME
        $data['control']    = $this->uri->segment(1);      
        $data['url']        = $explode[0];      
        $data['headernya']  = $this->_ci->load->view('theme/backend/'.$data['a_theme'].'/headers', $data, TRUE);
        $data['sidebar'] 	= $this->_ci->load->view('theme/backend/'.$data['a_theme'].'/menu', $data, TRUE);
        $data['forbidden']  = $this->_ci->load->view('theme/backend/'.$data['a_theme'].'/403', $data, TRUE);
        $data['contentnya'] = $this->_ci->load->view($content, $data, TRUE);
        $data['footernya']  = $this->_ci->load->view('theme/backend/'.$data['a_theme'].'/footer', $data, TRUE);
         
        $this->_ci->load->view('theme/Template_index', $data);
    }

    function frontend($content, $data = NULL){
        $data['title']	    = $this->config('nama_web');
        $data['logo']	    = $this->config('logo');
        $data['email']	    = $this->config('email');
        $data['alamat']	    = $this->config('alamat');
        $data['phone']	    = $this->config('phone');
        $data['themes']     = $this->config('themes');
        $data['favicon']    = $this->config('favicon');
        // var_dump($data['themes']);
        $lang 	            = $this->session->userdata('bahasa');
        $default_lang 	    = $this->global->get_data('p_language',['default' => '1'],null)->row()->id_language;
        $language           = ($lang)?$lang['id_language']:$default_lang;
        // var_dump($lang);exit;
        $data['menu']       = $this->front->menu(['parent' => null,'m_menufront.language' => $language]);
        $data['headernya']  = $this->_ci->load->view('theme/frontend/'.$data['themes'].'/header', $data, TRUE);
        $data['sidebar'] 	= null;
        $data['forbidden']  = $this->_ci->load->view('theme/frontend/'.$data['themes'].'/404', $data, TRUE);
        $data['contentnya'] = $this->_ci->load->view($content, $data, TRUE);
        $data['footernya']  = $this->_ci->load->view('theme/frontend/'.$data['themes'].'/footer', $data, TRUE);

        $this->_ci->load->view('theme/Template_free_acess', $data);
    }

    function all_acess($content, $data = NULL){
        
        #USER SESSION
        $session 	        = $this->session->userdata('logged_in');
        if(!$session){
            redirect('admin/dashboard', 'refresh');
            exit;
        }
        
        $url                = current_url();
        $base               = base_url();
        $curr               = str_replace($base, '', $url);
        $explode            = explode('?', $curr);

        #DATA TITLE
        $data['a_theme']    = $this->config('admin_themes');
        $data['title']	    = $this->config('nama_web');
        $data['app_name']	= $this->config('app_name');
        $data['version']    = $this->config('app_version');
        $data['theme']	    = $this->config('themes');
		$data['user_info']	= $this->global->get_data('tbl_user_detail',['id_user' => $session['id_user']])->result()[0];
		
        #MENU AND ROLE
		$menu	            = $this->global->get_menu_akses(['tbl_hak_akses.id_level_user' => $session['id_level'],'tbl_hak_akses.aktif' => '1'])->result_array();
		$data['menu2']	    = $this->global->get_menu_akses(['tbl_hak_akses.id_level_user' => $session['id_level'],'tbl_hak_akses.aktif' => '1'])->result_array();
        $data['akses']      = $this->global->get_menu_akses(['tbl_hak_akses.id_level_user' => $session['id_level'],'tbl_menu.link_menu' => $explode[0]])->result_array();
        $data['csrf']       = '<input type="hidden" name="'.$this->_ci->security->get_csrf_token_name().'" value="'.$this->_ci->security->get_csrf_hash().'">';
        $data['csrf_hash']  = $this->_ci->security->get_csrf_hash();
        
        // var_dump($menu);exit;
        #LOADING THEME
        // $data['menus']      = $this->prepareMenu($menu);
        $data['control']    = $this->uri->segment(1);      
        $data['url']        = $explode[0];      
        $data['headernya']  = $this->_ci->load->view('theme/backend/'.$data['a_theme'].'/headers', $data, TRUE);
        $data['sidebar'] 	= $this->_ci->load->view('theme/backend/'.$data['a_theme'].'/menu', $data, TRUE);
        $data['forbidden']  = $this->_ci->load->view('theme/backend/'.$data['a_theme'].'/403', $data, TRUE);
        $data['contentnya'] = $this->_ci->load->view($content, $data, TRUE);
        $data['footernya']  = $this->_ci->load->view('theme/backend/'.$data['a_theme'].'/footer', $data, TRUE);
         
        $this->_ci->load->view('theme/Template_free_acess', $data);
    }

    private function config($keys){
		$data = $this->global->get_data('m_config',null)->result();
		foreach ($data as $key) {
			$data[$key->NAMA_CONFIG]= $key->DETAIL_CONFIG;
		}
		return $data[$keys];
    }

    public function prepareMenu($array){
        $return = array();
        //1
        krsort($array);
        foreach ($array as $k => &$item){
            // var_dump($item['id_parent']);
            if (is_numeric($item['id_parent'])){
            $parent = $item['id_parent'];
            if (empty($array[$parent]['Childs']))
            {
                $array[$parent]['Childs'] = array();
            }
            //2
            array_unshift($array[$parent]['Childs'],$item);
            unset($array[$k]);
            }
        }
        //3
        ksort($array);
        return $array;
    }


    public function prepareMenu2($array){
        $return = array();
        //1
        krsort($array);
        foreach ($array as $k => &$item){
            if (is_numeric($item['id_parent']))
            {
            $parent = $item['id_parent'];
            if (empty($array[$parent]['Childs']))
            {
                $array[$parent]['Childs'] = array();
            }
            //2
            array_unshift($array[$parent]['Childs'],$item);
            unset($array[$k]);
            }
        }
        //3
        ksort($array);
        return $array;
    }
    
    public function buildMenu($array){
    echo '<ul>';
        foreach ($array as $item){
            echo '<li>';
            echo $item['nama_menu'];
            if (!empty($item['Childs']))
            {
            $this->buildMenu($item['Childs']);
            }
            echo '</li>';
        }
    echo '</ul>';
    }
    
    
}