<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
* Name:  DOMPDF
* 
* Author: Jd Fiscus
* 	 	  jdfiscus@gmail.com
*         @iamfiscus
*          
*
* Origin API Class: http://code.google.com/p/dompdf/
* 
* Location: http://github.com/iamfiscus/Codeigniter-DOMPDF/
*          
* Created:  06.22.2010 
* 
* Description:  This is a Codeigniter library which allows you to convert HTML to PDF with the DOMPDF library
* 
*/

use Dompdf\Dompdf;

class Dompdf_gen {
		
	public function __construct() {
		
		// require_once APPPATH.'libraries/dompdf_/dompdf_config.inc.php';
		require_once APPPATH.'libraries/dompdf-0.8.3/autoload.inc.php';
		// require_once APPPATH . '/libraries/mpdf/autoload.php';
		// require_once APPPATH.'libraries/dompdf-0.8.2/src/Autoloader.php';

		$pdf = new DOMPDF();
		// $pdf = new \Mpdf\Mpdf(['tempDir' => APPPATH . '../upload']);
		
		$CI =& get_instance();
		$CI->dompdf = $pdf;
		
	}
	
}