<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Menkes extends MY_Controller {
    function __construct(){
		parent::__construct();
	
	}


	#COLONG TEKO KEMENKES
	public function tes(){
        $url    = "http://202.70.136.86/ws/fo/rumah_sakit?kode_propinsi=35prop&kode_kabkota=3578";
		$ch = curl_init( $url );
        curl_setopt($ch, CURLOPT_URL,$url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);

        curl_setopt($ch, CURLOPT_TIMEOUT_MS, 2000);
        $server_output = curl_exec($ch);
        $curl_errno = curl_errno($ch);
    	$curl_error = curl_error($ch);

        curl_close ($ch);
        // var_dump($server_output);
        if ($curl_errno > 0) {
	        return (object)['status' => false,'pesan' => 'Koneksi Ke Server Aplikasi LSJ Express STT Gagal, coba lagi nanti'];
	    } else {
            return $server_output;
	       
        }
        

    }

    public function detail($id){
        $url    = "http://202.70.136.86/ws/fo/tempat_tidur?kode_rs=".$id;
		$ch = curl_init( $url );
        curl_setopt($ch, CURLOPT_URL,$url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);

        curl_setopt($ch, CURLOPT_TIMEOUT_MS, 2000);
        $server_output = curl_exec($ch);
        $curl_errno = curl_errno($ch);
    	$curl_error = curl_error($ch);

        curl_close ($ch);
        // var_dump($server_output);
        if ($curl_errno > 0) {
	        return (object)['status' => false,'pesan' => 'Koneksi Ke Server Aplikasi LSJ Express STT Gagal, coba lagi nanti'];
	    } else {
            // $json_dec   = json_decode($server_output);
            return $server_output;
	        // if($json_dec->status){

	        // 	return $json_dec;
	        // }else{
	        // 	return (object)['status' => false,'pesan' => 'data stt tidak di temukan'] ;
	        // }
        }
    }

    public function gabung(){
        $master = $this->tes();
        $decode = json_decode($master);
        if($decode->data){
            $data = [];
            foreach ($decode->data as $mas) {
                $detail = $this->detail($mas->kode_rs);
                $decode = json_decode($detail);
                $data[] = [
                    'kode_rs'       => $mas->kode_rs,
                    'RUMAH_SAKIT'   => $mas->RUMAH_SAKIT,
                    'ALAMAT'        => $mas->ALAMAT,
                    'telp'          => $mas->telp,
                    'detail'        => $decode->data
                ];
            }
            echo json_encode($data);
        }
    }
}