-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3307
-- Generation Time: Jul 05, 2021 at 11:21 PM
-- Server version: 10.5.4-MariaDB
-- PHP Version: 7.4.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `koni`
--

-- --------------------------------------------------------

--
-- Table structure for table `m_config`
--

DROP TABLE IF EXISTS `m_config`;
CREATE TABLE IF NOT EXISTS `m_config` (
  `ID_CONFIG` int(11) NOT NULL,
  `NAMA_CONFIG` varchar(100) NOT NULL,
  `DETAIL_CONFIG` varchar(150) NOT NULL,
  `ICON_CONFIG` varchar(255) DEFAULT NULL,
  `TYPE_CONFIG` varchar(255) DEFAULT NULL,
  `STATUS` varchar(255) DEFAULT NULL,
  `DELETE` int(255) DEFAULT NULL,
  PRIMARY KEY (`ID_CONFIG`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `m_config`
--

INSERT INTO `m_config` (`ID_CONFIG`, `NAMA_CONFIG`, `DETAIL_CONFIG`, `ICON_CONFIG`, `TYPE_CONFIG`, `STATUS`, `DELETE`) VALUES
(1, 'nama_web', 'Online Registration', NULL, NULL, NULL, NULL),
(2, 'tagline', 'koni', NULL, NULL, NULL, NULL),
(3, 'deskripsi', 'pendaftaran', NULL, NULL, NULL, NULL),
(4, 'favicon', 'favicon.png', NULL, NULL, NULL, NULL),
(5, 'logo', 'logo.png', NULL, NULL, NULL, NULL),
(6, 'phone', '+33768514468, +6281296072352', NULL, NULL, NULL, NULL),
(7, 'author', 'simpel solusindo', NULL, NULL, NULL, 1),
(8, 'themes', 'villa', NULL, NULL, NULL, NULL),
(9, 'admin_themes', 'material', NULL, NULL, NULL, NULL),
(10, 'app_name', 'Koni Online Registration', NULL, NULL, NULL, 1),
(11, 'app_version', '1.0.0', NULL, NULL, NULL, 1),
(12, 'instansi', 'Koni', NULL, NULL, NULL, 1),
(13, 'devisi', 'ONLINE', NULL, NULL, NULL, 1),
(14, 'email', 'koni@hotmail.com', NULL, NULL, NULL, NULL),
(15, 'alamat', 'Koni Surabaya', NULL, NULL, NULL, NULL),
(16, 'price_night', '180', NULL, NULL, NULL, 1),
(17, 'price_week', '1002', NULL, NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `m_event`
--

DROP TABLE IF EXISTS `m_event`;
CREATE TABLE IF NOT EXISTS `m_event` (
  `id_event` int(255) NOT NULL AUTO_INCREMENT,
  `nama_event` varchar(255) DEFAULT NULL,
  `date_event` datetime DEFAULT NULL,
  `image_event` varchar(255) DEFAULT NULL,
  `lokasi_event` varchar(255) DEFAULT NULL,
  `lap` int(11) DEFAULT NULL,
  `description` text DEFAULT NULL,
  `start_date_registration` datetime DEFAULT NULL,
  `finish_date_registration` datetime DEFAULT NULL,
  `status_event` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_event`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_event`
--

INSERT INTO `m_event` (`id_event`, `nama_event`, `date_event`, `image_event`, `lokasi_event`, `lap`, `description`, `start_date_registration`, `finish_date_registration`, `status_event`, `deleted`) VALUES
(1, 'tornado', '2021-06-10 00:00:00', '1624888449488.png', 'gor', 5, 'oke gais', '2021-06-29 20:53:55', '2021-06-30 20:54:01', 1, NULL),
(2, 'yy', '2021-06-29 20:35:29', '1625010879270.png', '', 8, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `m_gallery`
--

DROP TABLE IF EXISTS `m_gallery`;
CREATE TABLE IF NOT EXISTS `m_gallery` (
  `id_gallery` int(11) NOT NULL,
  `nama_gambar` varchar(100) DEFAULT NULL,
  `caption_gambar` varchar(150) DEFAULT NULL,
  `type` varchar(50) DEFAULT NULL,
  `home` int(1) DEFAULT NULL,
  `status` int(1) DEFAULT NULL,
  `deleted` int(1) DEFAULT NULL,
  PRIMARY KEY (`id_gallery`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `m_gallery`
--

INSERT INTO `m_gallery` (`id_gallery`, `nama_gambar`, `caption_gambar`, `type`, `home`, `status`, `deleted`) VALUES
(1, 'slide.jpg', 'Find your calmness in our place', 'slider', NULL, 1, NULL),
(2, 'lobby.jpeg', 'Lobby Depan', 'gallery', NULL, 1, 1),
(3, 'teras.jpeg', 'Teras Samping', 'gallery', NULL, 1, 1),
(4, 'lobby.jpeg', 'Lobby Depan', 'gallery', NULL, 1, 1),
(5, 'teras.jpeg', 'Teras Samping', 'gallery', NULL, 1, 1),
(6, '1601741027960.jpeg', 'Lobby samping', 'gallery', NULL, 1, 1),
(7, '1601740933325.jpeg', 'Lobby Depan', 'gallery', NULL, 1, 1),
(8, '1601740541852.jpg', 'lokon', 'gallery', NULL, 1, 1),
(9, '1602059043821.jpeg', 'wastafel', 'gallery', NULL, 1, 1),
(10, '1602326171639.jpg', 'drone 1', 'gallery', NULL, 1, 1),
(11, '1602327067963.jpg', 'drone 2', 'gallery', NULL, 1, 1),
(12, '1602327381599.jpg', 'pool view 1', 'gallery', NULL, 1, 1),
(13, '1602327603619.jpg', 'pool view 3', 'gallery', NULL, 1, 1),
(14, '1602328042538.jpg', 'poolview 3', 'gallery', NULL, 1, 1),
(15, '1602328149931.jpg', 'horse view', 'gallery', NULL, 1, 1),
(16, '1602328170961.jpg', 'gazebo', 'gallery', NULL, 1, 1),
(17, '1602346071669.jpg', 'dining room 1', 'gallery', NULL, 1, 1),
(18, '1602346111021.jpg', 'dingroom2', 'gallery', NULL, 1, 1),
(19, '1602346130487.jpg', 'dingroom3', 'gallery', NULL, 1, 1),
(20, '1602346156581.jpg', 'livingroom1', 'gallery', NULL, 1, 1),
(21, '1602346236180.jpg', 'pool view1', 'gallery', NULL, 1, 1),
(22, '1605176907636.jpg', 'master bedroom', 'gallery', NULL, 1, 1),
(23, '1605178307027.jpg', 'master bedroom2', 'gallery', NULL, 1, 1),
(24, '1605178370930.jpg', 'master bedroom3', 'gallery', NULL, 1, 1),
(25, '1606045658624.jpg', '23', 'gallery', NULL, 1, 1),
(26, '1606046347321.jpg', '487', 'gallery', NULL, 1, NULL),
(27, '1606046493257.jpg', '23', 'gallery', NULL, 1, NULL),
(28, '1606046610001.jpg', '22', 'gallery', NULL, 1, NULL),
(29, '1606046945719.jpg', '21', 'gallery', NULL, 1, NULL),
(30, '1606047060002.jpg', '20', 'gallery', NULL, 1, NULL),
(31, '1606047270365.jpg', '20a', 'gallery', NULL, 1, NULL),
(32, '1606047293843.jpg', '19', 'gallery', NULL, 1, NULL),
(33, '1606047318052.jpg', '18', 'gallery', NULL, 1, NULL),
(34, '1606047470661.jpg', '18', 'gallery', NULL, 1, NULL),
(35, '1606047503349.jpg', '17', 'gallery', NULL, 1, NULL),
(36, '1606047524974.jpg', '16', 'gallery', NULL, 1, NULL),
(37, '1606047803923.jpg', '15', 'gallery', NULL, 1, NULL),
(38, '1606048065669.jpg', '14', 'gallery', 0, 1, NULL),
(39, '1606048095663.jpg', '13', 'gallery', NULL, 1, NULL),
(40, '1606048155379.jpg', '12', 'gallery', 0, 1, NULL),
(41, '1606048187325.jpg', '11', 'gallery', NULL, 1, NULL),
(42, '1606048438495.jpg', '10', 'gallery', NULL, 1, NULL),
(43, '1606048525010.jpg', '9', 'gallery', 0, 1, NULL),
(44, '1606048617422.jpg', '8', 'gallery', NULL, 1, NULL),
(45, '1606048658459.jpg', '7', 'gallery', 1, 1, NULL),
(46, '1606048683284.jpg', '6', 'gallery', NULL, 1, NULL),
(47, '1606049043084.jpg', '5', 'gallery', NULL, 1, NULL),
(48, '1606049108750.jpg', '4', 'gallery', 1, 1, NULL),
(49, '1606049206603.jpg', '4', 'gallery', NULL, 1, NULL),
(50, '1606049243398.jpg', '3', 'gallery', NULL, 1, NULL),
(51, '1606049386423.jpg', '2', 'gallery', 0, 1, NULL),
(52, '1606049425911.jpg', '1', 'gallery', 1, 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `m_inbox`
--

DROP TABLE IF EXISTS `m_inbox`;
CREATE TABLE IF NOT EXISTS `m_inbox` (
  `id_inbox` int(255) NOT NULL,
  `nama` varchar(150) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `telp` varchar(50) DEFAULT NULL,
  `subject` varchar(100) DEFAULT NULL,
  `pesan` text DEFAULT NULL,
  `deleted` int(1) DEFAULT NULL,
  PRIMARY KEY (`id_inbox`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_inbox`
--

INSERT INTO `m_inbox` (`id_inbox`, `nama`, `email`, `telp`, `subject`, `pesan`, `deleted`) VALUES
(1, 'peci', 'ipungtole@gmail.com', '12.455', 'coba', 'mantuul', 1);

-- --------------------------------------------------------

--
-- Table structure for table `m_menufront`
--

DROP TABLE IF EXISTS `m_menufront`;
CREATE TABLE IF NOT EXISTS `m_menufront` (
  `id_menu` int(11) NOT NULL,
  `id_pages` int(255) DEFAULT NULL,
  `nama_menu` varchar(255) DEFAULT NULL,
  `parent` varchar(255) DEFAULT NULL,
  `modul` int(1) DEFAULT NULL,
  `language` varchar(2) NOT NULL,
  `order` int(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `status` int(1) DEFAULT NULL,
  `delete` int(1) DEFAULT NULL,
  PRIMARY KEY (`id_menu`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `m_menufront`
--

INSERT INTO `m_menufront` (`id_menu`, `id_pages`, `nama_menu`, `parent`, `modul`, `language`, `order`, `url`, `status`, `delete`) VALUES
(1, NULL, 'beranda', NULL, 2, '1', 1, '', 1, NULL),
(2, 1, 'tentang kami', NULL, NULL, '1', 2, '', 1, NULL),
(3, NULL, 'hubungi', NULL, 3, '1', 4, '', 1, NULL),
(4, NULL, 'Event', NULL, 4, '1', 3, '', 1, NULL),
(5, 2, 'profile', '2', NULL, '1', 5, NULL, 1, NULL),
(6, NULL, 'Home', NULL, 2, '2', 6, '', 1, NULL),
(7, 3, 'about us', NULL, NULL, '2', 7, '', 1, NULL),
(8, NULL, 'Accueil', NULL, 2, '3', 8, '', 1, NULL),
(9, NULL, 'à propos de nous', '9', 2, '3', 9, '', 1, NULL),
(10, NULL, 'test', NULL, NULL, '0', 10, '', 1, NULL),
(11, NULL, 'opening page', NULL, 2, '0', 11, '', 1, NULL),
(12, NULL, 'Gallery', NULL, 1, '3', 12, '', 1, NULL),
(13, 4, 'villa lussy', '11', NULL, '0', 13, '', 1, NULL),
(14, NULL, 'nous contacter', NULL, 3, '3', 14, '', 1, NULL),
(15, NULL, 'Contact Us', NULL, 3, '2', 17, '', 1, NULL),
(16, NULL, 'contact us', NULL, 3, '0', 16, '', 1, NULL),
(17, NULL, 'Gallery', NULL, NULL, '2', 15, '', 1, NULL),
(18, NULL, 'Gallery2sss hh', NULL, NULL, '2', 18, '', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `m_modul`
--

DROP TABLE IF EXISTS `m_modul`;
CREATE TABLE IF NOT EXISTS `m_modul` (
  `id_modul` int(11) NOT NULL,
  `nama_modul` varchar(155) DEFAULT NULL,
  `templates` varchar(50) DEFAULT NULL,
  `alias` varchar(155) DEFAULT NULL,
  PRIMARY KEY (`id_modul`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `m_modul`
--

INSERT INTO `m_modul` (`id_modul`, `nama_modul`, `templates`, `alias`) VALUES
(1, 'galleri', 'galleri', 'galleri kami'),
(2, 'home', 'home', 'home'),
(3, 'contact-us', 'hubungi', 'contact-us'),
(4, 'event', 'event', 'event');

-- --------------------------------------------------------

--
-- Table structure for table `m_pages`
--

DROP TABLE IF EXISTS `m_pages`;
CREATE TABLE IF NOT EXISTS `m_pages` (
  `id_pages` int(11) NOT NULL,
  `id_parent` varchar(255) DEFAULT NULL,
  `judul` varchar(100) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `gambar` varchar(100) DEFAULT NULL,
  `konten` text DEFAULT NULL,
  `status` int(1) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL ON UPDATE current_timestamp(),
  `language` varchar(2) NOT NULL,
  `deleted` int(1) DEFAULT NULL,
  PRIMARY KEY (`id_pages`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_pages`
--

INSERT INTO `m_pages` (`id_pages`, `id_parent`, `judul`, `slug`, `gambar`, `konten`, `status`, `created_at`, `language`, `deleted`) VALUES
(1, '1', 'tentang kami', 'tentang-kami', NULL, '\r\n<p><!-- Start Sample Area -->\r\n	</p><p><section class=\"sample-text-area\">\r\n		<div class=\"container box_1170\">\r\n			<h3 class=\"text-heading\">The VILLA LUSSY</h3>\r\n<center>\r\n<table class=\"table table-borderless  table-sm\">\r\n<tr>\r\n<td><img src=\"images/rooms_black.svg\" width=\"40px\"></td>\r\n<td><img src=\"images/property_size_black.svg\" width=\"40px\"></td>\r\n<td><img src=\"images/terasse_black.svg\" width=\"40px\"></td>\r\n<td><img src=\"images/land_area_black.svg\" width=\"40px\"></td>\r\n<td><img src=\"images/bedroom_black.svg\" width=\"40px\"></td>\r\n<td><img src=\"images/swimming-pool_black.svg\" width=\"40px\"></td>\r\n</tr>\r\n<tr>\r\n<td>4 Room</td>\r\n<td>200 m²</td>\r\n<td>40 m²</td>\r\n<td>700 m²</td>\r\n<td>3 Bedrooms</td>\r\n<td>Pool size 10x4,5 M </td>\r\n</tr>\r\n</table>\r\n</center><br>\r\n\r\n			<p class=\"sample-text\">The villa lussy located in the middle of the bukit in ungassan Jimbaran, not fat from the famous temple of uluwatu and very beautifull surf beaches all around. Is the achievement of a great collaboration of Indonesian and French.</p>\r\n\r\n \r\n<p>\r\nIntended to offer a stay of relaxation and discovery of the Balinese spirit and unique way of life in the world, the villa provides you with all the necessary comfort and Ketut, the manager, will do everything to provide your desires and requests. Built in the middle of a tropical garden, it has two separate buildings, the main one with terrace, living room, equipped kitchen, master bedroom with double bed and dressing room and also  private bathroom.</p>\r\n\r\n \r\n<p>\r\nThe second building has two separate bedrooms, each with private bathroom and half open  shower in the middle of the greenery. One bedroom is equipped with a large double bed, the second has two separate beds that can be joined. The garden, the swimming pool and the gazebo decorate the whole for moments of pleasure, games or rest, reading or meditation. In addition, a closed parking lot covered with vegetation for a part, allows you to park in the shade of scooters or cars.\r\n</p>\r\n \r\n<p>\r\nFinally and of course, the beeding  is included (sheets, bath and pool towels) and a cleaning lady comes every day. \r\n\r\n<p> KALAWEIT We have made the decision to support this very fine organization, including Chanee and his team, work daily for the preservation of Indonesian fauna and more particularly the Borneo orangutans and gibbons. Those a small part of the rental income will be donated to them, and each one can, according to his desire, complete this donation. Find out more on their site. </p>\r\n\r\n<p>\r\nFACILITY\r\n\r\nA pick up and drop to  the airport is possible by the manager Ketut, as well as an accompaniment for the departure (with financial participation), the rental of scooter, organization of excursions with driver in all Bali or tickets of boats for visit of neighbords islands.  Ketut will be able to respond to many other requests\r\n</p>\r\n\r\n<p class=\"sample-text\"><br></p><a href=\"https://www.kalaweit.org/\" target=\"blank\"><img src=\"images/partner/kalaweit.png\" width=\"150px\"></a>\r\n<a href=\"https://www.smoobu.com/en/\" target=\"blank\"><img src=\"images/partner/smoobu.png\" width=\"150px\"></a>\r\n</p>\r\n</div></section>', 1, '2021-01-25 22:30:19', '1', NULL),
(2, NULL, 'profile', 'profile', NULL, '<p><!-- Start Sample Area -->\r\n	</p><p><section class=\"sample-text-area\">\r\n		<div class=\"container box_1170\">\r\n			<h3 class=\"text-heading\">Profile Sample</h3>\r\n			<p class=\"sample-text\">Profile Profile&nbsp; Profile&nbsp; Profile&nbsp; Profile&nbsp; Profile&nbsp; Profile&nbsp; Profile&nbsp; Profile&nbsp;</p><p class=\"sample-text\">Profile Profile&nbsp; Profile&nbsp; Profile&nbsp; Profile&nbsp; Profile&nbsp; Profile&nbsp; Profile&nbsp; Profile&nbsp;</p><p class=\"sample-text\">Profile Profile&nbsp; Profile&nbsp; Profile&nbsp; Profile&nbsp; Profile&nbsp; Profile&nbsp; Profile&nbsp; Profile&nbsp;</p><p class=\"sample-text\">Profile Profile&nbsp; Profile&nbsp; Profile&nbsp; Profile&nbsp; Profile&nbsp; Profile&nbsp; Profile&nbsp; Profile&nbsp;</p><p class=\"sample-text\">Profile Profile&nbsp; Profile&nbsp; Profile&nbsp; Profile&nbsp; Profile&nbsp; Profile&nbsp; Profile&nbsp; Profile&nbsp;</p><p class=\"sample-text\">Profile Profile&nbsp; Profile&nbsp; Profile&nbsp; Profile&nbsp; Profile&nbsp; Profile&nbsp; Profile&nbsp; Profile&nbsp;</p><p class=\"sample-text\"><br></p></div></section></p>', 1, '2020-10-12 10:50:47', '1', NULL),
(3, '1', 'about us', 'about-us', NULL, '\r\n<p><!-- Start Sample Area -->\r\n	</p><p><section class=\"sample-text-area\">\r\n		<div class=\"container box_1170\">\r\n			<h3 class=\"text-heading\">The VILLA LUSSY</h3>\r\n			<p class=\"sample-text\">The villa lussy located in the middle of the bukit in ungassan Jimbaran, not fat from the famous temple of uluwatu and very beautifull surf beaches all around. Is the achievement of a great collaboration of Indonesian and French.</p>\r\n\r\n \r\n<p>\r\nIntended to offer a stay of relaxation and discovery of the Balinese spirit and unique way of life in the world, the villa provides you with all the necessary comfort and Ketut, the manager, will do everything to provide your desires and requests. Built in the middle of a tropical garden, it has two separate buildings, the main one with terrace, living room, equipped kitchen, master bedroom with double bed and dressing room and also  private bathroom.</p>\r\n\r\n \r\n<p>\r\nThe second building has two separate bedrooms, each with private bathroom and half open  shower in the middle of the greenery. One bedroom is equipped with a large double bed, the second has two separate beds that can be joined. The garden, the swimming pool and the gazebo decorate the whole for moments of pleasure, games or rest, reading or meditation. In addition, a closed parking lot covered with vegetation for a part, allows you to park in the shade of scooters or cars.\r\n</p>\r\n \r\n<p>\r\nFinally and of course, the beeding  is included (sheets, bath and pool towels) and a cleaning lady comes every day. <p class=\"sample-text\"><br></p></div></section></p>', 1, '2021-01-25 22:30:26', '2', NULL),
(4, '1', 'Bienvenue a Villa Lussy', 'Bienvenue-a-Villa-Lussy', NULL, '<p class=\"MsoNormal\" style=\"margin: 0cm 0cm 8pt; line-height: 15.693333625793457px; font-size: 11pt; font-family: Calibri, sans-serif; caret-color: rgb(0, 0, 0); color: rgb(0, 0, 0);\"><span lang=\"FR\" style=\"font-family: &quot;Eras Medium ITC&quot;, sans-serif;\">La villa Lussy, située en plein milieu de la presqu’île du Bukit à Ungassan Jimbarran, non loin du fameux temple d’Uluwatu et des très belles plages de surf tout autour de la presqu’île, est la réalisation d ‘une belle collaboration humaine indonésienne et française.<o:p></o:p></span></p><p class=\"MsoNormal\" style=\"margin: 0cm 0cm 8pt; line-height: 15.693333625793457px; font-size: 11pt; font-family: Calibri, sans-serif; caret-color: rgb(0, 0, 0); color: rgb(0, 0, 0);\"><span lang=\"FR\" style=\"font-family: &quot;Eras Medium ITC&quot;, sans-serif;\">Destinée à offrir un séjour de détente et de découverte de l’esprit et du mode de vie balinais unique au monde, la villa met à votre disposition tout le confort nécessaire et Ketut, le manager, fera tout pour répondre à vos désirs et demandes.<o:p></o:p></span></p><p class=\"MsoNormal\" style=\"margin: 0cm 0cm 8pt; line-height: 15.693333625793457px; font-size: 11pt; font-family: Calibri, sans-serif; caret-color: rgb(0, 0, 0); color: rgb(0, 0, 0);\"><span lang=\"FR\" style=\"font-family: &quot;Eras Medium ITC&quot;, sans-serif;\">Construite au milieu d’un jardin tropical, elle dispose deux bâtiments séparés, le principal avec terrasse, salon, cuisine équipée, chambre de maitre avec grand lit et dressing ainsi qu’une salle de bain privée.<o:p></o:p></span></p><p class=\"MsoNormal\" style=\"margin: 0cm 0cm 8pt; line-height: 15.693333625793457px; font-size: 11pt; font-family: Calibri, sans-serif; caret-color: rgb(0, 0, 0); color: rgb(0, 0, 0);\"><span lang=\"FR\" style=\"font-family: &quot;Eras Medium ITC&quot;, sans-serif;\">Le second bâtiment comprend deux chambres séparées, chacune avec salle de bain privative et douche au milieu de la végétation. Une chambre étant équipée d’un grand lit double, la seconde de deux lits séparés pouvant être joints.&nbsp;<o:p></o:p></span></p><p class=\"MsoNormal\" style=\"margin: 0cm 0cm 8pt; line-height: 15.693333625793457px; font-size: 11pt; font-family: Calibri, sans-serif; caret-color: rgb(0, 0, 0); color: rgb(0, 0, 0);\"><span lang=\"FR\" style=\"font-family: &quot;Eras Medium ITC&quot;, sans-serif;\">Le jardin, la piscine et le gazebo, agrémentent l’ensemble pour des moments de plaisirs, de jeux ou de repos, de lecture ou de méditation.<o:p></o:p></span></p><p class=\"MsoNormal\" style=\"margin: 0cm 0cm 8pt; line-height: 15.693333625793457px; font-size: 11pt; font-family: Calibri, sans-serif; caret-color: rgb(0, 0, 0); color: rgb(0, 0, 0);\"><span lang=\"FR\" style=\"font-family: &quot;Eras Medium ITC&quot;, sans-serif;\">De plus, un parking fermé et couvert de végétation pour une partie, permet de stationner à l’ombre scooters ou voiture.<o:p></o:p></span></p><p class=\"MsoNormal\" style=\"margin: 0cm 0cm 8pt; line-height: 15.693333625793457px; font-size: 11pt; font-family: Calibri, sans-serif; caret-color: rgb(0, 0, 0); color: rgb(0, 0, 0);\"><span lang=\"FR\" style=\"font-family: &quot;Eras Medium ITC&quot;, sans-serif;\">Enfin et bien sûr, le linge de maison est compris, (draps, serviettes de bain et de piscine) et une dame de ménage passe chaque jour.</span><span style=\"font-family: &quot;Times New Roman&quot;;\">?</span><span lang=\"FR\" style=\"font-family: &quot;Eras Medium ITC&quot;, sans-serif;\"><o:p></o:p></span></p>', 1, '2021-01-25 22:30:32', '0', NULL),
(5, '1', 'Accueil', 'Accueil', NULL, '<p><b><font color=\"#000000\" style=\"background-color: rgb(255, 255, 0);\">La Villa Lussy</font></b><br></p><p class=\"MsoNormal\" style=\"margin: 0cm 0cm 8pt; line-height: 15.693333625793457px; font-size: 11pt; font-family: Calibri, sans-serif; caret-color: rgb(0, 0, 0); color: rgb(0, 0, 0);\"><span lang=\"FR\" style=\"font-family: &quot;Eras Medium ITC&quot;, sans-serif;\">La villa Lussy, située en plein milieu de la presqu’île du Bukit à Ungassan Jimbarran, non loin du fameux temple d’Uluwatu et des très belles plages de surf tout autour de la presqu’île, est la réalisation d ‘une belle collaboration humaine indonésienne et française.<o:p></o:p></span></p><p class=\"MsoNormal\" style=\"margin: 0cm 0cm 8pt; line-height: 15.693333625793457px; font-size: 11pt; font-family: Calibri, sans-serif; caret-color: rgb(0, 0, 0); color: rgb(0, 0, 0);\"><span lang=\"FR\" style=\"font-family: &quot;Eras Medium ITC&quot;, sans-serif;\">Destinée à offrir un séjour de détente et de découverte de l’esprit et du mode de vie balinais unique au monde, la villa met à votre disposition tout le confort nécessaire et Ketut, le manager, fera tout pour répondre à vos désirs et demandes.<o:p></o:p></span></p><p class=\"MsoNormal\" style=\"margin: 0cm 0cm 8pt; line-height: 15.693333625793457px; font-size: 11pt; font-family: Calibri, sans-serif; caret-color: rgb(0, 0, 0); color: rgb(0, 0, 0);\"><span lang=\"FR\" style=\"font-family: &quot;Eras Medium ITC&quot;, sans-serif;\">Construite au milieu d’un jardin tropical, elle dispose deux bâtiments séparés, le principal avec terrasse, salon, cuisine équipée, chambre de maitre avec grand lit et dressing ainsi qu’une salle de bain privée.<o:p></o:p></span></p><p class=\"MsoNormal\" style=\"margin: 0cm 0cm 8pt; line-height: 15.693333625793457px; font-size: 11pt; font-family: Calibri, sans-serif; caret-color: rgb(0, 0, 0); color: rgb(0, 0, 0);\"><span lang=\"FR\" style=\"font-family: &quot;Eras Medium ITC&quot;, sans-serif;\">Le second bâtiment comprend deux chambres séparées, chacune avec salle de bain privative et douche au milieu de la végétation. Une chambre étant équipée d’un grand lit double, la seconde de deux lits séparés pouvant être joints.&nbsp;<o:p></o:p></span></p><p class=\"MsoNormal\" style=\"margin: 0cm 0cm 8pt; line-height: 15.693333625793457px; font-size: 11pt; font-family: Calibri, sans-serif; caret-color: rgb(0, 0, 0); color: rgb(0, 0, 0);\"><span lang=\"FR\" style=\"font-family: &quot;Eras Medium ITC&quot;, sans-serif;\">Le jardin, la piscine et le gazebo, agrémentent l’ensemble pour des moments de plaisirs, de jeux ou de repos, de lecture ou de méditation.<o:p></o:p></span></p><p class=\"MsoNormal\" style=\"margin: 0cm 0cm 8pt; line-height: 15.693333625793457px; font-size: 11pt; font-family: Calibri, sans-serif; caret-color: rgb(0, 0, 0); color: rgb(0, 0, 0);\"><span lang=\"FR\" style=\"font-family: &quot;Eras Medium ITC&quot;, sans-serif;\">De plus, un parking fermé et couvert de végétation pour une partie, permet de stationner à l’ombre scooters ou voiture.<o:p></o:p></span></p><p class=\"MsoNormal\" style=\"margin: 0cm 0cm 8pt; line-height: 15.693333625793457px; font-size: 11pt; font-family: Calibri, sans-serif; caret-color: rgb(0, 0, 0); color: rgb(0, 0, 0);\"><span lang=\"FR\" style=\"font-family: &quot;Eras Medium ITC&quot;, sans-serif;\">Enfin et bien sûr, le linge de maison est compris, (draps, serviettes de bain et de piscine) et une dame de ménage passe chaque jour.<o:p></o:p></span></p>', 1, '2021-01-25 22:30:34', '3', NULL),
(6, NULL, 'Welcome to our  Villa Lussy', 'Welcome-to-our-Villa-Lussy', NULL, '<p class=\"MsoNormal\" style=\"margin: 0cm; font-size: medium; font-family: Calibri, sans-serif; caret-color: rgb(0, 0, 0); color: rgb(0, 0, 0);\"><span lang=\"EN-US\">The VILLA LUSSY<o:p></o:p></span></p><p class=\"MsoNormal\" style=\"margin: 0cm; font-size: medium; font-family: Calibri, sans-serif; caret-color: rgb(0, 0, 0); color: rgb(0, 0, 0);\"><span lang=\"EN-US\">&nbsp;</span></p><p class=\"MsoNormal\" style=\"margin: 0cm; font-size: medium; font-family: Calibri, sans-serif; caret-color: rgb(0, 0, 0); color: rgb(0, 0, 0);\"><span lang=\"EN-US\">&nbsp;</span></p><p class=\"MsoNormal\" style=\"margin: 0cm; font-size: medium; font-family: Calibri, sans-serif; caret-color: rgb(0, 0, 0); color: rgb(0, 0, 0);\"><span lang=\"EN-US\">The villa lussy located in the middle of the bukit in ungassan Jimbaran, not fat from the famous temple of uluwatu and very beautifull surf beaches all around. Is the achievement of a great collaboration of Indonesian and French.<o:p></o:p></span></p><p class=\"MsoNormal\" style=\"margin: 0cm; font-size: medium; font-family: Calibri, sans-serif; caret-color: rgb(0, 0, 0); color: rgb(0, 0, 0);\"><span lang=\"EN-US\">&nbsp;</span></p><p class=\"MsoNormal\" style=\"margin: 0cm; font-size: medium; font-family: Calibri, sans-serif; caret-color: rgb(0, 0, 0); color: rgb(0, 0, 0);\"><span style=\"font-family: &quot;Times New Roman&quot;, serif;\">Intended to offer a stay of relaxation and discovery of the Balinese spirit and&nbsp;</span><span lang=\"EN-US\" style=\"font-family: &quot;Times New Roman&quot;, serif;\">unique&nbsp;</span><span style=\"font-family: &quot;Times New Roman&quot;, serif;\">way of life in the world, the villa provides you with all the necessary comfort and Ketut, the manager, will do everything to&nbsp;</span><span lang=\"EN-US\" style=\"font-family: &quot;Times New Roman&quot;, serif;\">provide&nbsp;</span><span style=\"font-family: &quot;Times New Roman&quot;, serif;\">your desires and requests. Built in the middle of a tropical garden, it has two separate buildings, the main one with terrace, living room, equipped kitchen, master bedroom with double bed and dressing room a</span><span lang=\"EN-US\" style=\"font-family: &quot;Times New Roman&quot;, serif;\">nd also&nbsp;</span><span style=\"font-family: &quot;Times New Roman&quot;, serif;\">&nbsp;private bathroom.<o:p></o:p></span></p><p class=\"MsoNormal\" style=\"margin: 0cm; font-size: medium; font-family: Calibri, sans-serif; caret-color: rgb(0, 0, 0); color: rgb(0, 0, 0);\"><o:p>&nbsp;</o:p></p><p class=\"MsoNormal\" style=\"margin: 0cm; font-size: medium; font-family: Calibri, sans-serif; caret-color: rgb(0, 0, 0); color: rgb(0, 0, 0);\"><span style=\"font-family: &quot;Times New Roman&quot;, serif;\">The second building has two separate bedrooms, each with private bathroom and</span><span lang=\"EN-US\" style=\"font-family: &quot;Times New Roman&quot;, serif;\">&nbsp;half open&nbsp;</span><span style=\"font-family: &quot;Times New Roman&quot;, serif;\">&nbsp;shower in the middle of the greenery. One bedroom is equipped with a large double bed, the second has two separate beds that can be joined. The garden, the swimming pool and the gazebo decorate the whole for moments of pleasure, games or rest, reading or meditation. In addition, a closed parking lot covered with vegetation for a part, allows you to park in the shade of scooters or cars.<o:p></o:p></span></p><p class=\"MsoNormal\" style=\"margin: 0cm; font-size: medium; font-family: Calibri, sans-serif; caret-color: rgb(0, 0, 0); color: rgb(0, 0, 0);\"><span style=\"font-family: &quot;Times New Roman&quot;, serif;\"><o:p>&nbsp;</o:p></span></p><p><span style=\"caret-color: rgb(0, 0, 0); color: rgb(0, 0, 0); font-size: 12pt; font-family: &quot;Times New Roman&quot;, serif;\">Finally and of course,&nbsp;</span><span lang=\"EN-US\" style=\"caret-color: rgb(0, 0, 0); color: rgb(0, 0, 0); font-size: 12pt; font-family: &quot;Times New Roman&quot;, serif;\">the beeding&nbsp;</span><span style=\"caret-color: rgb(0, 0, 0); color: rgb(0, 0, 0); font-size: 12pt; font-family: &quot;Times New Roman&quot;, serif;\">&nbsp;is included (sheets, bath and pool towels) and a cleaning lady comes every day.&nbsp;</span><span style=\"caret-color: rgb(0, 0, 0); color: rgb(0, 0, 0); font-family: -webkit-standard; font-size: medium;\"></span><br></p>', 1, NULL, '2', NULL),
(7, NULL, 'Home', 'Home', NULL, '<p><b style=\"caret-color: rgb(0, 0, 0); color: rgb(0, 0, 0); font-family: &quot;Times New Roman&quot;, serif;\"><span style=\"font-family: &quot;Eras Medium ITC&quot;, sans-serif;\">LA VILLA LUSSY</span></b><br></p><p class=\"MsoNormal\" style=\"margin: 0cm; font-size: medium; font-family: &quot;Times New Roman&quot;, serif; caret-color: rgb(0, 0, 0); color: rgb(0, 0, 0);\"><b><span style=\"font-family: &quot;Eras Medium ITC&quot;, sans-serif;\"><o:p>&nbsp;</o:p></span></b></p><p class=\"MsoNormal\" style=\"margin: 0cm; font-size: medium; font-family: &quot;Times New Roman&quot;, serif; caret-color: rgb(0, 0, 0); color: rgb(0, 0, 0);\"><span style=\"font-family: &quot;Eras Medium ITC&quot;, sans-serif;\">La villa Lussy, située en plein milieu de la presqu’île du Bukit à Ungassan Jimbarran, non loin du fameux temple d’Uluwatu et des très belles plages de surf tout autour de la presqu’île, est la réalisation d ‘une belle collaboration humaine indonésienne et française.<o:p></o:p></span></p><p class=\"MsoNormal\" style=\"margin: 0cm; font-size: medium; font-family: &quot;Times New Roman&quot;, serif; caret-color: rgb(0, 0, 0); color: rgb(0, 0, 0);\"><span style=\"font-family: &quot;Eras Medium ITC&quot;, sans-serif;\">Destinée à offrir un séjour de détente et de découverte de l’esprit et du mode de vie balinais unique au monde, la villa met à votre disposition tout le confort nécessaire et Ketut, le manager, fera tout pour répondre à vos désirs et demandes.<o:p></o:p></span></p><p class=\"MsoNormal\" style=\"margin: 0cm; font-size: medium; font-family: &quot;Times New Roman&quot;, serif; caret-color: rgb(0, 0, 0); color: rgb(0, 0, 0);\"><span style=\"font-family: &quot;Eras Medium ITC&quot;, sans-serif;\">Construite au milieu d’un jardin tropical, elle dispose deux bâtiments séparés, le principal avec terrasse, salon, cuisine équipée, chambre de maitre avec grand lit et dressing ainsi qu’une salle de bain privée.<o:p></o:p></span></p><p class=\"MsoNormal\" style=\"margin: 0cm; font-size: medium; font-family: &quot;Times New Roman&quot;, serif; caret-color: rgb(0, 0, 0); color: rgb(0, 0, 0);\"><span style=\"font-family: &quot;Eras Medium ITC&quot;, sans-serif;\">Le second bâtiment comprend deux chambres séparées, chacune avec salle de bain privative et douche au milieu de la végétation. Une chambre étant équipée d’un grand lit double, la seconde de deux lits séparés pouvant être joints.&nbsp;<o:p></o:p></span></p><p class=\"MsoNormal\" style=\"margin: 0cm; font-size: medium; font-family: &quot;Times New Roman&quot;, serif; caret-color: rgb(0, 0, 0); color: rgb(0, 0, 0);\"><span style=\"font-family: &quot;Eras Medium ITC&quot;, sans-serif;\">Le jardin, la piscine et le gazebo, agrémentent l’ensemble pour des moments de plaisirs, de jeux ou de repos, de lecture ou de méditation.<o:p></o:p></span></p><p class=\"MsoNormal\" style=\"margin: 0cm; font-size: medium; font-family: &quot;Times New Roman&quot;, serif; caret-color: rgb(0, 0, 0); color: rgb(0, 0, 0);\"><span style=\"font-family: &quot;Eras Medium ITC&quot;, sans-serif;\">De plus, un parking fermé et couvert de végétation pour une partie, permet de stationner à l’ombre scooters ou voiture.<o:p></o:p></span></p><p class=\"MsoNormal\" style=\"margin: 0cm; font-size: medium; font-family: &quot;Times New Roman&quot;, serif; caret-color: rgb(0, 0, 0); color: rgb(0, 0, 0);\"><span style=\"font-family: &quot;Eras Medium ITC&quot;, sans-serif;\">Enfin et bien sûr, le linge de maison est compris, (draps, serviettes de bain et de piscine) et une dame de ménage passe chaque jour.<o:p></o:p></span></p>', 1, '2020-11-25 20:04:28', '3', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `m_partner`
--

DROP TABLE IF EXISTS `m_partner`;
CREATE TABLE IF NOT EXISTS `m_partner` (
  `id_partner` int(11) NOT NULL AUTO_INCREMENT,
  `partner_name` varchar(100) DEFAULT NULL,
  `images` varchar(255) DEFAULT NULL,
  `description` text DEFAULT NULL,
  `deleted` int(1) DEFAULT NULL,
  `links` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_partner`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_partner`
--

INSERT INTO `m_partner` (`id_partner`, `partner_name`, `images`, `description`, `deleted`, `links`) VALUES
(1, 'kalaweit', 'kalaweit.png', NULL, NULL, 'https://www.kalaweit.org'),
(2, 'smoobu', 'smoobu.png', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `m_plugins`
--

DROP TABLE IF EXISTS `m_plugins`;
CREATE TABLE IF NOT EXISTS `m_plugins` (
  `id_plugins` int(11) NOT NULL,
  `nama_plugin` varchar(255) DEFAULT NULL,
  `status` int(1) DEFAULT NULL,
  PRIMARY KEY (`id_plugins`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_plugins`
--

INSERT INTO `m_plugins` (`id_plugins`, `nama_plugin`, `status`) VALUES
(1, 'Language', 1);

-- --------------------------------------------------------

--
-- Table structure for table `m_social_media`
--

DROP TABLE IF EXISTS `m_social_media`;
CREATE TABLE IF NOT EXISTS `m_social_media` (
  `id_sosmed` int(11) NOT NULL,
  `icon` varchar(50) DEFAULT NULL,
  `nama_sosmed` varchar(100) DEFAULT NULL,
  `user_id_sosmed` varchar(100) DEFAULT NULL,
  `url_sosmed` varchar(255) DEFAULT NULL,
  `order` int(1) DEFAULT NULL,
  `status` int(1) DEFAULT NULL,
  `deleted` int(1) DEFAULT NULL,
  PRIMARY KEY (`id_sosmed`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `m_social_media`
--

INSERT INTO `m_social_media` (`id_sosmed`, `icon`, `nama_sosmed`, `user_id_sosmed`, `url_sosmed`, `order`, `status`, `deleted`) VALUES
(1, '', 'whatsapp', '+33768514468', '', NULL, 0, 1),
(2, '<i class=\"fab fa-facebook-f\"></i>', 'facebook', '', 'facebook', NULL, 1, NULL),
(3, '<i class=\"fab fa-twitter\"></i>', 'twitter', NULL, 'twitter', NULL, 1, 1),
(4, '<i class=\"fab fa-behance\"></i>', 'instagram', '@villalussybali', 'https://www.instagram.com/villalussybali/', NULL, 1, NULL),
(5, '<i class=\"fab fa-linkedin-in\"></i>', 'linkedin', '', 'linkenid', NULL, 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `m_status`
--

DROP TABLE IF EXISTS `m_status`;
CREATE TABLE IF NOT EXISTS `m_status` (
  `id_status` int(11) NOT NULL AUTO_INCREMENT,
  `nama_status` varchar(100) DEFAULT NULL,
  `deleted` int(1) DEFAULT NULL,
  PRIMARY KEY (`id_status`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_status`
--

INSERT INTO `m_status` (`id_status`, `nama_status`, `deleted`) VALUES
(1, 'Diterima', NULL),
(2, 'Ditolak', NULL),
(3, 'Diskualifikasi', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `p_language`
--

DROP TABLE IF EXISTS `p_language`;
CREATE TABLE IF NOT EXISTS `p_language` (
  `id_language` int(11) NOT NULL,
  `language` varchar(255) DEFAULT NULL,
  `code` varchar(5) DEFAULT NULL,
  `alias` varchar(255) DEFAULT NULL,
  `flag` varchar(255) DEFAULT NULL,
  `default` varchar(1) DEFAULT NULL,
  `deleted` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`id_language`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `p_language`
--

INSERT INTO `p_language` (`id_language`, `language`, `code`, `alias`, `flag`, `default`, `deleted`) VALUES
(1, 'Indonesia', 'id', 'Bahasa', 'id.svg', '1', NULL),
(2, 'English', 'en', 'Language', 'uk.svg', '0', NULL),
(3, 'France', 'fr', 'Langue', 'fr.svg', '0', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_hak_akses`
--

DROP TABLE IF EXISTS `tbl_hak_akses`;
CREATE TABLE IF NOT EXISTS `tbl_hak_akses` (
  `id_hak_akses` int(11) NOT NULL,
  `id_menu` int(11) NOT NULL,
  `id_level_user` int(11) NOT NULL,
  `view_button` int(1) DEFAULT NULL,
  `list_data` int(1) DEFAULT NULL,
  `add_button` int(1) DEFAULT 1,
  `edit_button` int(1) DEFAULT 1,
  `delete_button` int(1) DEFAULT 1,
  `export_pdf_button` int(1) DEFAULT NULL,
  `verification_button` int(1) DEFAULT NULL,
  `aktif` int(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `tbl_hak_akses`
--

INSERT INTO `tbl_hak_akses` (`id_hak_akses`, `id_menu`, `id_level_user`, `view_button`, `list_data`, `add_button`, `edit_button`, `delete_button`, `export_pdf_button`, `verification_button`, `aktif`) VALUES
(1, 0, 1, NULL, NULL, 1, 1, 1, NULL, NULL, 1),
(2, 1, 1, NULL, NULL, 1, 1, 1, NULL, NULL, 1),
(3, 2, 1, NULL, NULL, 1, 1, 1, NULL, NULL, 1),
(4, 3, 1, NULL, 1, 1, 1, 1, NULL, NULL, 1),
(5, 4, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL, 1),
(6, 5, 1, NULL, 1, 1, 1, 1, NULL, 1, 1),
(7, 6, 1, NULL, 1, 1, 1, 1, NULL, NULL, 1),
(8, 7, 1, NULL, 1, 1, 1, 1, NULL, NULL, 1),
(9, 8, 1, NULL, 1, 1, 1, 1, NULL, NULL, 1),
(10, 9, 1, NULL, 1, 1, 1, 1, NULL, NULL, 1),
(11, 10, 1, NULL, 1, 1, 1, 1, NULL, NULL, 1),
(12, 11, 1, NULL, 1, NULL, 1, 1, 1, NULL, 1),
(13, 12, 1, NULL, 1, NULL, 1, 1, 1, NULL, 1),
(14, 13, 1, NULL, 1, 1, 1, 1, NULL, NULL, 1),
(15, 14, 1, NULL, 1, 1, 1, 1, NULL, NULL, 1),
(16, 15, 1, NULL, 1, 1, 1, 1, NULL, NULL, 1),
(17, 16, 1, NULL, 1, 1, 1, 1, NULL, NULL, 1),
(18, 17, 1, NULL, 1, 1, 1, 1, NULL, NULL, 1),
(19, 0, 2, NULL, NULL, 1, 1, 1, NULL, NULL, 1),
(20, 1, 2, NULL, NULL, 1, 1, 1, NULL, NULL, 1),
(21, 2, 2, NULL, 1, NULL, NULL, NULL, NULL, NULL, 1),
(22, 3, 2, NULL, 1, 1, 1, 1, NULL, NULL, 1),
(23, 4, 2, NULL, 1, NULL, NULL, NULL, NULL, NULL, 1),
(24, 5, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(25, 6, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(26, 7, 2, NULL, 1, 1, 1, 1, NULL, NULL, 1),
(27, 8, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(28, 9, 2, NULL, 1, 1, 1, 1, NULL, NULL, 1),
(29, 10, 2, NULL, 1, 1, 1, 1, NULL, NULL, 1),
(30, 11, 2, NULL, 1, NULL, NULL, 1, NULL, NULL, 1),
(31, 12, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(32, 13, 2, NULL, 1, NULL, 1, 1, NULL, NULL, 1),
(33, 14, 2, NULL, 1, NULL, NULL, NULL, NULL, NULL, 1),
(34, 15, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(35, 16, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(36, 17, 2, NULL, 1, NULL, 1, NULL, NULL, NULL, 1),
(37, 18, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL, 1),
(38, 19, 1, 1, 1, 1, NULL, NULL, NULL, NULL, 1),
(39, 18, 2, NULL, 1, NULL, NULL, NULL, NULL, NULL, 1),
(40, 19, 2, NULL, 1, NULL, NULL, NULL, NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_level_user`
--

DROP TABLE IF EXISTS `tbl_level_user`;
CREATE TABLE IF NOT EXISTS `tbl_level_user` (
  `id_level_user` int(11) NOT NULL,
  `nama_level_user` varchar(20) NOT NULL,
  `keterangan_level_user` varchar(255) DEFAULT '',
  `aktif` int(1) DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `tbl_level_user`
--

INSERT INTO `tbl_level_user` (`id_level_user`, `nama_level_user`, `keterangan_level_user`, `aktif`) VALUES
(1, 'DEVELOPER', 'SUPER ADMINISTRATOR', 1),
(2, 'ADMINISTRATOR', 'WEB ADMINISTRATOR', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_menu`
--

DROP TABLE IF EXISTS `tbl_menu`;
CREATE TABLE IF NOT EXISTS `tbl_menu` (
  `id_menu` int(11) NOT NULL,
  `id_parent` int(11) DEFAULT NULL,
  `nama_menu` varchar(255) DEFAULT NULL,
  `judul_menu` varchar(255) DEFAULT NULL,
  `link_menu` varchar(255) DEFAULT NULL,
  `icon_menu` varchar(255) DEFAULT NULL,
  `aktif_menu` int(1) DEFAULT NULL,
  `urutan_menu` int(11) DEFAULT NULL,
  `delete` int(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `tbl_menu`
--

INSERT INTO `tbl_menu` (`id_menu`, `id_parent`, `nama_menu`, `judul_menu`, `link_menu`, `icon_menu`, `aktif_menu`, `urutan_menu`, `delete`) VALUES
(0, NULL, 'Dashboards', NULL, 'admin/dashboard', 'home', NULL, 1, NULL),
(1, NULL, 'Web Manager', NULL, 'web', 'assessment', NULL, 2, NULL),
(2, NULL, 'Registration', NULL, 'registration', 'input', NULL, 4, NULL),
(3, 1, 'Gallery', NULL, 'web/gallery', '', NULL, 11, NULL),
(4, NULL, 'Master', NULL, 'master', 'account_balance_wallet', NULL, 3, NULL),
(5, 2, 'Daftar Peserta', NULL, 'registration/daftar_peserta', '', NULL, 6, NULL),
(6, 4, 'Event', NULL, 'master/event', '', NULL, 7, NULL),
(7, 1, 'Slider', NULL, 'web/slider', '', NULL, 9, NULL),
(8, 14, 'User Manager', NULL, 'configuration/user', NULL, NULL, 18, NULL),
(9, 1, 'Menu Manager', NULL, 'web/menu', '', NULL, 10, NULL),
(10, 1, 'Page Manager', NULL, 'web/page', '', NULL, 8, NULL),
(12, 14, 'Modul Manager', NULL, 'configuration/modul', '', NULL, 17, NULL),
(14, NULL, 'Configurasi', NULL, 'configuration', 'settings', NULL, 14, NULL),
(15, 14, 'Role Manager', NULL, 'configuration/role', NULL, NULL, 16, NULL),
(16, 14, 'Menu Manager', NULL, 'configuration/menu', NULL, NULL, 15, NULL),
(17, 14, 'Application', NULL, 'configuration/setting', NULL, NULL, 19, NULL),
(18, NULL, 'Perlombaan', NULL, 'perlombaan', 'devices', NULL, 5, NULL),
(19, 18, 'Catatan Perlombaan', NULL, 'perlombaan/catatan_perlombaan', '', NULL, 20, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user`
--

DROP TABLE IF EXISTS `tbl_user`;
CREATE TABLE IF NOT EXISTS `tbl_user` (
  `id_user` int(11) NOT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` text DEFAULT NULL,
  `id_level_user` int(11) DEFAULT NULL,
  `id_pegawai` int(11) DEFAULT NULL,
  `cookie` varchar(255) DEFAULT NULL,
  `status` int(1) DEFAULT NULL,
  `last_login` datetime DEFAULT NULL,
  `failed_login` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL ON UPDATE current_timestamp(),
  `delete` varchar(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_user`
--

INSERT INTO `tbl_user` (`id_user`, `username`, `password`, `id_level_user`, `id_pegawai`, `cookie`, `status`, `last_login`, `failed_login`, `created_at`, `updated_at`, `delete`) VALUES
(1, 'pungki', '937851b5db0f4cdf445d1299ec39a44de18433afeeef8bcbddd1811d2e282112b77d604c873097da22bec6b0c5a13c2dea93d75c5eba37e3926c486735bd3da98yLskjHwNLMakYicf45H8+EFSDxuHWdJC8U9G4Pmjtw=', 1, NULL, NULL, 1, '2021-07-05 13:09:11', NULL, NULL, '2021-07-05 20:09:11', ''),
(2, 'admin', '08a0daa42f6d17b2b39740e08f1392d3a96ad2f153da0235fb28b0a6d1b4ca43f6a909c2adbbe708be4288a454ed3bfb9d5106f86a3898ba3ee8db65c9f4cc92SbkyGt4zUc1YmhRreHDuPs8IkunwRcTwTq0oDKAzar4=', 2, NULL, NULL, 1, '2020-11-25 12:35:24', NULL, NULL, '2020-11-25 19:35:24', '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user_detail`
--

DROP TABLE IF EXISTS `tbl_user_detail`;
CREATE TABLE IF NOT EXISTS `tbl_user_detail` (
  `id_user_detail` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `nama_lengkap_user` varchar(100) DEFAULT 'Akun Baru',
  `alamat_user` text DEFAULT NULL,
  `tanggal_lahir_user` date DEFAULT '1970-01-01',
  `jenis_kelamin_user` varchar(10) DEFAULT NULL,
  `no_telp_user` varchar(15) DEFAULT NULL,
  `gambar_user` varchar(250) DEFAULT NULL,
  `thumb_gambar_user` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `tbl_user_detail`
--

INSERT INTO `tbl_user_detail` (`id_user_detail`, `id_user`, `email`, `nama_lengkap_user`, `alamat_user`, `tanggal_lahir_user`, `jenis_kelamin_user`, `no_telp_user`, `gambar_user`, `thumb_gambar_user`) VALUES
(1, 1, 'ipungtole@gmail.com', 'Pungki Dwi Prasetiyo', '', '1970-01-01', NULL, '', NULL, NULL),
(2, 2, 'villalussyungassan@hotmail.com', 'Eddy Witokyo', 'Jl. Langui Kauh, Ungasan', '1970-01-01', NULL, '+33768514468', '1602230668863.jpeg', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `t_booking`
--

DROP TABLE IF EXISTS `t_booking`;
CREATE TABLE IF NOT EXISTS `t_booking` (
  `id_boking` int(255) NOT NULL,
  `kode_booking` varchar(100) DEFAULT NULL,
  `id_villa` int(255) DEFAULT NULL,
  `keterangan` varchar(255) DEFAULT NULL,
  `nama_booking` varchar(150) DEFAULT NULL,
  `email_booking` varchar(255) DEFAULT NULL,
  `telp_booking` varchar(255) DEFAULT NULL,
  `tanggal_booking` datetime DEFAULT NULL,
  `tanggal_checkout` datetime DEFAULT NULL,
  `harga` float DEFAULT NULL,
  `tanggal_insert` datetime DEFAULT NULL,
  `status_booking` int(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `t_dictionary`
--

DROP TABLE IF EXISTS `t_dictionary`;
CREATE TABLE IF NOT EXISTS `t_dictionary` (
  `id_dictionary` int(11) NOT NULL AUTO_INCREMENT,
  `id_language` int(11) DEFAULT NULL,
  `id_parent` int(11) DEFAULT NULL,
  `words` varchar(255) DEFAULT NULL,
  `id_modul` int(11) DEFAULT NULL,
  `deleted` int(1) DEFAULT NULL,
  PRIMARY KEY (`id_dictionary`),
  KEY `sdsds` (`id_language`)
) ENGINE=MyISAM AUTO_INCREMENT=37 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_dictionary`
--

INSERT INTO `t_dictionary` (`id_dictionary`, `id_language`, `id_parent`, `words`, `id_modul`, `deleted`) VALUES
(1, 3, 1, 'Bienvenue à la Villa Lussy', 2, NULL),
(2, 1, 1, 'Selamat Datang Di Villa Lussy', 2, NULL),
(3, 2, 1, 'Welcome to Villa Lussy', 2, NULL),
(4, 1, 4, 'Galleri Villa Lussy', 1, NULL),
(5, 2, 4, 'Our Gallery', 1, NULL),
(6, 3, 4, 'Galerie', 1, NULL),
(7, 1, 7, 'Hubungi Kami', 3, NULL),
(8, 2, 7, 'Contact Us', 3, NULL),
(9, 3, 7, 'Contactez-nous', 3, NULL),
(10, 1, 10, 'Lama Tinggal', NULL, NULL),
(11, 2, 10, 'During of stay', NULL, NULL),
(12, 3, 10, 'Durée de séjour', NULL, NULL),
(13, 1, 13, 'Daftar Harga', NULL, NULL),
(14, 2, 13, 'Price whole villa', NULL, NULL),
(15, 3, 13, 'Prix la villa complète', NULL, NULL),
(16, 1, 16, 'Minimal Tinggal 3 malam', NULL, NULL),
(17, 2, 16, 'Minimum stay 3 night', NULL, NULL),
(18, 3, 16, 'Minimum 3 nuit', NULL, NULL),
(19, 1, 19, '1 Minggu', NULL, NULL),
(20, 2, 19, '1 week', NULL, NULL),
(21, 3, 19, '1 semaine', NULL, NULL),
(22, 1, 22, 'Selengkapnya...', NULL, NULL),
(23, 2, 22, 'More...', NULL, NULL),
(24, 3, 22, 'Plus...', NULL, NULL),
(25, 1, 25, 'Untuk reservasi dan informasi lebih lanjut, silahkan hubungi kami di \"villalussybali@hotmail.com\"', NULL, NULL),
(26, 2, 25, 'For reservation and more information, please contact us at \"villalussybali@hotmail.com\"', NULL, NULL),
(27, 3, 25, 'Pour la réservation et plus d\'informations, veuillez nous contacter sur villalussybali@hotmail.com', NULL, NULL),
(28, 1, 28, 'Send us your query anytime!', NULL, NULL),
(29, 2, 28, 'Send us your query anytime!', NULL, NULL),
(30, 3, 28, 'Envoyez-nous toutes vos demandes', NULL, NULL),
(31, 1, 31, 'Senin s/d Jumat 9 Pagi - 6 sore', NULL, NULL),
(32, 2, 31, 'Mon to Fri 9am to 6pm', NULL, NULL),
(33, 3, 31, 'Lundi à vendredi 9h00 à 18h00', NULL, NULL),
(34, 1, 34, 'malam', NULL, NULL),
(35, 2, 34, 'night', NULL, NULL),
(36, 3, 34, 'nuit', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `t_peserta`
--

DROP TABLE IF EXISTS `t_peserta`;
CREATE TABLE IF NOT EXISTS `t_peserta` (
  `id_peserta` int(255) NOT NULL AUTO_INCREMENT,
  `nama_peserta` varchar(255) DEFAULT NULL,
  `nik` varchar(17) DEFAULT NULL,
  `id_event` int(11) NOT NULL,
  `foto_peserta` varchar(255) DEFAULT NULL,
  `status_peserta` int(1) DEFAULT NULL,
  `tanggal_daftar` datetime DEFAULT NULL,
  `lap_temp` int(11) DEFAULT NULL,
  `score_temp` int(11) DEFAULT NULL,
  `deleted` int(1) DEFAULT NULL,
  PRIMARY KEY (`id_peserta`,`id_event`),
  KEY `id_event` (`id_event`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_peserta`
--

INSERT INTO `t_peserta` (`id_peserta`, `nama_peserta`, `nik`, `id_event`, `foto_peserta`, `status_peserta`, `tanggal_daftar`, `lap_temp`, `score_temp`, `deleted`) VALUES
(1, 'rania', '3578', 1, NULL, 1, '2021-06-29 20:09:08', NULL, NULL, NULL),
(2, 'yitno', '12345', 0, NULL, NULL, NULL, NULL, NULL, NULL),
(3, 'yitnos', '2', 1, NULL, 1, NULL, NULL, NULL, NULL),
(4, 'rrr', '666', 1, NULL, 2, '2021-06-29 13:24:46', NULL, NULL, NULL),
(5, 'yitno', '1234', 2, NULL, 1, '2021-06-29 13:42:08', NULL, NULL, NULL),
(6, 'rrr', '3571011304840008', 1, NULL, 1, '2021-06-30 14:48:34', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `t_scores`
--

DROP TABLE IF EXISTS `t_scores`;
CREATE TABLE IF NOT EXISTS `t_scores` (
  `id_transaction` int(11) NOT NULL AUTO_INCREMENT,
  `id_peserta` int(11) DEFAULT NULL,
  `skor` int(255) DEFAULT NULL,
  PRIMARY KEY (`id_transaction`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_scores`
--

INSERT INTO `t_scores` (`id_transaction`, `id_peserta`, `skor`) VALUES
(1, 1, 24),
(2, 1, 20),
(3, 1, 56),
(4, 5, 44),
(5, 3, 130),
(6, 6, 56),
(7, 1, 56),
(8, 1, 130),
(9, 3, 130),
(10, 3, 130),
(11, 3, 56),
(12, 3, 56);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
